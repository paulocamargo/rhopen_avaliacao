using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/RelatorioExecucao")]
   public class RelatorioExecucaoController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public RelatorioExecucaoController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           AvaliacaoModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(Avaliacao data)
       {
           return new
           {
               data.Codigo

           };
       }
       // GET: api/RelatorioExecucao
       [Authorize]
       public Object Get()
       {
           return Json(AvaliacaoModel.Instancia.Consultar().ToList().Select(p => new
           {
               p.Codigo
           }));
       }


        [HttpGet]
        [Route("consultarAvaliacao")]
        public Object consultarAvaliacao(int Codigo)
        {
            return Json(AvaliacaoModel.Instancia.Consultar(Codigo).ToList().Select(p => new
            {
                p.Codigo,
                p.ClientePessoaFisica.Nome,
                p.ItensSelecionadosEnunciados
            }));
        }

        // GET: api/RelatorioExecucao/5
        public Object Get(int id)
       {
           return Json(formatarDados(AvaliacaoModel.Instancia.Editar(id)));
       }

       // POST: api/RelatorioExecucao
       public Object Post([FromBody]Avaliacao value)
       {
           return Json(formatarDados(AvaliacaoModel.Instancia.Cadastrar(value)));
       }

       // PUT: api/RelatorioExecucao/5
       public Object Put(int id, [FromBody]Avaliacao value)
       {
           return Json(formatarDados(AvaliacaoModel.Instancia.Atualizar(value, id)));
       }

       // DELETE: api/RelatorioExecucao/5
       public Object Delete(int id)
       {
           return Json(formatarDados(AvaliacaoModel.Instancia.Deletar(id)));
       }
   }
}
