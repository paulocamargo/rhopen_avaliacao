using Globalsys;
using System;
using System.Linq;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;
using Globalsys.Extensoes;

namespace Plataforma.API.Controllers
{
    [RoutePrefix("api/PessoaFisica")]
   public class PessoaFisicaController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public PessoaFisicaController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           PessoaFisicaModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(PessoaFisica data)
       {
           return new
           {
               data.Codigo,
               data.Nome,
               data.CPF,
               CPFFormatado = data.CPF.ToCPFFormat(),
               data.Sexo,
               SexoFormatado = ((data.Sexo != "M") ? "Feminino" : "Masculino"),
               data.EMAIL,
               NivelHierarquico = (data.Hierarquia != null ? data.Hierarquia.Codigo : 0),
               hierarquiaNome = (data.Hierarquia != null ? data.Hierarquia.Nome : ""),
               DataNascimento = data.DataNascimento.ToShortDateString(),
               DataCadastro = data.DataCadastro.ToShortDateString()
           };
       }
        [HttpGet]
        public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            try
            {
                unidTrabalho.BeginTransaction();
                IQueryable<PessoaFisica> pessoaFisica = unidTrabalho.ObterTodos<PessoaFisica>().Where(x => x.DataDesativacao == null);
                unidTrabalho.Commit();
                return Json(pessoaFisica.ToList().Select(p => new
                {
                    p.Codigo,
                    p.Nome,
                    p.CPF,
                    CPFFormatado = p.CPF.ToCPFFormat(),
                    p.Sexo,
                    SexoFormatado = ((p.Sexo != "M") ? "Feminino" : "Masculino"),
                    p.EMAIL,
                    NivelHierarquico = (p.NivelHierarquico != null ? p.NivelHierarquico : ""),
                    hierarquiaNome = (p.NivelHierarquico != null ? p.Hierarquia.Nome : ""),
                    DataNascimento = p.DataNascimento.ToShortDateString(),
                    DataCadastro = p.DataCadastro.ToShortDateString()
                }));
                
            }
            catch (Exception e)
            {
                unidTrabalho.Commit();
                return e.Message;
            }
           //return Json(PessoaFisicaModel.Instancia.Consultar().ToList().Select(p => new
           //{
           //    p.Codigo,
           //    p.Nome,
           //    p.CPF,
           //    CPFFormatado = p.CPF.ToCPFFormat(),
           //    p.Sexo,
           //    SexoFormatado = ((p.Sexo != "M") ? "Feminino" : "Masculino"),
           //    p.EMAIL,
           //    NivelHierarquico = (p.NivelHierarquico!= null? p.NivelHierarquico: ""),
           //    hierarquiaNome = (p.NivelHierarquico != null ? p.Hierarquia.Nome : ""),
           //    DataNascimento = p.DataNascimento.ToShortDateString(),
           //    DataCadastro = p.DataCadastro.ToShortDateString()
           //}));
       }

        [HttpGet]
        [Route("buscaporCPF")]
        public Object buscaporCPF(string CPF)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<PessoaFisica> pessoaFisica = unidTrabalho.ObterTodos<PessoaFisica>().Where(x=>x.DataDesativacao == null);
            unidTrabalho.Commit();
            pessoaFisica = pessoaFisica.Where(x => x.CPF == CPF.Replace(".", "").Replace("-", ""));
            PessoaFisica pessoafisica = new PessoaFisica();
            return Json(pessoaFisica.ToList().Select(p => new
            {
                p.Codigo,
                p.Nome,
                p.CPF,
                CPFFormatado = p.CPF.ToCPFFormat(),
                p.Sexo,
                SexoFormatado = ((p.Sexo != "M") ? "Feminino" : "Masculino"),
                p.EMAIL,
                p.NivelHierarquico,
                DataNascimento = p.DataNascimento.ToShortDateString(),
                DataCadastro = p.DataCadastro.ToShortDateString()
            }));
        }

        // GET: api/PessoaFisica/5
        public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaFisica pessoa =  unidTrabalho.ObterPorId<PessoaFisica>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(pessoa));
       }

       // POST: api/PessoaFisica
       public Object Post([FromBody]PessoaFisica value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            UnidadeTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/PessoaFisica/5
       public Object Put(int id, [FromBody]PessoaFisica value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaFisica pessoa = unidTrabalho.ObterPorId<PessoaFisica>(id);
            pessoa.Nome = value.Nome;
            pessoa.Sexo = value.Sexo;
            pessoa.Hierarquia = value.Hierarquia;
            pessoa.EMAIL = value.EMAIL;
            pessoa.DataNascimento = value.DataNascimento;
            pessoa.CPF = value.CPF;
            unidTrabalho.Atualizar(pessoa);
            unidTrabalho.Commit();
            return Json(formatarDados(pessoa));
       }

       // DELETE: api/PessoaFisica/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaFisica pessoa = unidTrabalho.ObterPorId<PessoaFisica>(id);
            pessoa.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(pessoa);
            unidTrabalho.Commit();
            return Json(formatarDados(pessoa));
       }
   }
}
