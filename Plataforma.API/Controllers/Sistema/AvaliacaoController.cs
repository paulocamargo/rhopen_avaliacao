using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;
using Plataforma.Repositorios.Sistema;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/Avaliacao")]
   public class AvaliacaoController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public AvaliacaoController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           AvaliacaoModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(Avaliacao data)
       {
            return new
            {
                data.Codigo,
                NomeCliente =  data.ClientePessoaFisica.Nome,
                ClienteCodigo =  data.ClientePessoaFisica.Codigo,
                AvaliacaoRealizada = (data.HoraFim == null ? "N�o" : "Sim"),
                CategoriaPredomintante = (data.Categoria == null ? "" : data.Categoria.Nome),
                DataCadastro = data.DataCadastro.ToShortDateString(),
                itensSelecionados = data.ItensSelecionadosEnunciados,
                codCliente = data.ClientePessoaFisica.Codigo,
                codigoModAval = data.ModeloAvaliacao == null? 0 : data.ModeloAvaliacao.Codigo,
                nomeCliente = data.ClientePessoaFisica.Nome,
                ModeloAvaliacao = (data.ModeloAvaliacao == null? 0 :data.ModeloAvaliacao.Codigo)
            };
       }
       // GET: api/Avaliacao
       //[Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<PessoaFisica> iqueryCliente = unidTrabalho.ObterTodos<PessoaFisica>();
            IQueryable<Avaliacao> iqueryAvaliacao = unidTrabalho.ObterTodos<Avaliacao>();
            unidTrabalho.Commit();
            List<Avaliacao> avaliacoes = new List<Avaliacao>();
            foreach (var item in iqueryAvaliacao)
            {
                
                if(item.ClientePessoaFisica != null) { 
                    item.ClientePessoaFisica = iqueryCliente.Where(x => x.Codigo == item.ClientePessoaFisica.Codigo).FirstOrDefault();
                    avaliacoes.Add(item);
                }
            }

           return Json(avaliacoes.Select(p => new
           {
               p.Codigo,
               NomeCliente = (p.ClientePessoaFisica == null? "" : p.ClientePessoaFisica.Nome),
               ClienteCodigo = (p.ClientePessoaFisica == null? 0 : p.ClientePessoaFisica.Codigo),
               CategoriaPredomintante = (p.Categoria == null ? "" : p.Categoria.Nome ),
               DataCadastro = p.DataCadastro.ToShortDateString(),
               AvaliacaoRealizada = (p.HoraFim == null? "N�o" : "Sim"),
               ModeloAvaliacao = p.ModeloAvaliacao.Codigo
        }).OrderByDescending(x=>x.DataCadastro));
       }


        [HttpGet]
        [Route("consultarAvaliacaoRelatorio")]
        public Object consultarAvaliacaoRelatorio(int codigo)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Avaliacao iqueryAvaliacao = unidTrabalho.ObterPorId<Avaliacao>(codigo);
            unidTrabalho.Commit();
            return Json(formatarDados(iqueryAvaliacao));
        }

        [HttpGet]
        [Route("consultarEnunciados")]
        public Object consultarEnunciados(int Codigo)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaFisica pessoafisica = new PessoaFisica();
             pessoafisica.Codigo = Codigo;
            IQueryable<Avaliacao> avaliacaoCliente = unidTrabalho.ObterTodos<Avaliacao>().Where(X => X.ClientePessoaFisica.Codigo == Codigo && !(X.HoraFim.HasValue));
            Avaliacao aval = avaliacaoCliente.ToArray()[avaliacaoCliente.ToList().Count() - 1];
            IQueryable<EnunciadoAvaliacao> ListarEnunciadosAvaliacao = unidTrabalho.ObterTodos<EnunciadoAvaliacao>().Where(x => x.ModeloAvaliacao.Codigo == aval.ModeloAvaliacao.Codigo);
            //avaliacaoCliente = avaliacaoCliente.Where(X=>X.ClientePessoaFisica.Codigo == Codigo);
            unidTrabalho.Commit();
            if (aval.Codigo > 0)
            {
                
                
                return Json(ListarEnunciadosAvaliacao.Select(p => new
                {
                    p.Codigo,
                    //p.Codigo,
                    avalCodigo = aval.Codigo,
                    codigoModAval = p.ModeloAvaliacao.Codigo,
                    codigoEnum = p.Enunciado.Codigo,
                    codigoCategoria = p.Enunciado.Categoria.Codigo,
                    nomeCategoria = p.Enunciado.Categoria.Nome,
                    descCategoria = p.Enunciado.Categoria.Descricao,
                    nomeEnum = p.Enunciado.Nome,
                    p.DataCadastro
                }));
            }
            else
                return null;
        }

        [HttpGet]
        [Route("consultarEnunciadosRelatorio")]
        public Object consultarEnunciadosRelatorio(int Codigo)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaFisica pessoafisica = new PessoaFisica();
            pessoafisica.Codigo = Codigo;
            IQueryable<Avaliacao> avaliacaoCliente = unidTrabalho.ObterTodos<Avaliacao>();
            avaliacaoCliente = avaliacaoCliente.Where(x => x.ClientePessoaFisica.Codigo == pessoafisica.Codigo);
            IQueryable<EnunciadoAvaliacao> ListarEnunciadosAvaliacao = unidTrabalho.ObterTodos<EnunciadoAvaliacao>().Where(x => x.ModeloAvaliacao == avaliacaoCliente.ToList().Last().ModeloAvaliacao);
            unidTrabalho.Commit();
            //IQueryable<Avaliacao> avaliacaoCliente = AvaliacaoModel.Instancia.ConsultarAvaliacaoReltorio(pessoafisica);

            try { 
                return Json(ListarEnunciadosAvaliacao.ToList().Select(p => new
                {
                    p.Codigo,
                    //p.Codigo,
                    avalCodigo = avaliacaoCliente.ToList().Last().Codigo,
                    codigoModAval = p.ModeloAvaliacao.Codigo,
                    codigoEnum = p.Enunciado.Codigo,
                    codigoCategoria = p.Enunciado.Categoria.Codigo,
                    nomeCategoria = p.Enunciado.Categoria.Nome,
                    descCategoria = p.Enunciado.Categoria.Descricao,
                    nomeEnum = p.Enunciado.Nome,
                    p.DataCadastro
                }));
            }
            catch {           
                return Json(Fabrica.Instancia.ObterRepositorio<IRepositorioDadosGerais>(UnidadeTrabalho));
            }
        }

        // GET: api/Avaliacao/5
        public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Avaliacao avaliacao= unidTrabalho.ObterPorId<Avaliacao>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(avaliacao));
       }

       // POST: api/Avaliacao
       public Object Post([FromBody]Avaliacao value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            value.DataCadastro = DateTime.Now;
            value.HoraInicio = DateTime.Now;
            unidTrabalho.BeginTransaction();
            
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/Avaliacao/5
       public Object Put(int id, [FromBody]Avaliacao value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Avaliacao aval = unidTrabalho.ObterPorId<Avaliacao>(id);
            aval.Categoria = value.Categoria;
            aval.ClientePessoaFisica = value.ClientePessoaFisica;
            aval.HoraFim = value.HoraFim;
            aval.HoraInicio = value.HoraInicio;
            aval.ItensSelecionadosEnunciados = value.ItensSelecionadosEnunciados;
            aval.ModeloAvaliacao = value.ModeloAvaliacao;
            aval.QuantidadePagina = value.QuantidadePagina;
            aval.TempoCategoria = value.TempoCategoria;
            aval.TempoPagina = value.TempoPagina;
            aval.TempoTotalAvaliacao = value.TempoTotalAvaliacao;
            aval.TipoPessoa = value.TipoPessoa;
            unidTrabalho.Atualizar(aval);
            return Json(formatarDados(aval));
       }

        // PUT: api/Avaliacao/5 teste

        // DELETE: api/Avaliacao/5
        public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Avaliacao nova = unidTrabalho.ObterPorId<Avaliacao>(id);
            nova.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(nova);
            unidTrabalho.Commit();
            return Json(formatarDados(nova));
       }
   }
}
