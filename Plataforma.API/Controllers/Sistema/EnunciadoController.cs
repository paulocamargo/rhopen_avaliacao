using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/Enunciado")]
   public class EnunciadoController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public EnunciadoController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           EnunciadoModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(Enunciado data)
       {
           return new
           {
               data.Codigo,
               data.Nome,
               data.Identificador,
               DataCadastro = data.DataCadastro.ToShortDateString(),
               CodigoCategoria = ((data.Categoria != null) ? data.Categoria.Codigo : 0),
               Categoria = ((data.Categoria != null) ? data.Categoria.Nome : string.Empty),
           };
       }
       // GET: api/Enunciado
       [Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<Enunciado> enunciadoAvaliacao = unidTrabalho.ObterTodos<Enunciado>();
            unidTrabalho.Commit();
            return Json(enunciadoAvaliacao.ToList().Select(p => new
           {
               p.Codigo,
               p.Nome,
               p.Identificador,
               DataCadastro = p.DataCadastro.ToShortDateString(),
               CodigoCategoria = ((p.Categoria != null) ? p.Categoria.Codigo : 0),
               Categoria = ((p.Categoria != null) ? p.Categoria.Nome : string.Empty),
           }));
       }

       // GET: api/Enunciado/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Enunciado enunciado = unidTrabalho.ObterPorId<Enunciado>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(enunciado));
       }

       // POST: api/Enunciado
       public Object Post([FromBody]Enunciado value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/Enunciado/5
       public Object Put(int id, [FromBody]Enunciado value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Enunciado enunciado = unidTrabalho.ObterPorId<Enunciado>(id);
            enunciado.Categoria = value.Categoria;
            enunciado.Identificador = value.Identificador;
            enunciado.Nome = value.Nome;
            enunciado.TimeEnunciado = value.TimeEnunciado;
            unidTrabalho.Atualizar(enunciado);
            unidTrabalho.Commit();
            return Json(formatarDados(enunciado));
       }

       // DELETE: api/Enunciado/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Enunciado enunciado = unidTrabalho.ObterPorId<Enunciado>(id);
            enunciado.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(enunciado);
            unidTrabalho.Commit();
            return Json(formatarDados(enunciado));
       }
   }
}
