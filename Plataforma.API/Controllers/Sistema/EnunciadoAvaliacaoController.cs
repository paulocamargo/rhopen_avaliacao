using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Globalsys.Extensoes;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/EnunciadoAvaliacao")]
   public class EnunciadoAvaliacaoController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public EnunciadoAvaliacaoController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           EnunciadoAvaliacaoModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(Dominio.Sistema.EnunciadoAvaliacao data)
       {
           return new
           {
               data.Codigo,
               data.ModeloAvaliacao,
               data.Enunciado,
               enunCod = data.Enunciado.Codigo,
               enunNome = data.Enunciado.Nome,
               modvalCod = data.ModeloAvaliacao.Codigo,
               data.DataCadastro

           };
       }
       // GET: api/EnunciadoAvaliacao
       //[Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<EnunciadoAvaliacao> enunciadoAvaliacao = unidTrabalho.ObterTodos<EnunciadoAvaliacao>();
            unidTrabalho.Commit();
            return (enunciadoAvaliacao.ToList().Select(p => new
           {
               p.Codigo,
               p.ModeloAvaliacao,
               p.Enunciado,
               enunCod = p.Enunciado.Codigo,
               enunNome = p.Enunciado.Nome,
               modvalCod = p.ModeloAvaliacao.Codigo,
               p.DataCadastro
           }));
       }

        // GET: api/EnunciadoAvaliacao/5
        public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EnunciadoAvaliacao enunciadoAvaliacao = unidTrabalho.ObterPorId<EnunciadoAvaliacao>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(enunciadoAvaliacao));
       }

       // POST: api/EnunciadoAvaliacao
       public Object Post([FromBody]Dominio.Sistema.EnunciadoAvaliacao value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/EnunciadoAvaliacao/5
       public Object Put(int id, [FromBody]Dominio.Sistema.EnunciadoAvaliacao value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EnunciadoAvaliacao enunciadoAvaliacao = unidTrabalho.ObterPorId<EnunciadoAvaliacao>(id);
            enunciadoAvaliacao.ModeloAvaliacao = value.ModeloAvaliacao;
            enunciadoAvaliacao.Enunciado = value.Enunciado;
            unidTrabalho.Atualizar(enunciadoAvaliacao);
            unidTrabalho.Commit();
            return Json(formatarDados(enunciadoAvaliacao));
       }

       // DELETE: api/EnunciadoAvaliacao/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EnunciadoAvaliacao enunciadoAvaliacao = unidTrabalho.ObterPorId<EnunciadoAvaliacao>(id);
            enunciadoAvaliacao.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(enunciadoAvaliacao);
            unidTrabalho.Commit();
            return Json(formatarDados(enunciadoAvaliacao));
       }
   }
}
