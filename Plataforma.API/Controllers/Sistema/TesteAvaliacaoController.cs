using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.API.Models;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/TesteAvaliacao")]
   public class TesteAvaliacaoController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public TesteAvaliacaoController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           TesteAvaliacaoModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(TesteAvaliacao data)
       {
           return new
           {
               data.Codigo

           };
       }
       // GET: api/TesteAvaliacao
       [Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<TesteAvaliacao> testeAvaliacao = unidTrabalho.ObterTodos<TesteAvaliacao>();
            unidTrabalho.Commit();

            return (testeAvaliacao.ToList().Select(p => new
           {
               p.Codigo
           }));
       }

       // GET: api/TesteAvaliacao/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            TesteAvaliacao testeAvaliacao = unidTrabalho.ObterPorId<TesteAvaliacao>(id);
            unidTrabalho.Commit();
            return (formatarDados(testeAvaliacao));
       }

       // POST: api/TesteAvaliacao
       public Object Post([FromBody]TesteAvaliacao value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(TesteAvaliacaoModel.Instancia.Cadastrar(value)));
       }

       // PUT: api/TesteAvaliacao/5
       public Object Put(int id, [FromBody]TesteAvaliacao value)
       {
            return Json(formatarDados(value));
       }

        //public Object Put(int id, [FromBody]Avaliacao value)
        //{
        //    return Json(formatarDados(AvaliacaoModel.Instancia.Atualizar(value, id)));
        //}

        // DELETE: api/TesteAvaliacao/5
        public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            TesteAvaliacao testeAvaliacao = unidTrabalho.ObterPorId<TesteAvaliacao>(id);
            unidTrabalho.Remover(testeAvaliacao);
            unidTrabalho.Commit();

            return Json(formatarDados(testeAvaliacao));
       }
   }
}
