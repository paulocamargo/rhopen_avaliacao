using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.API.Models;
using Plataforma.Dominio.Sistema;
using Globalsys.Extensoes;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/PessoaJuridica")]
   public class PessoaJuridicaController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public PessoaJuridicaController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           PessoaJuridicaModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(PessoaJuridica data)
       {
           return new
           {
               data.Codigo,
               CNPJFormatado = data.CNPJ.ToCNPJFormat(),
               DataCadastro = data.DataCadastro.ToShortDateString(),
               data.EMAIL,
               Nome = data.RazaoSocial,
               RazaoSocial = data.RazaoSocial

           };
       }
       // GET: api/PessoaJuridica
       //[Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<PessoaJuridica> pessoaJuridica = unidTrabalho.ObterTodos<PessoaJuridica>();
            unidTrabalho.Commit();
            return Json(pessoaJuridica.ToList().Select(p => new
           {
               p.Codigo,
               CNPJFormatado = p.CNPJ.ToCNPJFormat(),
               DataCadastro = p.DataCadastro.ToShortDateString(),
               p.EMAIL,
               Nome = p.RazaoSocial,
               RazaoSocial = p.RazaoSocial
           }));
       }

       // GET: api/PessoaJuridica/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaJuridica pessoaJuridica = unidTrabalho.ObterPorId<PessoaJuridica>(id);
            unidTrabalho.Commit();

            return Json(formatarDados(pessoaJuridica));
       }

       // POST: api/PessoaJuridica
       public Object Post([FromBody]PessoaJuridica value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/PessoaJuridica/5
       public Object Put(int id, [FromBody]PessoaJuridica value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaJuridica pessoaJuridica = unidTrabalho.ObterPorId<PessoaJuridica>(id);
            pessoaJuridica.CNPJ = value.CNPJ;
            pessoaJuridica.EMAIL = value.EMAIL;
            pessoaJuridica.RazaoSocial = value.RazaoSocial;
            unidTrabalho.Atualizar(pessoaJuridica);
            unidTrabalho.Commit();
            return Json(formatarDados(pessoaJuridica));
       }

       // DELETE: api/PessoaJuridica/5
       public Object Delete(int id)
       {

            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            PessoaJuridica pessoaJuridica = unidTrabalho.ObterPorId<PessoaJuridica>(id);
            pessoaJuridica.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(pessoaJuridica);
            unidTrabalho.Commit();
            return Json(formatarDados(pessoaJuridica));
       }
   }
}
