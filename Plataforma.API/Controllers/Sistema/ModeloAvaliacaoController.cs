using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/ModeloAvaliacao")]
   public class ModeloAvaliacaoController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public ModeloAvaliacaoController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           ModeloAvaliacaoModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(ModeloAvaliacao data)
       {
           return new
           {
               data.Codigo,
               data.Nome,
               DataCadastro = data.DataCadastro.ToShortDateString(),
               QtdEnunciadosModelo = data.QtdEnunciadosModelo,
               TempoAvaliacao = data.TempoAvaliacao
           };
       }
       // GET: api/ModeloAvaliacao
       [Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<ModeloAvaliacao> modAvaliacao = unidTrabalho.ObterTodos<ModeloAvaliacao>();
            unidTrabalho.Commit();


            return Json(modAvaliacao.ToList().Select(p => new
           {
               p.Codigo,
               p.Nome,
               DataCadastro = p.DataCadastro.ToShortDateString(),
               QtdEnunciadosModelo = p.QtdEnunciadosModelo,
               TempoAvaliacao = p.TempoAvaliacao

           }));
       }

       // GET: api/ModeloAvaliacao/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            ModeloAvaliacao modAvaliacao = unidTrabalho.ObterPorId<ModeloAvaliacao>(id);
            unidTrabalho.Commit();

            return Json(formatarDados(modAvaliacao));
       }

       // POST: api/ModeloAvaliacao
       public Object Post([FromBody]ModeloAvaliacao value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/ModeloAvaliacao/5
       public Object Put(int id, [FromBody]ModeloAvaliacao value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            ModeloAvaliacao modAvaliacao = unidTrabalho.ObterPorId<ModeloAvaliacao>(id);
            modAvaliacao.EnunciadosModelo = value.EnunciadosModelo;
            modAvaliacao.Nome = value.Nome;
            modAvaliacao.QtdEnunciadosModelo = value.QtdEnunciadosModelo;
            modAvaliacao.TempoAvaliacao = value.TempoAvaliacao;
            unidTrabalho.Atualizar(modAvaliacao);
            unidTrabalho.Commit();
            return Json(formatarDados(modAvaliacao));
       }

       // DELETE: api/ModeloAvaliacao/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            ModeloAvaliacao modAvaliacao = unidTrabalho.ObterPorId<ModeloAvaliacao>(id);
            modAvaliacao.DataDesativacao = DateTime.Now;
            unidTrabalho.Salvar(modAvaliacao);
            unidTrabalho.Commit();

            return Json(formatarDados(modAvaliacao));
       }
   }
}
