using Globalsys;
using System;
using System.Linq;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;

namespace Plataforma.API.Controllers
{
    [RoutePrefix("api/Hierarquia")]
   public class HierarquiaController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public HierarquiaController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           HierarquiaModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(Hierarquia data)
       {
            return new
            {
                data.Codigo,
                data.Nome,
                data.DataCadastro

           };
       }
       // GET: api/Hierarquia
       [Authorize]
       public Object Get()
       {

            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<Hierarquia> hierarquia = unidTrabalho.ObterTodos<Hierarquia>();
            unidTrabalho.Commit();

            return Json(hierarquia.ToList().Select(p => new
           {
               p.Codigo,
               p.Nome,
               p.DataCadastro
           }));
       }

       // GET: api/Hierarquia/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Hierarquia hierarquia = unidTrabalho.ObterPorId<Hierarquia>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(hierarquia));
       }

       // POST: api/Hierarquia
       public Object Post([FromBody]Hierarquia value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/Hierarquia/5
       public Object Put(int id, [FromBody]Hierarquia value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Hierarquia hierarquia = unidTrabalho.ObterPorId<Hierarquia>(id);
            hierarquia.Nome = value.Nome;
            unidTrabalho.Atualizar(hierarquia);
            unidTrabalho.Commit();
            return Json(formatarDados(hierarquia));
       }

       // DELETE: api/Hierarquia/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Hierarquia hierarquia = unidTrabalho.ObterPorId<Hierarquia>(id);
            hierarquia.DataDesativacao = DateTime.Now;
            UnidadeTrabalho.Atualizar(hierarquia);
            unidTrabalho.Commit();

            return Json(formatarDados(hierarquia));
       }
   }
}
