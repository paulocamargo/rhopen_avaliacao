using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.API.Models;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/AvaliacaoCliente")]
   public class AvaliacaoClienteController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public AvaliacaoClienteController()
       {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            AvaliacaoModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
        }
       private Object formatarDados(AvaliacaoCliente data)
       {
           return new
           {
               data.Codigo

           };
       }
       // GET: api/AvaliacaoCliente
       [Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<AvaliacaoCliente> avalCliente = unidTrabalho.ObterTodos<AvaliacaoCliente>().Where(x => x.DataDesativado == null); ;
            unidTrabalho.Commit();
            return Json(avalCliente.ToList().Select(p => new
           {
               p.Codigo
           }));
       }

       // GET: api/AvaliacaoCliente/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            AvaliacaoCliente avaliacaoCliente = unidTrabalho.ObterPorId<AvaliacaoCliente>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(avaliacaoCliente));
       }

       // POST: api/AvaliacaoCliente
       public Object Post([FromBody]AvaliacaoCliente value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/AvaliacaoCliente/5
       public Object Put(int id, [FromBody]AvaliacaoCliente value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            AvaliacaoCliente avalCliente = unidTrabalho.ObterPorId<AvaliacaoCliente>(value.Codigo);
            avalCliente.PessoaFisica = value.PessoaFisica;
            avalCliente.PessoaJuridica = value.PessoaJuridica;
            unidTrabalho.Atualizar(avalCliente);
            unidTrabalho.Commit();
            return Json(formatarDados(avalCliente));
       }

       // DELETE: api/AvaliacaoCliente/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            AvaliacaoCliente avalCliente = unidTrabalho.ObterPorId<AvaliacaoCliente>(id);
            avalCliente.DataDesativado = DateTime.Now;
            unidTrabalho.Atualizar(avalCliente);
            unidTrabalho.Commit();
            return Json(formatarDados(avalCliente));
       }
   }
}
