﻿using Plataforma.API.Helpers.Globalsys;
using Plataforma.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Plataforma.API.Controllers
{
    [RoutePrefix("api/Menu")]
    public class MenuController : BaseApiController
    {
        [Authorize]
        public Object Get()
        {
            //new Exception("error");
            return Json(MenuModel.LoadMenuUsuario());
        }
    }
}
