using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.API.Models.Seguranca;
using Plataforma.Infra;
using Globalsys.Extensoes;
using Plataforma.Dominio.Sistema;
using Plataforma.API.Models;
using Plataforma.Persistencia;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/Categoria")]
   public class CategoriaController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public CategoriaController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           CategoriaModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(Categoria data)
       {
            return new
            {
                data.Codigo,
                data.Descricao,
                data.Nome,
                DataDeCadastro = data.DataCadastro.ToString(),
                CodigoRelacionalCategoria = ((data.CategoriaRelacionada != null) ? data.CategoriaRelacionada.Codigo : 0),
                DescCategoriaRelacionada = ((data.CategoriaRelacionada != null) ? data.CategoriaRelacionada.Descricao : string.Empty)
            };
       }
       // GET: api/Categoria
       //[Authorize]
       public Object Get()
       {

            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<Categoria> categorias = unidTrabalho.ObterTodos<Categoria>();
            unidTrabalho.Commit();
           return Json(categorias.ToList().Select(p => new
           {
               p.Codigo,
               p.Nome,
               p.Descricao,
               DataCadastro = p.DataCadastro.ToString(),
               CodigoRelacionalCategoria = ((p.CategoriaRelacionada != null) ? p.CategoriaRelacionada.Codigo : 0),
               DescCategoriaRelacionada = ((p.CategoriaRelacionada != null) ? p.CategoriaRelacionada.Descricao : string.Empty)

           }));
            
       }

       // GET: api/Categoria/5
       public Object Get(int id)
       {

            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Categoria categorias = unidTrabalho.ObterPorId<Categoria>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(categorias));
       }

       // POST: api/Categoria
       public Object Post([FromBody]Categoria value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/Categoria/5
       public Object Put(int id, [FromBody]Categoria value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Categoria categorias = unidTrabalho.ObterPorId<Categoria>(id);
            categorias.CategoriaRelacionada = value.CategoriaRelacionada;
            categorias.Descricao = value.Descricao;
            categorias.Nome = value.Nome;
            unidTrabalho.Atualizar(categorias);
            unidTrabalho.Commit();
            return Json(formatarDados(categorias));
       }

       // DELETE: api/Categoria/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Categoria categorias = unidTrabalho.ObterPorId<Categoria>(id);
            categorias.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(categorias);
            unidTrabalho.Commit();
            return Json(formatarDados(categorias));
       }
   }
}
