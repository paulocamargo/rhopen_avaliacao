using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.API.Models.Seguranca;
using Plataforma.Infra;
using Globalsys.Extensoes;
using Plataforma.Dominio.Seguranca;
using Plataforma.Repositorios.Seguranca;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/Usuario")]
   public class UsuarioController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public UsuarioController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           UsuarioModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(Usuario data)
       {
           return new
           {
               data.Codigo,
               data.Nome,
               data.Login,
               data.Senha,
               data.CPF,
               CPFFormatado = data.CPF.ToCPFFormat(),
               data.Email,
               GrupoUsuario = data.GrupoUsuario.Codigo,
               DescGrupoUsuario = data.GrupoUsuario.Nome

           };
       }
       // GET: api/Usuario
       [Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<Usuario> user = unidTrabalho.ObterTodos<Usuario>();
            unidTrabalho.Commit();

            return Json(user.ToList().Select(p => new
           {
               p.Codigo,
               p.Nome,
               p.Login,
               p.Senha,
               p.CPF,
               CPFFormatado = p.CPF.ToCPFFormat(),
               p.Email,
               DescGrupoUsuario = p.GrupoUsuario.Nome
           }));
       }

       // GET: api/Usuario/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Usuario user = unidTrabalho.ObterTodos<Usuario>().Where(x => x.Codigo == id).FirstOrDefault();
            unidTrabalho.Commit();

            return Json(formatarDados(user));
       }

       // POST: api/Usuario
       public Object Post([FromBody]Usuario value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/Usuario/5
       public Object Put(int id, [FromBody]Usuario value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Usuario user = unidTrabalho.ObterTodos<Usuario>().Where(x => x.Codigo == id).FirstOrDefault();
            user.CPF = value.CPF;
            user.Email = value.Email;
            user.GrupoUsuario = value.GrupoUsuario;
            user.Login = value.Login;
            user.Nome = value.Nome;
            user.Senha = value.Senha;
            unidTrabalho.Atualizar(user);
            return Json(formatarDados(user));
       }

       // DELETE: api/Usuario/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            Usuario user = unidTrabalho.ObterTodos<Usuario>().Where(x => x.Codigo == id).FirstOrDefault();
            user.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(user);
            unidTrabalho.Commit();

            return Json(formatarDados(user));
       }

        [HttpPost]
        [Route("RegistraPrimeiroAcessoUsuario")]
        public Object RegistraPrimeiroAcessoUsuario(Usuario usuario)
        {
            return Json(formatarDados(UsuarioModel.Instancia.RegistraPrimeiroAcessoUsuario(usuario)));
        }

        [HttpGet]
        [Route("ObterNomeDoUsuarioLogado")]
        public Object ObterNomeDoUsuarioLogado()
        {
            return Json(Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho).ObterNomeDoUsuarioLogado());
        }
    }
}
