using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Plataforma.API.Helpers.Globalsys;
using Plataforma.Infra;
using Plataforma.Dominio.Seguranca;
using Plataforma.API.Models;

namespace Plataforma.API.Controllers
{
   [RoutePrefix("api/GrupoUsuario")]
   public class GrupoUsuarioController : BaseApiController
   {
       public IUnidadeTrabalho UnidadeTrabalho { get; set; }

       public GrupoUsuarioController()
       {
           UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
           GrupoUsuarioModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
       }
       private Object formatarDados(GrupoUsuario data)
       {
           return new
           {
               data.Codigo,
               data.Nome,
               data.NivelAcesso,
               DataCadastro = data.DataCadastro.ToString(),
               data.DataDesativacao

           };
       }
       // GET: api/GrupoUsuario
       [Authorize]
       public Object Get()
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            IQueryable<GrupoUsuario> grupoUsuario = unidTrabalho.ObterTodos<GrupoUsuario>();
            unidTrabalho.Commit();

            return Json(grupoUsuario.ToList().Select(p => new
           {
               p.Codigo,
               p.Nome,
               p.NivelAcesso,
               DataCadastro = p.DataCadastro.ToString()
           }));
       }

       // GET: api/GrupoUsuario/5
       public Object Get(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            GrupoUsuario grupoUsuario = unidTrabalho.ObterPorId<GrupoUsuario>(id);
            unidTrabalho.Commit();
            return Json(formatarDados(GrupoUsuarioModel.Instancia.Editar(id)));
       }

       // POST: api/GrupoUsuario
       public Object Post([FromBody]GrupoUsuario value)
       {

            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            unidTrabalho.Salvar(value);
            unidTrabalho.Commit();
            return Json(formatarDados(value));
       }

       // PUT: api/GrupoUsuario/5
       public Object Put(int id, [FromBody]GrupoUsuario value)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            GrupoUsuario grupoUsuario = unidTrabalho.ObterPorId<GrupoUsuario>(id);
            grupoUsuario.NivelAcesso = value.NivelAcesso;
            grupoUsuario.Nome = value.Nome;
            unidTrabalho.Atualizar(grupoUsuario);
            unidTrabalho.Commit();
            return Json(formatarDados(grupoUsuario));
       }

       // DELETE: api/GrupoUsuario/5
       public Object Delete(int id)
       {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            GrupoUsuario grupoUsuario = unidTrabalho.ObterPorId<GrupoUsuario>(id);
            grupoUsuario.DataDesativacao = DateTime.Now;
            unidTrabalho.Atualizar(grupoUsuario);
            unidTrabalho.Commit();
            return Json(formatarDados(grupoUsuario));
       }
   }
}
