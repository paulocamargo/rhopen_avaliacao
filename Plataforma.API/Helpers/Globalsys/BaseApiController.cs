﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Globalsys;
using Globalsys.Mvc;
using Plataforma.Infra;
using Plataforma.API.Models.Seguranca;

namespace Plataforma.API.Helpers.Globalsys
{
    public class BaseApiController : ApiController, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public BaseApiController()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            UsuarioModel.Instancia.UnidadeDeTrabalho = UnidadeTrabalho;
        }
    }
}