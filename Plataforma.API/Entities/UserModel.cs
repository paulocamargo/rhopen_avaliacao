﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Plataforma.API.Entities
{
    public class UserModel
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}