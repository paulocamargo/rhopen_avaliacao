﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Plataforma.API.Entities;
using System.Collections.Generic;
using Plataforma.API.Providers;
using Microsoft.Owin.Security.OAuth;
using Plataforma.API.Util;
using System.Linq;
using System.Web.Http;

[assembly: OwinStartup(typeof(Plataforma.API.Startup))]

namespace Plataforma.API
{
    public class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        //public static GoogleOAuth2AuthenticationOptions googleAuthOptions { get; private set; }
        //public static FacebookAuthenticationOptions facebookAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
          


           // GlobalHost.HubPipeline.RequireAuthentication();
            /*  app.Map("/signalr", map =>
              {
                  // Setup the CORS middleware to run before SignalR.
                  // By default this will allow all origins. You can 
                  // configure the set of origins and/or http verbs by
                  // providing a cors options with a different policy.
                  map.UseCors(CorsOptions.AllowAll);
                  var hubConfiguration = new HubConfiguration
                  {
                      // You can enable JSONP by uncommenting line below.
                      // JSONP requests are insecure but some older browsers (and some
                      // versions of IE) require JSONP to work cross domain
                      // EnableJSONP = true
                  };
                  // Run the SignalR pipeline. We're not using MapSignalR
                  // since this branch already runs under the "/signalr"
                  // path.
                  map.RunSignalR(hubConfiguration);
              });
              GlobalHost.HubPipeline.RequireAuthentication();*/
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            //use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {

                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(12),
                Provider = new SimpleAuthorizationServerProvider(),
                RefreshTokenProvider = new SimpleRefreshTokenProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            /* //Configure Google External Login
             googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
             {
                 ClientId = "xxxxxx",
                 ClientSecret = "xxxxxx",
                 Provider = new GoogleAuthProvider()
             };
             app.UseGoogleAuthentication(googleAuthOptions);

             //Configure Facebook External Login
             facebookAuthOptions = new FacebookAuthenticationOptions()
             {
                 AppId = "xxxxxx",
                 AppSecret = "xxxxxx",
                 Provider = new FacebookAuthProvider()
             };
             app.UseFacebookAuthentication(facebookAuthOptions);*/
            InitializeData();
        }
        private static List<Client> BuildClientsList()
        {

            List<Client> ClientsList = new List<Client>
            {
                new Client
                { Id = "ngAuthApp",
                    Secret= Helper.GetHash("abc@123"),
                    Name="Globalsys Soluções em TI - Front-end Application",
                    ApplicationType =  ApplicationTypes.JavaScript,
                    Active = true,
                    RefreshTokenLifeTime = 7200,
                    AllowedOrigin = "*" //COLOCAR O DOMINIO DA APLICAÇÂO
                },
                new Client
                { Id = "consoleApp",
                    Secret=Helper.GetHash("123@abc"),
                    Name="Globalsys Soluções em TI - Aplicativo",
                    ApplicationType = ApplicationTypes.NativeConfidential,
                    Active = true,
                    RefreshTokenLifeTime = 14400,
                    AllowedOrigin = "*"
                }
            };

            return ClientsList;
        }
        private void InitializeData()
        {
            using (AuthContext context = new AuthContext())
            {
                if (context.Clients.Count() > 0)
                {
                    return;
                }

                context.Clients.AddRange(BuildClientsList());
                context.SaveChanges();

            }
        }
    }

}
