﻿using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Model;
using Globalsys.Validacao;
using Plataforma.Dominio.Seguranca;
using Plataforma.Infra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Extensoes;
using Plataforma.Repositorios.Seguranca;

namespace Plataforma.API.Models.Seguranca
{
    public class UsuarioColaborador
    {
        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Login { get; set; }
        public virtual string Email { get; set; }
        public virtual string Cpf { get; set; }
        public virtual bool LoginAd { get; set; }
    }

    public class UsuarioTO
    {
        public virtual int Codigo { get; set; }
        public virtual string Email { get; set; }
        public virtual string Cpf { get; set; }
        public virtual string SenhaAntiga { get; set; }
        public virtual string SenhaNova { get; set; }
        public virtual string ConfirmacaoSenha { get; set; }
    }


    public class UsuarioModel : IModel
    {
        private static UsuarioModel usuarioModel { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }
        /// <summary>
        /// Singleton.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Instancia.
        /// </value>
        public static UsuarioModel Instancia
        {
            get
            {
                if (usuarioModel == null)
                    usuarioModel = new UsuarioModel();

                return usuarioModel;
            }
        }

        public string ObterNomeDoUsuarioLogado()
        {
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            return Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidadeTrabalho).ObterNomeDoUsuarioLogado();
        }

        public Usuario ObterUsuarioLogado()
        {
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            return Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidadeTrabalho).ObterUsuarioLogado();
        }

        public Dominio.Seguranca.Usuario Login(string login, string senha)
        {
            //Chamar unidade de trabalho desta forma pois esse metodo será chamado de outros lugares
            IUnidadeTrabalho contexto = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            senha = Util.Helper.GetHash(senha);

            var userList = contexto.ObterTodos<Dominio.Seguranca.Usuario>();
            //return contexto.ObterTodos<Dominio.Seguranca.Usuario>().Where(t => t.Login.Equals(login) && t.Senha.Equals(senha)).FirstOrDefault();
            foreach (var item in userList)
            {
                if (item.Login.Equals(login) && item.Senha.Equals(senha))
                    return item;
            }
            return null;
            
        }
        public Dominio.Seguranca.Usuario UsuarioLogado()
        {
            //Chamar unidade de trabalho desta forma pois esse metodo será chamado de outros lugares
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            return Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidadeTrabalho).ObterUsuarioLogado();
        }
        public Dominio.Seguranca.Usuario UsuariPossuiLogin(string login)
        {
            //Chamar unidade de trabalho desta forma pois esse metodo será chamado de outros lugares
            IUnidadeTrabalho contexto = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            //return contexto.ObterTodos<Dominio.Seguranca.Usuario>().Where(t => t.Login.Equals(login)).FirstOrDefault();
            var userList = contexto.ObterTodos<Dominio.Seguranca.Usuario>();

            foreach (var item in userList)
            {
                if (item.Login.Equals(login))
                    return item;
            }
            return null;
        }

        public IQueryable<Dominio.Seguranca.Usuario> Consultar(string parametro)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Dominio.Seguranca.Usuario> query = contexto.ObterTodos<Dominio.Seguranca.Usuario>();
            if (!parametro.IsEmpty())
                query = query.Where(p => p.Login.Contains(parametro));

            return query;
        }
        public Usuario Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Usuario usuario = contexto.ObterPorId<Usuario>(codigo);
            usuario.DataDesativacao = DateTime.Today;
            //ColaboradorModel.Instancia.UnidadeDeTrabalho = UnidadeDeTrabalho;
            //ColaboradorModel.Instancia.Deletar(usuario.Codigo);
            contexto.BeginTransaction();
            contexto.Atualizar(usuario);
            contexto.Commit();

            return usuario;
        }

        public Usuario Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<Usuario>(codigo);
        }

        public Usuario Atualizar(Usuario usuarioColaborador, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Usuario usuarioA = contexto.ObterPorId<Usuario>(codigo);
            Usuario usuario = new Usuario();
            usuario.Login = usuarioColaborador.Login;
            usuario.Codigo = usuarioColaborador.Codigo;
            usuario.Nome = usuarioColaborador.Nome;
            usuario.Email = usuarioColaborador.Email;
            usuario.CPF = usuarioColaborador.CPF;
            ColaboradorModel.Instancia.UnidadeDeTrabalho = UnidadeDeTrabalho;
            ColaboradorModel.Instancia.Atualizar(usuario, usuario.Codigo);

            ValidarCampos(usuario);
            ValidarRegras(usuario, EstadoObjeto.Alterado);
            contexto.BeginTransaction();
            usuarioA.Login = usuario.Login;
            contexto.Atualizar(usuarioA);
            contexto.Commit();

            return usuarioA;
        }

        public Dominio.Seguranca.Usuario Cadastrar(Usuario usuarioColaborador)
        {

            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            string senha = Globalsys.Util.Tools.GerarSenhaAleatoria();
      
            /*******************USÚARIO******************/
            Usuario usuario = new Usuario();
            usuario.Login = usuarioColaborador.Login;
            usuario.Senha = Util.Helper.GetHash(senha);
            usuario.Nome = usuarioColaborador.Nome;
            usuario.CPF = usuarioColaborador.CPF;
            usuario.PrimeiroAcesso = true;
            usuario.Email = usuarioColaborador.Email;
            usuario.DataCadastro = DateTime.Now;
            usuario.DataDesativacao = null;
            usuario.GrupoUsuario = usuarioColaborador.GrupoUsuario;

            /*******************USÚARIO******************/
            ValidarCampos(usuario);
            ValidarRegras(usuario, EstadoObjeto.Novo);
            ColaboradorModel.Instancia.UnidadeDeTrabalho = UnidadeDeTrabalho;

            //Colaborador novoColaborador = ColaboradorModel.Instancia.Cadastrar(colaborador);

            contexto.Salvar<Dominio.Seguranca.Usuario>(usuario);

            return usuario;
        }

        public Usuario RegistraPrimeiroAcessoUsuario(Usuario usuario)
        {
            IUnidadeTrabalho contexto = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var userList = contexto.ObterTodos<Dominio.Seguranca.Usuario>();
            Usuario user = null;

            foreach (var item in userList)
            {
                if (item.Login.Equals(usuario.Login))
                    user =  item;
            }

            if (user == null)
            {
                throw new CoreException("Não foi possível validar o usuário, favor verificar os dados informados!");
            }
            else

            {
                user.Senha = Util.Helper.GetHash(usuario.Senha);
                user.PrimeiroAcesso = false;

                contexto.BeginTransaction();
                contexto.Atualizar<Usuario>(user);
                contexto.Commit();

                user.Senha = usuario.Senha;
            }



            return user;
        }

        public bool ValidarSenhaUsuario(Usuario usuario, string senha)
        {
            senha = Util.Helper.GetHash(senha);
            return usuario.Senha.Equals(senha);
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = null)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Usuario usuario = (Usuario)objeto;
            switch ((EstadoObjeto)estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (contexto.ObterTodos<Usuario>().Any(t => t.Login.Equals(usuario.Login) && t.DataDesativacao == null))
                        throw new CoreException("Login já cadastrado no sistema.");
                    break;
                case EstadoObjeto.Alterado:
                    if (contexto.ObterTodos<Usuario>().Any(t => t.Login.Equals(usuario.Login) && t.Codigo != usuario.Codigo && t.DataDesativacao == null))
                        throw new CoreException("Login já cadastrado no sistema.");
                    if (contexto.ObterTodos<Usuario>().Any(t => t.Login.Equals(usuario.Login) && t.Codigo != usuario.Codigo && t.DataDesativacao == null))
                        throw new CoreException("Login já cadastrado no sistema.");
                    break;
            }

        }

        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = null)
        {
            Usuario usuario = (Usuario)objeto;
            if (!usuario.CPF.ClearCPFFormat().IsValidCPF())
                throw new CoreException("O campo Login precisa de um CPF válido.");
        }

        public UsuarioTO AlterarSenha(UsuarioTO usuario)
        {
            if (!usuario.SenhaNova.Equals(usuario.ConfirmacaoSenha))
            {
                throw new CoreException("Confirmação de senha não confere com a nova senha.");
            }

            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Usuario usu = contexto.ObterTodos<Usuario>().Where(x => x.Senha.Equals(Util.Helper.GetHash(usuario.SenhaAntiga))).FirstOrDefault();

            if (usu != null)
            {
                contexto.BeginTransaction();
                usu.Senha = Util.Helper.GetHash(usuario.SenhaNova);
                contexto.Atualizar<Usuario>(usu);
                contexto.Commit();
            }
            else
            {
                throw new CoreException("Dados incorretos. Favor verificar a senha antiga.");
            }

            return usuario;
        }

        public bool EsqueciSenha(UsuarioTO usuario)
        {

            IUnidadeTrabalho contexto = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            string novaSenha = Globalsys.Util.Tools.GerarSenhaAleatoria();
            Usuario usu = contexto.ObterTodos<Usuario>().Where(x => x.CPF == usuario.Cpf && x.Email == usuario.Email).FirstOrDefault();

            if (usu != null)
            {

                contexto.BeginTransaction();
                usu.Senha = Util.Helper.GetHash(novaSenha);
                contexto.Atualizar<Usuario>(usu);
                contexto.Commit();
                Dictionary<string, string> valoresSubs = new Dictionary<string, string>();
                valoresSubs.Add("$$NovaSenha$$", novaSenha);
                Globalsys.Util.Email.Enviar("[RHOpen Plataforma] - Nova senha de acesso",
                                                            HttpContext.Current.Server.MapPath("/Content/templates/esqueciSenha.htm"),
                                                            valoresSubs,
                                                            string.Empty,
                                                            null,
                                                            System.Web.HttpContext.Current.Server.MapPath("/Content/images/logo-Vix.png"),
                                                            null,
                                                            usu.TryGetValue(v => v.Email));
            }
            else
            {
                throw new CoreException("Informações incorretas.");
            }

            return true;
        }
    }
}