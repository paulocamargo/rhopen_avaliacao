using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Seguranca;

namespace Plataforma.API.Models
{
    public class GrupoUsuarioModel : IModel
    {
        private static GrupoUsuarioModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static GrupoUsuarioModel Instancia
        {
            get
            {
                if (model == null)
                    model = new GrupoUsuarioModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public GrupoUsuario Cadastrar(GrupoUsuario data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<GrupoUsuario>(data);
            return data;
        }

        public IQueryable<GrupoUsuario> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<GrupoUsuario> query = contexto.ObterTodos<GrupoUsuario>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public GrupoUsuario Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<GrupoUsuario>(codigo);
        }

        public GrupoUsuario Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            GrupoUsuario data = contexto.ObterPorId<GrupoUsuario>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public GrupoUsuario Atualizar(GrupoUsuario data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            GrupoUsuario dataA = contexto.ObterPorId<GrupoUsuario>(codigo);
            dataA.Nome = data.Nome;
            dataA.NivelAcesso = data.NivelAcesso;
            
            contexto.BeginTransaction();
            contexto.Atualizar<GrupoUsuario>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
