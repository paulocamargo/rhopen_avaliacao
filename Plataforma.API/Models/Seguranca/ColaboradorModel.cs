﻿using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Model;
using Globalsys.Validacao;
using Plataforma.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Extensoes;

namespace Plataforma.API.Models.Seguranca
{
    public class ColaboradorModel : IModel
    {

        private static ColaboradorModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }
        /// <summary>
        /// Singleton.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Instancia.
        /// </value>
        public static ColaboradorModel Instancia
        {
            get
            {
                if (model == null)
                    model = new ColaboradorModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            Colaborador colaborador = (Colaborador)objeto;
            if (colaborador.Nome.IsEmpty())
                throw new CoreException("O campo \"Nome\" é obrigatório.");
            if (colaborador.Email.IsEmpty())
                throw new CoreException("O campo \"E-mail\" é obrigatório.");
            if (colaborador.Cpf.IsEmpty())
                throw new CoreException("O campo \"CPF\" é obrigatório.");
            if (!colaborador.Email.IsValidEmail())
                throw new CoreException("Endereço de E-mail inválido.");
            if (!colaborador.Cpf.IsValidCPF())
                throw new CoreException("Número de CPF inválido.");

        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Colaborador colaborador = (Colaborador)objeto;
            switch ((EstadoObjeto)estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (contexto.ObterTodos<Colaborador>().Any(t => t.Cpf.Equals(colaborador.Cpf)))
                        throw new CoreException("CPF já cadastrado no sistema.");
                    if (contexto.ObterTodos<Colaborador>().Any(t => t.Email.Equals(colaborador.Email)))
                        throw new CoreException("E-mail já cadastrado no sistema.");

                    break;
                case EstadoObjeto.Alterado:
                    if (contexto.ObterTodos<Colaborador>().Any(t => t.Cpf.Equals(colaborador.Cpf) && t.Codigo != colaborador.Codigo))
                        throw new CoreException("CPF já cadastrado no sistema.");
                    if (contexto.ObterTodos<Colaborador>().Any(t => t.Email.Equals(colaborador.Email) && t.Codigo != colaborador.Codigo))
                        throw new CoreException("E-mail já cadastrado no sistema.");

                    break;
            }
        }

        public Usuario Atualizar(Usuario colaborador, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            ValidarCampos(colaborador);
            ValidarRegras(colaborador, EstadoObjeto.Alterado);
            Usuario colaboradorA = contexto.ObterPorId<Usuario>(codigo);
            colaboradorA.CPF = colaborador.CPF;
            colaboradorA.Email = colaborador.Email;
            colaboradorA.Nome = colaborador.Nome;
            contexto.BeginTransaction();
            contexto.Atualizar(colaboradorA);
            contexto.Commit();
            return colaboradorA;

        }

        public Colaborador Cadastrar(Colaborador colaborador)
        {
            ValidarCampos(colaborador, EstadoObjeto.Novo);
            ValidarRegras(colaborador, EstadoObjeto.Novo);
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            contexto.Salvar<Colaborador>(colaborador);
            return colaborador;
        }

        public Colaborador Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Colaborador usuario = contexto.ObterPorId<Colaborador>(codigo);
            usuario.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(usuario);
            contexto.Commit();

            return usuario;
        }
    }
}