using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class ModeloAvaliacaoModel : IModel
    {
        private static ModeloAvaliacaoModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static ModeloAvaliacaoModel Instancia
        {
            get
            {
                if (model == null)
                    model = new ModeloAvaliacaoModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public ModeloAvaliacao Cadastrar(ModeloAvaliacao data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<ModeloAvaliacao>(data);
            return data;
        }

        public IQueryable<ModeloAvaliacao> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<ModeloAvaliacao> query = contexto.ObterTodos<ModeloAvaliacao>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public ModeloAvaliacao Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<ModeloAvaliacao>(codigo);
        }

        public ModeloAvaliacao Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            ModeloAvaliacao data = contexto.ObterPorId<ModeloAvaliacao>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public ModeloAvaliacao Atualizar(ModeloAvaliacao data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            ModeloAvaliacao dataA = contexto.ObterPorId<ModeloAvaliacao>(codigo);

            dataA.EnunciadosModelo = data.EnunciadosModelo;
            dataA.Nome = data.Nome;
            dataA.QtdEnunciadosModelo = data.QtdEnunciadosModelo;
            dataA.TempoAvaliacao = data.TempoAvaliacao;
            contexto.BeginTransaction();
            contexto.Atualizar<ModeloAvaliacao>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
