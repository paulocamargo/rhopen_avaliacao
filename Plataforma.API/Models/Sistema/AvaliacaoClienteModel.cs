using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;
using Plataforma.Infra;

namespace Plataforma.API.Models
{
    public class AvaliacaoClienteModel : IModel
    {
        private static AvaliacaoClienteModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static AvaliacaoClienteModel Instancia
        {
            get
            {
                if (model == null)
                    model = new AvaliacaoClienteModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public AvaliacaoCliente Cadastrar(AvaliacaoCliente data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadatro = DateTime.Now;
            contexto.Salvar<AvaliacaoCliente>(data);
            return data;
        }

        public IQueryable<AvaliacaoCliente> Consultar()
        {
            IUnidadeTrabalho contexto = Fabrica.Instancia.Obter<IUnidadeTrabalho>(); 
            IQueryable<AvaliacaoCliente> query = contexto.ObterTodos<AvaliacaoCliente>().Where(x => x.DataDesativado == null);

            return query;
        }

        public AvaliacaoCliente Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<AvaliacaoCliente>(codigo);
        }

        public AvaliacaoCliente Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            AvaliacaoCliente data = contexto.ObterPorId<AvaliacaoCliente>(codigo);
            data.DataDesativado = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public AvaliacaoCliente Atualizar(AvaliacaoCliente data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            AvaliacaoCliente dataA = contexto.ObterPorId<AvaliacaoCliente>(codigo);

            contexto.BeginTransaction();
            contexto.Atualizar<AvaliacaoCliente>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
