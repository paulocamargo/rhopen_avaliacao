using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class TesteAvaliacaoModel : IModel
    {
        private static TesteAvaliacaoModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static TesteAvaliacaoModel Instancia
        {
            get
            {
                if (model == null)
                    model = new TesteAvaliacaoModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public TesteAvaliacao Cadastrar(TesteAvaliacao data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            //data.DataCadastro = DateTime.Now;
            contexto.Salvar<TesteAvaliacao>(data);
            return data;
        }

        public IQueryable<EnunciadoAvaliacao> Consultar(int codigoModeloAvaliacao)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            //IQueryable<TesteAvaliacao> query = contexto.ObterTodos<TesteAvaliacao>();

            IQueryable<EnunciadoAvaliacao> query = contexto.ObterTodos<EnunciadoAvaliacao>().Where(x=>x.ModeloAvaliacao.Codigo == codigoModeloAvaliacao);

            return query;
        }

        public IQueryable<TesteAvaliacao> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<TesteAvaliacao> query = contexto.ObterTodos<TesteAvaliacao>();

            
            return query;
        }

        public TesteAvaliacao Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<TesteAvaliacao>(codigo);
        }

        public TesteAvaliacao Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            TesteAvaliacao data = contexto.ObterPorId<TesteAvaliacao>(codigo);
            //data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public TesteAvaliacao Atualizar(TesteAvaliacao data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            TesteAvaliacao dataA = contexto.ObterPorId<TesteAvaliacao>(codigo);

            contexto.BeginTransaction();
            contexto.Atualizar<TesteAvaliacao>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
