using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class AvaliacaoModel : IModel
    {
        private static AvaliacaoModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static AvaliacaoModel Instancia
        {
            get
            {
                if (model == null)
                    model = new AvaliacaoModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public Avaliacao Cadastrar(Avaliacao data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            data.HoraInicio = DateTime.Now;
            contexto.Salvar<Avaliacao>(data);
           
            return data;
        }

        public IQueryable<Avaliacao> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Avaliacao> query = contexto.ObterTodos<Avaliacao>().Where(x => x.DataDesativacao == null && x.HoraFim == null);

            return query;
        }

        public IQueryable<Avaliacao> Consultar(int Codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Avaliacao> query = contexto.ObterTodos<Avaliacao>().Where(x => x.DataDesativacao == null && x.Codigo == Codigo);

            return query;
        }

        public Avaliacao Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<Avaliacao>(codigo);
        }

        internal IQueryable<Avaliacao> Consultar(PessoaFisica pessoafisica)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Avaliacao> query = contexto.ObterTodos<Avaliacao>().Where(x => x.DataDesativacao == null && x.HoraFim == null && x.ClientePessoaFisica.Codigo == pessoafisica.Codigo);

            return query;
        }

        internal IQueryable<Avaliacao> ConsultarAvaliacaoReltorio(PessoaFisica pessoafisica)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Avaliacao> query = contexto.ObterTodos<Avaliacao>().Where(x => x.DataDesativacao == null && x.HoraFim != null && x.ItensSelecionadosEnunciados != null && x.ClientePessoaFisica.Codigo == pessoafisica.Codigo).OrderBy(x=>x.HoraFim);

            return query;
        }

        internal IQueryable<Avaliacao> consultarEnunciadosRelatorio(PessoaFisica pessoafisica)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Avaliacao> query = contexto.ObterTodos<Avaliacao>().Where(x => x.DataDesativacao == null && x.HoraFim != null && x.ItensSelecionadosEnunciados != null && x.ClientePessoaFisica.Codigo == pessoafisica.Codigo).OrderBy(x => x.HoraFim);

            return query;
        }
        


        internal IQueryable<Avaliacao> ConsultarAvaliacaoTeste(PessoaFisica pessoafisica)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Avaliacao> query = contexto.ObterTodos<Avaliacao>().Where(x => x.DataDesativacao == null && x.HoraFim == null && x.ItensSelecionadosEnunciados == null && x.ClientePessoaFisica.Codigo == pessoafisica.Codigo);

            return query;
        }

        //internal IQueryable<Avaliacao> Consultar(PessoaJuridica  pessoajuridica)
        //{
        //    IUnidadeTrabalho contexto = UnidadeDeTrabalho;
        //    IQueryable<Avaliacao> query = contexto.ObterTodos<Avaliacao>().Where(x => x.DataDesativacao == null && x.HoraFim == null && x.ClientePessoaJuridica.Codigo == pessoajuridica.Codigo);

        //    return query;
        //}

        public Avaliacao Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Avaliacao data = contexto.ObterPorId<Avaliacao>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public Avaliacao Atualizar(Avaliacao data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Avaliacao dataA = contexto.ObterPorId<Avaliacao>(codigo);
            
            dataA.TempoTotalAvaliacao = (DateTime.Now.Subtract(data.HoraInicio)).Minutes.ToString().Replace("-", "");
            dataA.ItensSelecionadosEnunciados = data.ItensSelecionadosEnunciados;
            dataA.HoraInicio = DateTime.Now.AddSeconds( - Convert.ToDouble(dataA.TempoTotalAvaliacao));
            //dataA.ClientePessoaJuridica = null;
            dataA.HoraFim = DateTime.Now;

            contexto.BeginTransaction();
            contexto.Atualizar<Avaliacao>(dataA);
            contexto.Commit();
            return dataA;
        }

        public Avaliacao atualizarAvaliacaoRealizada(Avaliacao data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Avaliacao dataA = contexto.ObterPorId<Avaliacao>(codigo);
            dataA.HoraFim = DateTime.Now;
            //dataA.DataDesativacao = DateTime.Now;
            dataA.ItensSelecionadosEnunciados = data.ItensSelecionadosEnunciados;
            dataA.TempoTotalAvaliacao = (dataA.HoraFim - data.HoraInicio).ToString();
            contexto.BeginTransaction();
            contexto.Atualizar<Avaliacao>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
