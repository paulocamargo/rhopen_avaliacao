using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class CategoriaModel : IModel
    {
        private static CategoriaModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static CategoriaModel Instancia
        {
            get
            {
                if (model == null)
                    model = new CategoriaModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public Categoria Cadastrar(Categoria data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<Categoria>(data);
            return data;
        }

        public IQueryable<Categoria> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Categoria> query = contexto.ObterTodos<Categoria>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public Categoria Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<Categoria>(codigo);
        }

        public Categoria Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Categoria data = contexto.ObterPorId<Categoria>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public Categoria Atualizar(Categoria data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Categoria dataA = contexto.ObterPorId<Categoria>(codigo);
            dataA.Descricao = data.Descricao;
            dataA.CategoriaRelacionada = data.CategoriaRelacionada;
            dataA.Nome = data.Nome;

            contexto.BeginTransaction();
            contexto.Atualizar<Categoria>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
