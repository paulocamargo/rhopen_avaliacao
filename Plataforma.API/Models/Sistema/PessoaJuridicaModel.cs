using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class PessoaJuridicaModel : IModel
    {
        private static PessoaJuridicaModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static PessoaJuridicaModel Instancia
        {
            get
            {
                if (model == null)
                    model = new PessoaJuridicaModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public PessoaJuridica Cadastrar(PessoaJuridica data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<PessoaJuridica>(data);
            return data;
        }

        public IQueryable<PessoaJuridica> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<PessoaJuridica> query = contexto.ObterTodos<PessoaJuridica>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public PessoaJuridica Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<PessoaJuridica>(codigo);
        }

        public PessoaJuridica Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            PessoaJuridica data = contexto.ObterPorId<PessoaJuridica>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public PessoaJuridica Atualizar(PessoaJuridica data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            PessoaJuridica dataA = contexto.ObterPorId<PessoaJuridica>(codigo);

            contexto.BeginTransaction();
            contexto.Atualizar<PessoaJuridica>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
