using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class HierarquiaModel : IModel
    {
        private static HierarquiaModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static HierarquiaModel Instancia
        {
            get
            {
                if (model == null)
                    model = new HierarquiaModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public Hierarquia Cadastrar(Hierarquia data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<Hierarquia>(data);
            return data;
        }

        public IQueryable<Hierarquia> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Hierarquia> query = contexto.ObterTodos<Hierarquia>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public Hierarquia Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<Hierarquia>(codigo);
        }

        public Hierarquia Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Hierarquia data = contexto.ObterPorId<Hierarquia>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public Hierarquia Atualizar(Hierarquia data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Hierarquia dataA = contexto.ObterPorId<Hierarquia>(codigo);
            dataA.Nome = data.Nome;
            contexto.BeginTransaction();
            contexto.Atualizar<Hierarquia>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
