using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class EnunciadoModel : IModel
    {
        private static EnunciadoModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static EnunciadoModel Instancia
        {
            get
            {
                if (model == null)
                    model = new EnunciadoModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public Enunciado Cadastrar(Enunciado data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<Enunciado>(data);
            return data;
        }

        public IQueryable<Enunciado> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<Enunciado> query = contexto.ObterTodos<Enunciado>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public Enunciado Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<Enunciado>(codigo);
        }

        public Enunciado Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Enunciado data = contexto.ObterPorId<Enunciado>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public Enunciado Atualizar(Enunciado data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            Enunciado dataA = contexto.ObterPorId<Enunciado>(codigo);

            dataA.Categoria = data.Categoria;
            dataA.Identificador = data.Identificador;
            dataA.Nome = data.Nome;
            dataA.TimeEnunciado = data.TimeEnunciado;
            contexto.BeginTransaction();
            contexto.Atualizar<Enunciado>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
