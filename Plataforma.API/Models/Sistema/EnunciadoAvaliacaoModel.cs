using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;
using Plataforma.Infra;

namespace Plataforma.API.Models
{
    public class EnunciadoAvaliacaoModel : IModel
    {
        private static EnunciadoAvaliacaoModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static EnunciadoAvaliacaoModel Instancia
        {
            get
            {
                if (model == null)
                    model = new EnunciadoAvaliacaoModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public EnunciadoAvaliacao Cadastrar(EnunciadoAvaliacao data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<EnunciadoAvaliacao>(data);
            return data;
        }

        public IQueryable<EnunciadoAvaliacao> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<EnunciadoAvaliacao> query = contexto.ObterTodos<EnunciadoAvaliacao>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public IQueryable<EnunciadoAvaliacao> Consultar(ModeloAvaliacao modval)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<EnunciadoAvaliacao> query = contexto.ObterTodos<EnunciadoAvaliacao>().Where(x => x.DataDesativacao == null);// && x.ModeloAvaliacao.Codigo == modval.Codigo);

            return query;
        }

        //public IQueryable<EnunciadoAvaliacao> Consultar(Avaliacao modeloAvaliacao)
        //{
        //    IUnidadeTrabalho contexto = UnidadeDeTrabalho;
        //    //IQueryable<EnunciadoAvaliacao> query = contexto.ObterTodos<EnunciadoAvaliacao>().Where(x => x.DataDesativacao == null && x.ModeloAvaliacao.Codigo == modeloAvaliac;ao.ModeloAvaliacao.Codigo);
        //    IQueryable<EnunciadoAvaliacao> query = contexto.ObterTodos<EnunciadoAvaliacao>().Where(x => x.DataDesativacao == null && x.ModeloAvaliacao.Codigo == 7);

        //    return query;
        //}

        public Dominio.Sistema.EnunciadoAvaliacao Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<EnunciadoAvaliacao>(codigo);
        }

        public EnunciadoAvaliacao Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            EnunciadoAvaliacao data = contexto.ObterPorId<EnunciadoAvaliacao>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public EnunciadoAvaliacao Atualizar(EnunciadoAvaliacao data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            EnunciadoAvaliacao dataA = contexto.ObterPorId<EnunciadoAvaliacao>(codigo);

            contexto.BeginTransaction();
            contexto.Atualizar<EnunciadoAvaliacao>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
