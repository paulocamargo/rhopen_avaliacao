using Globalsys.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Globalsys.Validacao;
using Globalsys;
using Plataforma.Dominio.Sistema;

namespace Plataforma.API.Models
{
    public class PessoaFisicaModel : IModel
    {
        private static PessoaFisicaModel model { get; set; }
        public IUnidadeTrabalho UnidadeDeTrabalho { get; set; }

        public static PessoaFisicaModel Instancia
        {
            get
            {
                if (model == null)
                    model = new PessoaFisicaModel();

                return model;
            }
        }
        public void ValidarCampos(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public void ValidarRegras(object objeto, EstadoObjeto? estadoObjeto = default(EstadoObjeto?))
        {
            throw new NotImplementedException();
        }

        public PessoaFisica Cadastrar(PessoaFisica data)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            data.CPF = data.CPF.Replace(".", "").Replace("-", "");
            data.DataCadastro = DateTime.Now;
            contexto.Salvar<PessoaFisica>(data);
            return data;
        }

        public IQueryable<PessoaFisica> Consultar()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<PessoaFisica> query = contexto.ObterTodos<PessoaFisica>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public IQueryable<PessoaFisica> Consultar(string CPF)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<PessoaFisica> query = contexto.ObterTodos<PessoaFisica>().Where(x => x.DataDesativacao == null && x.CPF == CPF);

            return query;
        }

        public IQueryable<PessoaFisica> Consultar(PessoaFisica pessoafisica)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<PessoaFisica> query = contexto.ObterTodos<PessoaFisica>().Where(x => x.DataDesativacao == null);

            return query;
        }

        public IQueryable<PessoaFisica> ConsultarClinteCPF (string pessoafisica)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<PessoaFisica> query = contexto.ObterTodos<PessoaFisica>().Where(x => x.DataDesativacao == null && x.CPF == pessoafisica);

            return query;
        }

        public object ConsultarTodosClientes()
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            IQueryable<PessoaFisica> queryPF = contexto.ObterTodos<PessoaFisica>().Where(x => x.DataDesativacao == null);
            IQueryable<PessoaJuridica> queryPJ = contexto.ObterTodos<PessoaJuridica>().Where(x => x.DataDesativacao == null);
            return null;
        }

        public PessoaFisica Editar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            return contexto.ObterPorId<PessoaFisica>(codigo);
        }

        public PessoaFisica Deletar(int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            PessoaFisica data = contexto.ObterPorId<PessoaFisica>(codigo);
            data.DataDesativacao = DateTime.Today;
            contexto.BeginTransaction();
            contexto.Atualizar(data);
            contexto.Commit();
            return data;
        }

        public PessoaFisica Atualizar(PessoaFisica data, int codigo)
        {
            IUnidadeTrabalho contexto = UnidadeDeTrabalho;
            PessoaFisica dataA = contexto.ObterPorId<PessoaFisica>(codigo);
            dataA.CPF = data.CPF;
            dataA.EMAIL = data.EMAIL;
            dataA.DataNascimento = data.DataNascimento;
            dataA.Nome = data.Nome;
            dataA.NivelHierarquico = data.NivelHierarquico;


            contexto.BeginTransaction();
            contexto.Atualizar<PessoaFisica>(dataA);
            contexto.Commit();
            return dataA;
        }

    }
}
