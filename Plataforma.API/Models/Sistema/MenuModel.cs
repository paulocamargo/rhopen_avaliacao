﻿using Globalsys;
using Plataforma.Dominio.Seguranca;
using Plataforma.Infra;
using Plataforma.Repositorios.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Plataforma.API.Models
{
    public class MenuModel
    {

        public static List<Menu> LoadMenu()
        {
            List<Menu> data = new List<Menu>();
            
            Menu menuHeading = new Menu();
            menuHeading.text = "Menu Heading";
            menuHeading.heading = true;
            menuHeading.translate = "RHOPEN";
            data.Add(menuHeading);
            //Dados de segurança
            Menu menuSegurança = new Menu();
            menuSegurança.text = "Segurança";
            menuSegurança.sref = "#";
            menuSegurança.icon = "icon-key";
            
            Menu menuUsuario = new Menu();
            menuUsuario.text = "Usuário";
            menuUsuario.sref = "app.seguranca-usuario";
            menuSegurança.submenu.Add(menuUsuario);

            Menu menuGrupo = new Menu();
            menuGrupo.text = "Grupos de Usuário";
            menuGrupo.sref = "app.seguranca-grupo";
            menuSegurança.submenu.Add(menuGrupo);

            data.Add(menuSegurança);
            //Dados de Cadastro do Sistema
            Menu menuSistema = new Menu();
            menuSistema.text = "Sistema";
            menuSistema.sref = "#";
            menuSistema.icon = "icon-organization";

            Menu menuSistemaCategoria = new Menu();
            menuSistemaCategoria.text = "Categorias";
            menuSistemaCategoria.sref = "app.sistema-categoria";
            menuSistema.submenu.Add(menuSistemaCategoria);

            Menu menuSistemaEnunciado = new Menu();
            menuSistemaEnunciado.text = "Enunciado";
            menuSistemaEnunciado.sref = "app.sistema-enunciado";
            menuSistema.submenu.Add(menuSistemaEnunciado);

            data.Add(menuSistema);
            
            Menu menuSistemaCliente = new Menu();
            menuSistemaCliente.text = "Clientes";
            menuSistemaCliente.sref = "#";
            menuSistemaCliente.icon = "icon-people";

            Menu menuSistemaPessoaFisica = new Menu();
            menuSistemaPessoaFisica.text = "Pessoa Física";
            menuSistemaPessoaFisica.sref = "app.sistema-pessoafisica";
            menuSistemaCliente.submenu.Add(menuSistemaPessoaFisica);

            Menu menuSistemaPessoaJuridica = new Menu();
            menuSistemaPessoaJuridica.text = "Pessoa Jurídica";
            menuSistemaPessoaJuridica.sref = "app.sistema-pessoajuridica";
            menuSistemaCliente.submenu.Add(menuSistemaPessoaJuridica);

            data.Add(menuSistemaCliente);

            Menu menuAvaliacao = new Menu();
            menuAvaliacao.text = "Avaliação";
            menuAvaliacao.sref = "#";
            menuAvaliacao.icon = "icon-check";

            Menu menuModeloAvaliacao = new Menu();
            menuModeloAvaliacao.text = "Modelo Avaliação";
            menuModeloAvaliacao.sref = "app.sistema-modeloavaliacao";
            menuAvaliacao.submenu.Add(menuModeloAvaliacao);

            Menu menuNovaModeloAvaliacao = new Menu();
            menuNovaModeloAvaliacao.text = "Nova Avaliação";
            menuNovaModeloAvaliacao.sref = "app.sistema-avaliacao";
            menuAvaliacao.submenu.Add(menuNovaModeloAvaliacao);

            data.Add(menuAvaliacao);

            return data;
        }

        public static List<Menu> LoadMenuUsuario()
        {
            List<Menu> data = new List<Menu>();
            List<Menu> dataResult = new List<Menu>();
            Menu menuHeading = new Menu();
            menuHeading.text = "MENU";
            menuHeading.heading = true;
            // menuHeading.translate = "sidebar.heading.HEADER";
            dataResult.Add(menuHeading);
            IUnidadeTrabalho contexto = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IQueryable<Funcao> funcoes = contexto.ObterTodos<Funcao>();
            IRepositorioUsuario repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(contexto);
            foreach (KeyValuePair<Funcao, IEnumerable<Acao>> permisao in repPermissao.ObterFuncaoeAcoesPorUsuarioLogado())
            {

                var moduloDaFuncao = funcoes.Where(p => p.Tipo.Equals(TipoPagina.Modulo) && p.Codigo == permisao.Key.Pai.Codigo).FirstOrDefault();

                if (!dataResult.Any(x => x.text.Equals(moduloDaFuncao.Descricao)))
                {
                    if (!String.IsNullOrEmpty(permisao.Key.Ref))
                    {
                        Menu pai = new Menu();
                        pai.text = moduloDaFuncao.Descricao;
                        pai.sref = "#";
                        pai.icon = moduloDaFuncao.Icone;

                        Menu filha = new Menu();
                        filha.text = permisao.Key.Descricao;
                        filha.sref = permisao.Key.Ref;
                        pai.submenu.Add(filha);
                        dataResult.Add(pai);
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(permisao.Key.Ref))
                    {
                        Menu paiSelecionado = dataResult.Where(x => x.text.Equals(moduloDaFuncao.Descricao)).FirstOrDefault();
                        Menu filha = new Menu();
                        filha.text = permisao.Key.Descricao;
                        filha.sref = permisao.Key.Ref;
                        paiSelecionado.submenu.Add(filha);
                    }
                }
            }

            return dataResult;
        }
    }
}