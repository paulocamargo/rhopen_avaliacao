﻿using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Repositorios.Sistema
{
    public interface IRepositorioDadosGerais
    {
        IQueryable<EnunciadoAvaliacao> ListarEnunciadosAvaliacao(ModeloAvaliacao modAval);
        IQueryable<EnunciadoAvaliacao> ListarDadosRelatorioAvaliacao(Avaliacao Aval);
    }
}
