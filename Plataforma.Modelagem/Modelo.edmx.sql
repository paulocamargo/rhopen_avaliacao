
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/04/2017 10:18:21
-- Generated from EDMX file: C:\Desenv\globalsys_plataformabackoffice\Plataforma.Modelagem\Modelo.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [DB_WINE];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[ModeloStoreContainer].[SEG_GRUPO]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SEG_GRUPO];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SEG_MEMBRO]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SEG_MEMBRO];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SEG_PERM_GRUPO_ACAO]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SEG_PERM_GRUPO_ACAO];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SEG_PERM_USUARIO_ACAO]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SEG_PERM_USUARIO_ACAO];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SEG_USUARIO]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SEG_USUARIO];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SIS_ACAO]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SIS_ACAO];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SIS_AUDITORIA]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SIS_AUDITORIA];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SIS_FUNCOES]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SIS_FUNCOES];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SIS_LOG_ERRO]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SIS_LOG_ERRO];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[SIS_RECURSOS]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[SIS_RECURSOS];
GO
IF OBJECT_ID(N'[ModeloStoreContainer].[VIX_COLABORADOR]', 'U') IS NOT NULL
    DROP TABLE [ModeloStoreContainer].[VIX_COLABORADOR];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'SEG_GRUPO'
CREATE TABLE [dbo].[SEG_GRUPO] (
    [CD_GRUPO] int  NOT NULL,
    [TX_NOME] nvarchar(150)  NOT NULL,
    [TX_DESCRICAO] nvarchar(350)  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL,
    [DT_CADASTRO] datetime  NOT NULL
);
GO

-- Creating table 'SEG_MEMBRO'
CREATE TABLE [dbo].[SEG_MEMBRO] (
    [CD_MEMBRO] int  NOT NULL,
    [CD_GRUPO] int  NOT NULL,
    [CD_USUARIO] int  NOT NULL,
    [DT_CADASTRO] datetime  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL
);
GO

-- Creating table 'SEG_PERM_GRUPO_ACAO'
CREATE TABLE [dbo].[SEG_PERM_GRUPO_ACAO] (
    [CD_GRUPO_ACAO] int  NOT NULL,
    [CD_GRUPO] int  NOT NULL,
    [CD_ACAO] int  NOT NULL,
    [DT_CADASTRO] datetime  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL
);
GO

-- Creating table 'SEG_PERM_USUARIO_ACAO'
CREATE TABLE [dbo].[SEG_PERM_USUARIO_ACAO] (
    [CD_USUARIO_ACAO] int  NOT NULL,
    [CD_USUARIO] int  NOT NULL,
    [CD_ACAO] int  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL,
    [DT_CADASTRO] datetime  NOT NULL
);
GO

-- Creating table 'SEG_USUARIO'
CREATE TABLE [dbo].[SEG_USUARIO] (
    [CD_USUARIO] int  NOT NULL,
    [TX_LOGIN] nvarchar(100)  NULL,
    [DT_CADASTRO] datetime  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL,
    [CD_COLABORADOR] int  NULL,
    [BL_LOGIN_AD] bit  NOT NULL,
    [TX_SENHA] varchar(200)  NULL,
    [TX_PLAYER_ID] nvarchar(max)  NULL
);
GO

-- Creating table 'SIS_ACAO'
CREATE TABLE [dbo].[SIS_ACAO] (
    [CD_ACAO] int  NOT NULL,
    [TX_NOME] nvarchar(150)  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL,
    [TX_REF] nvarchar(100)  NOT NULL,
    [CD_FUNCAO] int  NOT NULL,
    [DT_CADASTRO] datetime  NOT NULL
);
GO

-- Creating table 'SIS_AUDITORIA'
CREATE TABLE [dbo].[SIS_AUDITORIA] (
    [CD_AUDITORIA] int  NOT NULL,
    [CD_USUARIO] int  NOT NULL,
    [TX_ACAO] nvarchar(100)  NOT NULL,
    [TX_COD_REGISTRO] nvarchar(100)  NOT NULL,
    [TX_DESCRICAO] varchar(max)  NULL,
    [DT_CADASTRO] datetime  NOT NULL,
    [TX_IP] nvarchar(100)  NOT NULL,
    [TX_COMPLETO_ENTIDADE] nvarchar(100)  NOT NULL,
    [TX_NOME_ENTIDADE] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'SIS_FUNCOES'
CREATE TABLE [dbo].[SIS_FUNCOES] (
    [CD_FUNCAO] int  NOT NULL,
    [TX_NOME] nvarchar(150)  NOT NULL,
    [TX_DESCRICAO] nvarchar(350)  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL,
    [TX_REF] nvarchar(100)  NULL,
    [TX_TIPO] nvarchar(50)  NOT NULL,
    [TX_COR] nvarchar(50)  NULL,
    [TX_ICON] nvarchar(150)  NULL,
    [TX_CANAL] nvarchar(100)  NOT NULL,
    [CD_PAI] int  NULL,
    [DT_CADASTRO] datetime  NOT NULL
);
GO

-- Creating table 'SIS_LOG_ERRO'
CREATE TABLE [dbo].[SIS_LOG_ERRO] (
    [CD_LOG_ERRO] int  NOT NULL,
    [TX_ACAO] varchar(max)  NULL,
    [DT_CADASTRO] datetime  NOT NULL,
    [TX_MENSAGEM] nvarchar(max)  NOT NULL,
    [TX_STACKTRACE] nvarchar(max)  NOT NULL,
    [CD_USUARIO] int  NOT NULL
);
GO

-- Creating table 'SIS_RECURSOS'
CREATE TABLE [dbo].[SIS_RECURSOS] (
    [CD_RECURSO] int  NOT NULL,
    [CD_ACAO_PRINCIPAL] int  NOT NULL,
    [CD_ACAO_SECUNDARIA] int  NOT NULL
);
GO

-- Creating table 'SEG_COLABORADOR'
CREATE TABLE [dbo].[SEG_COLABORADOR] (
    [CD_COLABORADOR] int  NOT NULL,
    [TX_NOME] nvarchar(150)  NOT NULL,
    [TX_EMAIL] nvarchar(150)  NOT NULL,
    [TX_CPF] nvarchar(30)  NOT NULL,
    [DT_CADASTRO] datetime  NOT NULL,
    [DT_DESATIVACAO] datetime  NULL,
    [TX_TIPO] nvarchar(50)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [CD_GRUPO], [TX_NOME], [TX_DESCRICAO], [DT_CADASTRO] in table 'SEG_GRUPO'
ALTER TABLE [dbo].[SEG_GRUPO]
ADD CONSTRAINT [PK_SEG_GRUPO]
    PRIMARY KEY CLUSTERED ([CD_GRUPO], [TX_NOME], [TX_DESCRICAO], [DT_CADASTRO] ASC);
GO

-- Creating primary key on [CD_MEMBRO], [CD_GRUPO], [CD_USUARIO], [DT_CADASTRO] in table 'SEG_MEMBRO'
ALTER TABLE [dbo].[SEG_MEMBRO]
ADD CONSTRAINT [PK_SEG_MEMBRO]
    PRIMARY KEY CLUSTERED ([CD_MEMBRO], [CD_GRUPO], [CD_USUARIO], [DT_CADASTRO] ASC);
GO

-- Creating primary key on [CD_GRUPO_ACAO], [CD_GRUPO], [CD_ACAO], [DT_CADASTRO] in table 'SEG_PERM_GRUPO_ACAO'
ALTER TABLE [dbo].[SEG_PERM_GRUPO_ACAO]
ADD CONSTRAINT [PK_SEG_PERM_GRUPO_ACAO]
    PRIMARY KEY CLUSTERED ([CD_GRUPO_ACAO], [CD_GRUPO], [CD_ACAO], [DT_CADASTRO] ASC);
GO

-- Creating primary key on [CD_USUARIO_ACAO], [CD_USUARIO], [CD_ACAO], [DT_CADASTRO] in table 'SEG_PERM_USUARIO_ACAO'
ALTER TABLE [dbo].[SEG_PERM_USUARIO_ACAO]
ADD CONSTRAINT [PK_SEG_PERM_USUARIO_ACAO]
    PRIMARY KEY CLUSTERED ([CD_USUARIO_ACAO], [CD_USUARIO], [CD_ACAO], [DT_CADASTRO] ASC);
GO

-- Creating primary key on [CD_USUARIO], [DT_CADASTRO], [BL_LOGIN_AD] in table 'SEG_USUARIO'
ALTER TABLE [dbo].[SEG_USUARIO]
ADD CONSTRAINT [PK_SEG_USUARIO]
    PRIMARY KEY CLUSTERED ([CD_USUARIO], [DT_CADASTRO], [BL_LOGIN_AD] ASC);
GO

-- Creating primary key on [CD_ACAO], [TX_NOME], [TX_REF], [CD_FUNCAO], [DT_CADASTRO] in table 'SIS_ACAO'
ALTER TABLE [dbo].[SIS_ACAO]
ADD CONSTRAINT [PK_SIS_ACAO]
    PRIMARY KEY CLUSTERED ([CD_ACAO], [TX_NOME], [TX_REF], [CD_FUNCAO], [DT_CADASTRO] ASC);
GO

-- Creating primary key on [CD_AUDITORIA], [CD_USUARIO], [TX_ACAO], [TX_COD_REGISTRO], [DT_CADASTRO], [TX_IP], [TX_COMPLETO_ENTIDADE], [TX_NOME_ENTIDADE] in table 'SIS_AUDITORIA'
ALTER TABLE [dbo].[SIS_AUDITORIA]
ADD CONSTRAINT [PK_SIS_AUDITORIA]
    PRIMARY KEY CLUSTERED ([CD_AUDITORIA], [CD_USUARIO], [TX_ACAO], [TX_COD_REGISTRO], [DT_CADASTRO], [TX_IP], [TX_COMPLETO_ENTIDADE], [TX_NOME_ENTIDADE] ASC);
GO

-- Creating primary key on [CD_FUNCAO], [TX_NOME], [TX_DESCRICAO], [TX_TIPO], [TX_CANAL], [DT_CADASTRO] in table 'SIS_FUNCOES'
ALTER TABLE [dbo].[SIS_FUNCOES]
ADD CONSTRAINT [PK_SIS_FUNCOES]
    PRIMARY KEY CLUSTERED ([CD_FUNCAO], [TX_NOME], [TX_DESCRICAO], [TX_TIPO], [TX_CANAL], [DT_CADASTRO] ASC);
GO

-- Creating primary key on [CD_LOG_ERRO], [DT_CADASTRO], [TX_MENSAGEM], [TX_STACKTRACE], [CD_USUARIO] in table 'SIS_LOG_ERRO'
ALTER TABLE [dbo].[SIS_LOG_ERRO]
ADD CONSTRAINT [PK_SIS_LOG_ERRO]
    PRIMARY KEY CLUSTERED ([CD_LOG_ERRO], [DT_CADASTRO], [TX_MENSAGEM], [TX_STACKTRACE], [CD_USUARIO] ASC);
GO

-- Creating primary key on [CD_RECURSO], [CD_ACAO_PRINCIPAL], [CD_ACAO_SECUNDARIA] in table 'SIS_RECURSOS'
ALTER TABLE [dbo].[SIS_RECURSOS]
ADD CONSTRAINT [PK_SIS_RECURSOS]
    PRIMARY KEY CLUSTERED ([CD_RECURSO], [CD_ACAO_PRINCIPAL], [CD_ACAO_SECUNDARIA] ASC);
GO

-- Creating primary key on [CD_COLABORADOR], [TX_NOME], [TX_EMAIL], [TX_CPF], [DT_CADASTRO], [TX_TIPO] in table 'SEG_COLABORADOR'
ALTER TABLE [dbo].[SEG_COLABORADOR]
ADD CONSTRAINT [PK_SEG_COLABORADOR]
    PRIMARY KEY CLUSTERED ([CD_COLABORADOR], [TX_NOME], [TX_EMAIL], [TX_CPF], [DT_CADASTRO], [TX_TIPO] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------