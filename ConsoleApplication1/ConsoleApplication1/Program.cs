﻿using GoogleMaps.LocationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var address = "Av. Saturnino Rangel Mauro, 400 - Praia de Itaparica, Vila Velha - ES, Brazil";
            var locationService = new GoogleLocationService();
            var point = locationService.GetLatLongFromAddress(address);
            var latitude = point.Latitude;
            var longitude = point.Longitude;

            Console.WriteLine("Latitude: " + latitude);
            Console.WriteLine("Longitude: " + longitude);
            Console.ReadLine();
        }
    }
}
