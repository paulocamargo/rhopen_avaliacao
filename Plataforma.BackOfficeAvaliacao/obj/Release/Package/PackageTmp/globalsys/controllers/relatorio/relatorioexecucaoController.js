'use strict';
Globalsys.controller('relatorioexecucaoController', ['$scope', '$location', 'relatorioexecucaoService', 'testeavaliacaoService', 'avaliacaoService', '$uibModal', function ($scope, $location, relatorioexecucaoService,testeavaliacaoService, avaliacaoService, $uibModal) {
    $scope.registros = [];
    $scope.registro = {};
    $scope.tituloModal = "";
    $scope.labels = [];
    $scope.series = [];
    $scope.dadosGraficos = [];
    $scope.data = [];
    $scope.dataHostilidade = [];
    $scope.dataDominancia = [];
    $scope.dataDominanciaLegend = [];
    $scope.visaoGeral = [];
    $scope.introducao = [];
    $scope.resultados = [];
    $scope.introducaoposImagem = [];
    $scope.introducaoposResultado = [];
    $scope.labelsDominancia = ["Dominância", "Submissão"];
    $scope.labelsHostilidade = ["Hostilidade", "Cordialidade"];
    $scope.labelsPerfil = ["Q1", "Q2", "Q3", "Q4"];
    $scope.dataPerfil = [];

    $scope.planejamentoResultados = [];
    $scope.lideranca = [];
    $scope.comunicacaoRelacionamento = [];
    $scope.processoDecisorio = [];
    $scope.consideracaoFinal = [];
    $scope.admConflitos = [];
    //$scope.dataDominancia = [];
    $scope.Avaliacao = [];
    $scope.options = {};
    $scope.optionsPie = {};
    $scope.Respostas = [];
    $scope.Error = {};

    $scope.Avaliacao = $location.$$search.param.split(";");

    $scope.montarDadosGrafico = function () {

        $scope.labels = [];
        $scope.series = [];
        $scope.data = [];
        $scope.maioresResultados = [];
        $scope.maioresResultadosNome = [];

        var QtdCategoria8 = $scope.Avaliacao[0];
        var nomeCategoria8 = $scope.Avaliacao[1];
        var descCategoria8 = $scope.Avaliacao[2];

        var QtdCategoria9 = $scope.Avaliacao[3];
        var nomeCategoria9 = $scope.Avaliacao[4];
        var descCategoria9 = $scope.Avaliacao[5];

        var QtdCategoria10 = $scope.Avaliacao[6];
        var nomeCategoria10 = $scope.Avaliacao[7];
        var descCategoria10 = $scope.Avaliacao[8];

        var QtdCategoria11 = $scope.Avaliacao[9];
        var nomeCategoria11 = $scope.Avaliacao[10];
        var descCategoria11 = $scope.Avaliacao[11];
        //var qtdMaxplanejamentoResultados;
        //var qtdMaxlideranca;
        //var qtdcomunicacaoRelacionamento;
        //var qtdprocessoDecisorio;
        //var qtdadmConflitos;

        for (var i = 13; i < $scope.Avaliacao.length; i++) {
            $scope.Respostas.push($scope.Avaliacao[i]);
        }
        $scope.introducao.push("Este relatório é confidencial e tem por objetivo demonstrar se baseia no Modelo Bidimensional de Comportamento que analisa o comportamento humano e seus motivadores através da relação entre o EU e o MUNDO, relação onde são geradas as crenças que sustentam a forma de uma pessoa se comportar.");
        $scope.introducao.push(" Neste modelo cada indivíduo organiza seu mapa de mundo com crenças sobre si mesmo chamadas “Arquivos do EU” e crenças sobre os outros chamadas “ Arquivo do Mundo”. Estes dois arquivos são construídos a partir do critério de Okeidade, ou seja, o quanto que as experiencias vividas foram compreendidas pelo indivíduo como positivas “OK” ou negativas “Não OK”. Neste contexto o mapa de mundo que balizará os comportamentos apresentados por cada pessoa está alicerçado na relação: ");
        $scope.introducao.push(" Mapa de Mundo = Arquivo do EU (OK ou Não OK) X Arquivo do Mundo (OK ou Não OK) ");
        $scope.introducao.push(" Resultando nas possíveis combinações: ");



        $scope.introducaoposImagem.push("O comportamento de cada pessoa é constituído de um mix dessas diferentes possibilidades, dependendo dos diferentes níveis de Okeidade das crenças acessadas para o gerenciamento de cada ação. Desta forma toda pessoa possui uma quantidade de cada uma das 4  possíveis combinações, se diferenciando das demais pessoas pela proporção ou dose dessas combinações e sendo assim identificado pela combinação mais forte apresentada. ");
        $scope.introducaoposImagem.push(" Cada combinação abarcará consigo um conjunto de crenças e necessidades que darão sentido aos comportamentos que serão apresentados no cotidiano de cada pessoa e que poderão ser melhor visualizados na figura abaixo: ");

        $scope.introducaoposResultado.push("Com base no acima exposto, segue a analise do perfil comportamental de " + $scope.Avaliacao[12] + ". Estimulado por quarenta enunciados e elegendo as respostas aferiu-se o seguinte : ");
        var planejamentoResultados = $scope.Avaliacao[12];
        var lideranca = $scope.Avaliacao[12];
        var comunicacaoRelacionamento = $scope.Avaliacao[12];
        var processoDecisorio = $scope.Avaliacao[12];
        var admConflitos = $scope.Avaliacao[12];

        var validaplanejamentoResultados = 0;
        var validalideranca = 0;
        var validacomunicacaoRelacionamento = 0;
        var validaprocessoDecisorio = 0;
        var validaadmConflitos = 0;
        $scope.maioresResultados[0] = QtdCategoria8;
        $scope.maioresResultados[1] = QtdCategoria9;
        $scope.maioresResultados[2] = QtdCategoria10;
        $scope.maioresResultados[3] = QtdCategoria11;
        $scope.maioresResultadosNome[0] = 'Q1';
        $scope.maioresResultadosNome[1] = 'Q2';
        $scope.maioresResultadosNome[2] = 'Q3';
        $scope.maioresResultadosNome[3] = 'Q4';


        for (var j = 0 ; j < 4; j++) {
            for (var k = j; k < 4; k++) {
                if ($scope.maioresResultados[j] < $scope.maioresResultados[k]) {
                    var aux = $scope.maioresResultados[j];
                    $scope.maioresResultados[j] = $scope.maioresResultados[k];
                    $scope.maioresResultados[k] = aux;

                    var auxNome = $scope.maioresResultadosNome[j];
                    $scope.maioresResultadosNome[j] = $scope.maioresResultadosNome[k];
                    $scope.maioresResultadosNome[k] = auxNome;

                }
            }
        }
        //$scope.introducaoposResultado.push("O que nos leva as seguintes considerações: O perfil comportamental de fulano de tal é primariamente :  ")
        $scope.introducaoposResultado.push("O que nos leva as seguintes considerações:");
        if (parseInt(QtdCategoria8) > parseInt(QtdCategoria9) && parseInt(QtdCategoria8) > parseInt(QtdCategoria10) && parseInt(QtdCategoria8) > parseInt(QtdCategoria11)) {
            //$scope.maioresResultados[0] = 'Q1';
            //$scope.maioresResultados[1] = QtdCategoria8;
            // $scope.introducaoposResultado.push("O que nos leva as seguintes considerações: O perfil comportamental de " + $scope.Avaliacao[12] + " é primariamente :  Dominância");

            if ($scope.maioresResultadosNome[0] != 'Q1') {
                for (var i = 0; i < $scope.maioresResultadosNome.Length ; i++) {
                    if ($scope.maioresResultadosNome[i] == 'Q1') {
                        var aux = $scope.maioresResultadosNome[i];
                        $scope.maioresResultadosNome[i] = $scope.maioresResultadosNome[0];
                        $scope.maioresResultadosNome[0] = aux;

                        var auxnome = $scope.maioresResultados[i];
                        $scope.maioresResultados[i] = $scope.maioresResultadosNome[0];
                        $scope.maioresResultados[0] = auxnome;
                    }
                }
            }
            var percentual = (parseInt(QtdCategoria8) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100;
            $scope.visaoGeral.push("De acordo com o MBC, o perfil comportamental de ");
            $scope.visaoGeral.push($scope.Avaliacao[12]);
            $scope.visaoGeral.push(" encontra-se predominantemente localizado no Quadrante 1  (Q1) do Sistema Cartesiano anteriormente apresentado, recebendo para ");
            $scope.visaoGeral.push("efeito de estudo classificatório, a designação de Dominante Hostil. Isso é consequência de que , em  seu Sistema Mental ou Mapa de Mundo, existe,");
            $scope.visaoGeral.push("a respeito de si mesmo , uma predominância de crenças com elevado nível de “okeidade”-(Crenças OK) - e, em contrapartida, a respeito do Mundo (Outros),");
            $scope.visaoGeral.push("é  maior a quantidade de Crenças com reduzido nível de okeidade, – (Crenças Não OK).   Sob a ótica Maslowiana da Hierarquia das Necessidades, esse estilo comportamental");
            $scope.visaoGeral.push("é movido por Necessidade de Valorização e age em busca de status, poder, promoção. \n Sob pressão, " + $scope.Avaliacao[12] + " poderá apresentar Flutuação Comportamental,seja como estratégia para reinvestir na busca do objetivo, assumindo diferentes atitudes,seja como reação emocional quando acredita ter esgotado suas alternativas.");
            //if (parseInt(QtdCategoria9) > parseInt(QtdCategoria10) && parseInt(QtdCategoria9) > parseInt(QtdCategoria11)) {
            //    $scope.maioresResultados[1] = 'Q2';
            //    if (parseInt(QtdCategoria10) > parseInt(QtdCategoria11)) {
            //        $scope.maioresResultados[2] = 'Q3';
            //    } else
            //        $scope.maioresResultados[2] = 'Q4';
            //    //$scope.maioresResultados[3] = QtdCategoria9;
            //} else
            //    if (parseInt(QtdCategoria10) > parseInt(QtdCategoria9) && parseInt(QtdCategoria10) > parseInt(QtdCategoria11)) {
            //        $scope.maioresResultados[1] = 'Q3';
            //        if (parseInt(QtdCategoria9) > parseInt(QtdCategoria11)) {
            //            $scope.maioresResultados[2] = 'Q2';
            //        } else
            //            $scope.maioresResultados[2] = 'Q4';
            //        //$scope.maioresResultados[3] = QtdCategoria9;
            //    } else {
            //        if (parseInt(QtdCategoria11) > parseInt(QtdCategoria9) && parseInt(QtdCategoria11) > parseInt(QtdCategoria10)) {
            //            $scope.maioresResultados[1] = 'Q4';
            //            if (parseInt(QtdCategoria9) > parseInt(QtdCategoria11)) {
            //                $scope.maioresResultados[2] = 'Q2';
            //            } else
            //                $scope.maioresResultados[2] = 'Q3';
            //            //$scope.maioresResultados[3] = QtdCategoria9;
            //        }
            //    }

        } else
            if (parseInt(QtdCategoria9) > parseInt(QtdCategoria8) && parseInt(QtdCategoria9) > parseInt(QtdCategoria10) && parseInt(QtdCategoria9) > parseInt(QtdCategoria11)) {
                //$scope.maioresResultados[0] = 'Q2';
                //$scope.maioresResultados[1] = QtdCategoria9;
                //  $scope.introducaoposResultado.push("O que nos leva as seguintes considerações: O perfil comportamental de " + $scope.Avaliacao[12] + " é primariamente :  Hostil");

                if ($scope.maioresResultadosNome[0] != 'Q2') {
                    for (var i = 0; i < $scope.maioresResultadosNome.Length ; i++) {
                        if ($scope.maioresResultadosNome[i] == 'Q2') {

                            var aux = $scope.maioresResultadosNome[i];
                            $scope.maioresResultadosNome[i] = $scope.maioresResultadosNome[0];
                            $scope.maioresResultadosNome[0] = aux;

                            var auxnome = $scope.maioresResultados[i];
                            $scope.maioresResultados[i] = $scope.maioresResultadosNome[0];
                            $scope.maioresResultados[0] = auxnome;
                        }
                    }
                }
                var percentual = (parseInt(QtdCategoria9) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100;
                $scope.visaoGeral.push("De acordo com o MBC, o perfil comportamental de ");
                $scope.visaoGeral.push($scope.Avaliacao[12]);
                $scope.visaoGeral.push("encontra-se predominantemente localizado no Quadrante 2  (Q2) do Sistema Cartesiano anteriormente apresentado e recebe a designação de SUBMISSO HOSTIL, ");
                $scope.visaoGeral.push("para facilitar o estudo classificatório. No Sistema Mental ou Mapa de Mundo deste perfil,existe uma predominância de crenças com reduzido nível de ");
                $scope.visaoGeral.push("“okeidade”-(Crenças Não  OK) – tanto a respeito de si mesmo quanto a  respeito do Mundo (Outros– (Crenças Não OK). Tal repertório de crenças alicerçando esse comportamento,");
                $scope.visaoGeral.push("torna " + $scope.Avaliacao[12] + " mais retraído e introspectivo, avesso a exposição, resistindo a dar opiniões e tomar posições, cético a respeito de planos e promessas e desconfiado em relação aos outros e às circunstâncias.");
                $scope.visaoGeral.push("Sob a ótica Maslowiana da Hierarquia das Necessidades, esse estilo comportamental  é movido por Necessidade deSegurança e age em busca resguardo e proteção.  \n Sob pressão, " + $scope.Avaliacao[12] + " poderá apresentar Flutuação Comportamental,seja como estratégia para reinvestir na busca do objetivo, assumindo diferentes atitudes,seja como reação emocional quando acredita ter esgotado suas alternativas.");

                //if (parseInt(QtdCategoria8) > parseInt(QtdCategoria10) && parseInt(QtdCategoria8) > parseInt(QtdCategoria11)) {
                //    $scope.maioresResultados[1] = 'Q1';
                //    if (parseInt(QtdCategoria10) > parseInt(QtdCategoria11)) {
                //        $scope.maioresResultados[2] = 'Q3';
                //    } else
                //        $scope.maioresResultados[2] = 'Q4';
                //    //$scope.maioresResultados[3] = QtdCategoria9;
                //} else
                //    if (parseInt(QtdCategoria10) > parseInt(QtdCategoria8) && parseInt(QtdCategoria10) > parseInt(QtdCategoria11)) {
                //        $scope.maioresResultados[1] = 'Q3';
                //        if (parseInt(QtdCategoria8) > parseInt(QtdCategoria11)) {
                //            $scope.maioresResultados[2] = 'Q1';
                //        } else
                //            $scope.maioresResultados[2] = 'Q4';
                //        //$scope.maioresResultados[3] = QtdCategoria9;
                //    } else {
                //        if (parseInt(QtdCategoria11) > parseInt(QtdCategoria9) && parseInt(QtdCategoria11) > parseInt(QtdCategoria10)) {
                //            $scope.maioresResultados[1] = 'Q4';
                //            if (parseInt(QtdCategoria8) > parseInt(QtdCategoria11)) {
                //                $scope.maioresResultados[2] = 'Q1';
                //            } else
                //                $scope.maioresResultados[2] = 'Q3';
                //            //$scope.maioresResultados[3] = QtdCategoria9;
                //        }

                //    }

            } else

                if (parseInt(QtdCategoria10) > parseInt(QtdCategoria8) && parseInt(QtdCategoria10) > parseInt(QtdCategoria9) && parseInt(QtdCategoria10) > parseInt(QtdCategoria11)) {

                    if ($scope.maioresResultadosNome[0] != 'Q3') {
                        for (var i = 0; i < $scope.maioresResultadosNome.Length ; i++) {
                            if ($scope.maioresResultadosNome[i] == 'Q3') {
                                var aux = $scope.maioresResultadosNome[i];
                                $scope.maioresResultadosNome[i] = $scope.maioresResultadosNome[0];
                                $scope.maioresResultadosNome[0] = aux;

                                var auxnome = $scope.maioresResultados[i];
                                $scope.maioresResultados[i] = $scope.maioresResultadosNome[0];
                                $scope.maioresResultados[0] = auxnome;
                            }
                        }
                    }

                    //$scope.maioresResultados[0] = 'Q3';
                    //var percentual = (parseInt(QtdCategoria10) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100;
                    ////$scope.maioresResultados[1] = QtdCategoria10;
                    //     $scope.introducaoposResultado.push("O que nos leva as seguintes considerações: O perfil comportamental de " + $scope.Avaliacao[12] + " é primariamente :  Submisso");
                    $scope.visaoGeral.push("De acordo com o MBC, o perfil comportamental de ");
                    $scope.visaoGeral.push($scope.Avaliacao[12]);
                    $scope.visaoGeral.push(" encontra-se predominantemente localizado no Quadrante3  (Q3) do Sistema Cartesiano anteriormente apresentado e recebe a designação de SUBMISSO CORDIAL, para facilitar o estudo classificatório.");
                    $scope.visaoGeral.push("No Sistema Mental ou Mapa de Mundo deste perfil, existe uma predominância de crenças com reduzido nível de “okeidade”-(Crenças Não  OK) – a respeito de si mesmo e, em contrapartida, as crenças de elevado nível de okeidade (Crenças OK)  a  respeito do Mundo (Outros) ");
                    $scope.visaoGeral.push("–superam as de crenças limitadoras (Mundo Não OK). Tal repertório de crenças alicerçando esse comportamento, torna  " + $scope.Avaliacao[12] + " mais amistoso e complascente, ");
                    $scope.visaoGeral.push("de fácil relacionamento, confiante e calmo. Sob a ótica Maslowiana da Hierarquia das Necessidades, esse estilo comportamental  é movido por  Necessidade Social, ");
                    $scope.visaoGeral.push("de modo que  age em busca de aceitação e pertinência em relação ao grupo.  \n Sob pressão, " + $scope.Avaliacao[12] + " poderá apresentar Flutuação Comportamental,seja como estratégia para reinvestir na busca do objetivo, assumindo diferentes atitudes,seja como reação emocional quando acredita ter esgotado suas alternativas.");

                    //if (parseInt(QtdCategoria8) > parseInt(QtdCategoria9) && parseInt(QtdCategoria8) > parseInt(QtdCategoria11)) {
                    //    $scope.maioresResultados[1] = 'Q1';
                    //    //$scope.maioresResultados[3] = QtdCategoria8;
                    //} else
                    //    if (parseInt(QtdCategoria9) > parseInt(QtdCategoria8) && parseInt(QtdCategoria9) > parseInt(QtdCategoria11)) {
                    //        $scope.maioresResultados[1] = 'Q2';
                    //        //$scope.maioresResultados[3] = QtdCategoria9;
                    //    } else {
                    //        $scope.maioresResultados[1] = 'Q4';
                    //        //$scope.maioresResultados[3] = QtdCategoria11;
                    //    }

                } else {
                    var percentual = Math.round((parseInt(QtdCategoria11) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100, 2);
                    //$scope.maioresResultadosNome[0] = 'Q4';
                    if ($scope.maioresResultadosNome[0] != 'Q4') {
                        for (var i = 0; i < $scope.maioresResultadosNome.Length ; i++) {
                            if ($scope.maioresResultadosNome[i] == 'Q4') {
                                var aux = $scope.maioresResultadosNome[i];
                                $scope.maioresResultadosNome[i] = $scope.maioresResultadosNome[0];
                                $scope.maioresResultadosNome[0] = aux;

                                var auxnome = $scope.maioresResultados[i];
                                $scope.maioresResultados[i] = $scope.maioresResultadosNome[0];
                                $scope.maioresResultados[0] = auxnome
                            }
                        }
                    }

                    $scope.visaoGeral.push("De acordo com o MBC, o perfil comportamental de ");
                    $scope.visaoGeral.push($scope.Avaliacao[12]);
                    $scope.visaoGeral.push(" apresenta um percentual de " + percentual + "% \n no Quadrante4  (Q4) \n do Sistema Cartesiano anteriormente apresentado, recebendo para efeito de estudo classificatório, a designação de Dominante Cordial. ");
                    $scope.visaoGeral.push("Isso é consequência de que , em  seu Sistema Mental ou Mapa de Mundo, existe,  a respeito de si mesmo , uma predominância de crenças com elevado nível de “okeidade”-(Crenças OK) – o mesmo acontecendo, ");
                    $scope.visaoGeral.push("a respeito do Mundo (Outros), o que o leva a estabelecer as relações dentro dos princípios de Verdade  x  Respeito. ");
                    $scope.visaoGeral.push("Sob a ótica Maslowiana da Hierarquia das Necessidades, esse estilo comportamental  é");
                    $scope.visaoGeral.push("movido por Necessidade de Realização e seu foco são os Resultados e o cumprimento da Missão a que se propôs. \n Sob pressão, " + $scope.Avaliacao[12] + " poderá apresentar Flutuação Comportamental,seja como estratégia para reinvestir na busca do objetivo, assumindo diferentes atitudes,seja como reação emocional quando acredita ter esgotado suas alternativas.");


                }
        for (var k = 0; k < $scope.maioresResultados.length - 2; k++) {
            if (k != 0) {
                //planejamentoResultados += "\n";
                //planejamentoResultados += "\n";
                //planejamentoResultados += "Comportamento de flutuação";
                //planejamentoResultados += "\n";

                //lideranca += "\n";
                //lideranca += "\n";
                //lideranca += "Comportamento de flutuação";
                //lideranca += "\n";

                comunicacaoRelacionamento += "\n";
                comunicacaoRelacionamento += "\n";
                comunicacaoRelacionamento += "Comportamento de flutuação";
                comunicacaoRelacionamento += "\n";

                processoDecisorio += "\n";
                processoDecisorio += "\n";
                processoDecisorio += "Comportamento de flutuação";
                processoDecisorio += "\n";

                admConflitos += "\n";
                admConflitos += "\n";
                admConflitos += "Comportamento de flutuação";
                admConflitos += "\n";
            }


            //Q4 prioridade 
            if (k == 0) {

                if ($scope.Respostas.toString().indexOf("," + 11 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 39 + ",") >= 0) {
                    //                  planejamentoResultados += "";
                    //                    planejamentoResultados += " Sente-se perdido se não trabalhar com objetivos e parte da realidade de hoje para o estabelecimento de metas, envolvendo  a equipe na discussão de estratégias necessárias ao atingimento.";
                    $scope.planejamentoResultados.push($scope.Avaliacao[12] + " sente-se perdido se não trabalhar com objetivos e parte da realidade de hoje para o estabelecimento de metas, envolvendo  a equipe na discussão de estratégias necessárias ao atingimento.");
                    $scope.planejamentoResultados.push(" Delega tarefas e compartilha responsabilidades, acompanhando, avaliando e redirecionando  o trabalho , sempre que se fizer necessário, dividindo com a equipe o mérito dos resultados e assumindo, com o grupo, a responsabilidade também pelos erros e insucessos.");
                    //validaplanejamentoResultados = 1;
                    $scope.Respostas = $scope.Respostas.toString().replace(11 + ",", "");
                    $scope.Respostas = $scope.Respostas.toString().replace(39 + ",", "");
                    //$scope.Respostas.splice($scope.Respostas.toString().indexOf("," + 11 + ","), 1);
                    //$scope.Respostas.splice($scope.Respostas.toString().indexOf("," + 39 + ","), 1);
                } else
                    if ($scope.Respostas.toString().indexOf("," + 11 + ",") >= 0) {
                        $scope.planejamentoResultados.push($scope.Avaliacao[12] + " sente-se perdido se não trabalhar com objetivos e parte da realidade de hoje para o estabelecimento de metas, envolvendo  a equipe na discussão de estratégias necessárias ao atingimento.\n");
                        //  planejamentoResultados += " Sente-se perdido se não trabalhar com objetivos e parte da realidade de hoje para o estabelecimento de metas, envolvendo  a equipe na discussão de estratégias necessárias ao atingimento.\n";
                        //  planejamentoResultados += "\n";
                        //  planejamentoResultados += "\n";
                        ////  validaplanejamentoResultados = 1;
                        $scope.Respostas = $scope.Respostas.toString().replace(11 + ",", "");
                    } else {
                        if ($scope.Respostas.toString().indexOf("," + 39 + ",") >= 0) {
                            $scope.planejamentoResultados.push($scope.Avaliacao[12] + " delega tarefas e compartilha responsabilidades, acompanhando, avaliando e redirecionando  o trabalho , sempre que se fizer necessário, dividindo com a equipe o mérito dos resultados e assumindo, com o grupo, a responsabilidade também pelos erros e insucessos.");
                            //planejamentoResultados += " Delega tarefas e compartilha responsabilidades, acompanhando, avaliando e redirecionando  o trabalho , sempre que se fizer necessário, dividindo com a equipe o mérito dos resultados e assumindo, com o grupo, a responsabilidade também pelos erros e insucessos.\n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(39 + ",", "");
                        }
                        //    validaplanejamentoResultados = 1;
                    }

                if ($scope.Respostas.toString().indexOf("," + 19 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 38 + ",") >= 0) {

                    $scope.lideranca.push($scope.Avaliacao[12] + " tem um Plano de Ação claro e consistente, com vista a realização das tarefas e ao desenvolvimento das pessoas. Envolve a equipe nesse plano, de modo a motivar cada colaborador à realização do que lhe é delegado, buscando dar o melhor possível.");
                    //lideranca += "\n";
                    //lideranca += " Tem um Plano de Ação claro e consistente, com vista a realização das tarefas e ao desenvolvimento das pessoas. Envolve a equipe nesse plano, de modo a motivar cada colaborador à realização do que lhe é delegado, buscando dar o melhor possível.\n";
                    //lideranca += "\n";
                    //lideranca += "\n";
                    $scope.lideranca.push(" Incentiva o esforço coletivo e estimula a cooperação reduzindo, com isso, a competição. Usa a crítica e o elogio de forma certa no exercício do feedback, sendo que não critica em público, mas de forma privada e com foco no potencial do indivíduo.");
                    //lideranca += " Incentiva o esforço coletivo e estimula a cooperação reduzindo, com isso, a competição. Usa a crítica e o elogio de forma certa no exercício do feedback, sendo que não critica em público, mas de forma privada e com foco no potencial do indivíduo. \n";
                    //lideranca += "\n";
                    //lideranca += "\n";
                    //validalideranca = 1;
                    $scope.Respostas = $scope.Respostas.toString().replace(19 + ",", "");
                    $scope.Respostas = $scope.Respostas.toString().replace(38 + ",", "");
                } else
                    if ($scope.Respostas.toString().indexOf("," + 19 + ",") >= 0) {
                        //lideranca += " Tem um Plano de Ação claro e consistente, com vista a realização das tarefas e ao desenvolvimento das pessoas. Envolve a equipe nesse plano, de modo a motivar cada colaborador à realização do que lhe é delegado, buscando dar o melhor possível.\n";
                        //lideranca += "\n";
                        //lideranca += "\n";
                        $scope.lideranca.push($scope.Avaliacao[12] + " tem um Plano de Ação claro e consistente, com vista a realização das tarefas e ao desenvolvimento das pessoas. Envolve a equipe nesse plano, de modo a motivar cada colaborador à realização do que lhe é delegado, buscando dar o melhor possível.");
                        $scope.Respostas = $scope.Respostas.toString().replace(19 + ",", "");
                    }
                    else
                        if ($scope.Respostas.toString().indexOf("," + 38 + ",") >= 0) {
                            $scope.lideranca.push($scope.Avaliacao[12] + " incentiva o esforço coletivo e estimula a cooperação reduzindo, com isso, a competição. Usa a crítica e o elogio de forma certa no exercício do feedback, sendo que não critica em público, mas de forma privada e com foco no potencial do indivíduo.");
                            //lideranca += " Incentiva o esforço coletivo e estimula a cooperação reduzindo, com isso, a competição. Usa a crítica e o elogio de forma certa no exercício do feedback, sendo que não critica em público, mas de forma privada e com foco no potencial do indivíduo.\n ";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(38 + ",", "");
                        }

                if ($scope.Respostas.toString().indexOf("," + 20 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 36 + ",") >= 0) {

                    $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " pratica uma comunicação assertiva, falando de seus erros e acertos e estimula as pessoas a exporem suas idéias, ouvindo-as com atenção e respeito. É humilde, sendo capaz de aprender com os outros , independente de posição social ou hierárquica.");
                    //comunicacaoRelacionamento += "";
                    //comunicacaoRelacionamento += " Pratica uma comunicação assertiva, falando de seus erros e acertos e estimula as pessoas a exporem suas idéias, ouvindo-as com atenção e respeito. É humilde, sendo capaz de aprender com os outros , independente de posição social ou hierárquica.\n";
                    //comunicacaoRelacionamento += "\n";
                    //comunicacaoRelacionamento += "\n";
                    $scope.comunicacaoRelacionamento.push(" Dá e recebe feedback, dentro de uma comunicação de verdade e respeito e, quando faz perguntas espera receber respostas diretas. Relaciona-se bem nas diferentas camadas sociais e hierárquicas.");
                    //comunicacaoRelacionamento += " Dá e recebe feedback, dentro de uma comunicação de verdade e respeito e, quando faz perguntas espera receber respostas diretas. Relaciona-se bem nas diferentas camadas sociais e hierárquicas.\n";
                    //comunicacaoRelacionamento += "\n";
                    //comunicacaoRelacionamento += "\n";
                    //validacomunicacaoRelacionamento = 1;
                    $scope.Respostas = $scope.Respostas.toString().replace(20 + ",", "");
                    $scope.Respostas = $scope.Respostas.toString().replace(36 + ",", "");
                } else
                    if ($scope.Respostas.toString().indexOf("," + 20 + ",") >= 0) {
                        $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " pratica uma comunicação assertiva, falando de seus erros e acertos e estimula as pessoas a exporem suas idéias, ouvindo-as com atenção e respeito. É humilde, sendo capaz de aprender com os outros , independente de posição social ou hierárquica. ");
                        //comunicacaoRelacionamento += " Pratica uma comunicação assertiva, falando de seus erros e acertos e estimula as pessoas a exporem suas idéias, ouvindo-as com atenção e respeito. É humilde, sendo capaz de aprender com os outros , independente de posição social ou hierárquica.\n";
                        //comunicacaoRelacionamento += "\n";
                        //comunicacaoRelacionamento += "\n";
                        $scope.Respostas = $scope.Respostas.toString().replace(20 + ",", "");
                    }
                    else
                        if ($scope.Respostas.toString().indexOf("," + 36 + ",") >= 0) {
                            $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " dá e recebe feedback, dentro de uma comunicação de verdade e respeito e, quando faz perguntas espera receber respostas diretas. Relaciona-se bem nas diferentas camadas sociais e hierárquicas.");
                            //comunicacaoRelacionamento += " Dá e recebe feedback, dentro de uma comunicação de verdade e respeito e, quando faz perguntas espera receber respostas diretas. Relaciona-se bem nas diferentas camadas sociais e hierárquicas.\n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(36 + ",", "");
                        }

                if ($scope.Respostas.toString().indexOf("," + 28 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 33 + ",") >= 0) {

                    $scope.processoDecisorio.push($scope.Avaliacao[12] + " é favorável às alianças e parcerias, seu processo decisório é, preferencialmente, consensual. No entanto, em situações críticas, esse perfil não vacilará em tomar decisão sozinho e responsabilizar-se pelas consequências. ");
                    //processoDecisorio += "";
                    //processoDecisorio += " Gosto de tomar decisões ouvindo os outros mas sei que sou o responsável pelas minhas escolhas.\n";
                    //processoDecisorio += "\n";
                    //processoDecisorio += "\n";
                    $scope.processoDecisorio.push(" Aprecia ouvir opniões, faz perguntas e dá respostas e pratica uma troca enriquecedora para embasamento das decisões a serem tomadas, demonstrando habilidade em lidar com divergências. ");
                    //processoDecisorio += " Gosto de desafios, não de perigos e, por isso, faço planos para o futuro com base na realidade de hoje.\n";
                    //processoDecisorio += "\n";
                    //processoDecisorio += "\n";
                    //validaprocessoDecisorio = 1;
                    $scope.Respostas = $scope.Respostas.toString().replace(28 + ",", "");
                    $scope.Respostas = $scope.Respostas.toString().replace(33 + ",", "");
                } else
                    if ($scope.Respostas.toString().indexOf("," + 28 + ",") >= 0) {
                        $scope.processoDecisorio.push($scope.Avaliacao[12] + " é favorável às alianças e parcerias, seu processo decisório é, preferencialmente, consensual. No entanto, em situações críticas, esse perfil não vacilará em tomar decisão sozinho e responsabilizar-se pelas consequências. ");
                        //processoDecisorio += " Gosto de tomar decisões ouvindo os outros mas sei que sou o responsável pelas minhas escolhas.\n";
                        //processoDecisorio += "\n";
                        //processoDecisorio += "\n";
                        $scope.Respostas = $scope.Respostas.toString().replace(28 + ",", "");
                    }
                    else
                        if ($scope.Respostas.toString().indexOf("," + 33 + ",") >= 0) {
                            //processoDecisorio += " Gosto de desafios, não de perigos e, por isso, faço planos para o futuro com base na realidade de hoje.\n";
                            $scope.processoDecisorio.push($scope.Avaliacao[12] + " aprecia ouvir opniões, faz perguntas e dá respostas e pratica uma troca enriquecedora para embasamento das decisões a serem tomadas, demonstrando habilidade em lidar com divergências. ");
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(33 + ",", "");
                        }


                if ($scope.Respostas.toString().indexOf("," + 29 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 32 + ",") >= 0) {
                    $scope.admConflitos.push($scope.Avaliacao[12] + " não gosta de aplicar panos quentes ou deixar as coisas se resolverem por si. Ao perceber o conflito irá ter com as partes, ouví-las, questioná-las e esgotar as razões de modo a esclarecer causas e consequências. Tenderá a transformar o conflito em negociação, fazendo com que as partes aprendam com o ocorrido.\n");
                    //admConflitos += "";
                    //admConflitos += " Não gosta de aplicar panos quentes ou deixar as coisas se resolverem por si. Ao perceber o conflito irá ter com as partes, ouví-las, questioná-las e esgotar as razões de modo a esclarecer causas e consequências. Tenderá a transformar o conflito em negociação, fazendo com que as partes aprendam com o ocorrido.\n";
                    //admConflitos += "\n";
                    //admConflitos += "\n";
                    $scope.admConflitos.push(" Quando ele próprio for uma das partes do conflito, abrir-se-à ao diálogo com a outra parte, argumentando na defesa de seu ponto de vista. Porém, se perceber que está errado, terá a humildade de reconhece-lo.");
                    //admConflitos += " Quando ele próprio for uma das partes do conflito, abrir-se-à ao diálogo com a outra parte, argumentando na defesa de seu ponto de vista. Porém, se perceber que está errado, terá a humildade de reconhece-lo.\n";
                    //validaadmConflitos = 1;
                    $scope.Respostas = $scope.Respostas.toString().replace(29 + ",", "");
                    $scope.Respostas = $scope.Respostas.toString().replace(32 + ",", "");
                } else
                    if ($scope.Respostas.toString().indexOf("," + 29 + ",") >= 0) {
                        //admConflitos += " Não gosta de aplicar panos quentes ou deixar as coisas se resolverem por si. Ao perceber o conflito irá ter com as partes, ouví-las, questioná-las e esgotar as razões de modo a esclarecer causas e consequências. Tenderá a transformar o conflito em negociação, fazendo com que as partes aprendam com o ocorrido.\n";
                        //admConflitos += "\n";
                        //admConflitos += "\n";
                        $scope.admConflitos.push($scope.Avaliacao[12] + " não gosta de aplicar panos quentes ou deixar as coisas se resolverem por si. Ao perceber o conflito irá ter com as partes, ouví-las, questioná-las e esgotar as razões de modo a esclarecer causas e consequências. Tenderá a transformar o conflito em negociação, fazendo com que as partes aprendam com o ocorrido.\n");
                        $scope.Respostas = $scope.Respostas.toString().replace(29 + ",", "");
                    }
                    else
                        if ($scope.Respostas.toString().indexOf("," + 32 + ",") >= 0) {
                            //admConflitos += " Quando ele próprio for uma das partes do conflito, abrir-se-à ao diálogo com a outra parte, argumentando na defesa de seu ponto de vista. Porém, se perceber que está errado, terá a humildade de reconhece-lo.\n";
                            //admConflitos += "\n";
                            //admConflitos += "\n";
                            $scope.admConflitos.push($scope.Avaliacao[12] + " quando ele próprio for uma das partes do conflito, abrir-se-à ao diálogo com a outra parte, argumentando na defesa de seu ponto de vista. Porém, se perceber que está errado, terá a humildade de reconhece-lo.");
                            $scope.Respostas = $scope.Respostas.toString().replace(32 + ",", "");
                        }
            }
            //


            //monta dados planejamento foco nos resultados

            //considerar o maior numero de marcações para o secundario, independente do total geral
            if (((k == 0 && planejamentoResultados.length < 1) || k > 0)) {
                //var planejamentoResultado
                //if ($scope.maioresResultados[k] == 'Q1')
                //{
                if (validaplanejamentoResultados < 1) {
                    if ($scope.Respostas.toString().indexOf("" + 1 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 24 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                        if (k == 0)
                            $scope.planejamentoResultados.push($scope.Avaliacao[12] + " com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros.");
                            //$scope.planejamentoResultados.push($scope.Avaliacao[12] + " bastante envolvido com a concretização do que é planejado , frequentemente sobrecarrega-se, puxando de volta para si, tarefas já delegadas, temendo que não aconteçam. Poderá também, nem delegar aquelas que julga mais difíceis, deixando para a equipe apenas a realização das rotinas a que já está acostumada e poderá realizar com o menor risco de erro. ");
                        else
                            $scope.planejamentoResultados.push(" Com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros.");
                            //$scope.planejamentoResultados.push(" Bastante envolvido com a concretização do que é planejado , frequentemente sobrecarrega-se, puxando de volta para si, tarefas já delegadas, temendo que não aconteçam. Poderá também, nem delegar aquelas que julga mais difíceis, deixando para a equipe apenas a realização das rotinas a que já está acostumada e poderá realizar com o menor risco de erro. ");
                        //planejamentoResultados += " ";
                        //planejamentoResultados += "Bastante envolvido com a concretização do que é planejado , frequentemente sobrecarrega-se, puxando de volta para si, tarefas já delegadas, temendo que não aconteçam. Poderá também, nem delegar aquelas que julga mais difíceis, deixando para a equipe apenas a realização das rotinas a que já está acostumada e poderá realizar com o menor risco de erro. Não costuma dar muitas explicações e investir tempo e energia no treinamento e desenvolvimento da equipe, por acreditar que a maioria das pessoas tem preguiça mental , o que as impede de raciocinar e aplicar os conteúdos às situações práticas da vida.\n";
                        //planejamentoResultados += " ";
                        //planejamentoResultados += "\n";
                        //planejamentoResultados += "\n";
                        $scope.planejamentoResultados.push(" Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferÊncia para garantir o êxito; porém, no \"não atingimento \", poderá tornar-se agressivo, atribuindo à equipe, ao processo, ao fornecedor... enfim, a qualquer fator externo a causa dos maus resultados. ");
                        //$scope.planejamentoResultados.push("Em razão disso, com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros. Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferência para garantir o êxito;  porém, no “não atingimento” , fica emocionalmente abalado , atribuindo à equipe, ao processo, ao fornecedor...enfim, a qualquer fator externo a responsabilidade pela qualidade dos resultados. ");
                        //planejamentoResultados += "Em razão disso, com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros. Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferência para garantir o êxito;  porém, no “não atingimento” , fica emocionalmente abalado , atribuindo à equipe, ao processo, ao fornecedor...enfim, a qualquer fator externo a responsabilidade pela qualidade dos resultados. \n";
                        //planejamentoResultados += "\n";
                        //planejamentoResultados += "\n";
                        $scope.Respostas = $scope.Respostas.toString().replace(1 + ",", "");
                        $scope.Respostas = $scope.Respostas.toString().replace(24 + ",", "");
                        if (k >= 1)
                            validaplanejamentoResultados = 1;
                    } else
                        if ($scope.Respostas.toString().indexOf("," + 2 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 40 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                            if (k == 0)
                                $scope.planejamentoResultados.push($scope.Avaliacao[12] + " não prioriza o planejamento, ao contrário, prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                            else
                                $scope.planejamentoResultados.push(" Não prioriza o planejamento, ao contrário, prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                            //if (k == 0)
                            //    $scope.planejamentoResultados.push($scope.Avaliacao[12] + " prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                            //else
                            //    $scope.planejamentoResultados.push("Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");

                            //planejamentoResultados += "";
                            //planejamentoResultados += "Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos.\n";
                            //planejamentoResultados += "\n";
                            //
                            $scope.planejamentoResultados.push(" Não da opiniões ou sugestões, temendo que o comprometam e submete-se sem discutir ao plano que lhe é apresentado, aceitando passivamente a idéia dos outros. Quando cobrado, colocará a responsabilidade dos resultados na equipe e tornar-se-à hostil, distante e autoritário em relação a ela. ");
                            //$scope.planejamentoResultados.push("Tende a delegar a tarefa ao subordinado sem, no entanto, opinar sobre a forma de fazer, deixando que isso aconteça de acordo com o estilo de cada um. No entanto, se for cobrado, colocará a responsabilidade sobre os resultados na equipe e tornar-se -à hostil, distante  e autoritário em relação a ela.");
                            //planejamentoResultados += "Tende a delegar a tarefa ao subordinado sem, no entanto, opinar sobre a forma de fazer, deixando que isso aconteça de acordo com o estilo de cada um. No entanto, se for cobrado, colocará a responsabilidade sobre os resultados na equipe e tornar-se -à hostil, distante  e autoritário em relação a ela.\n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(2 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(40 + ",", "");
                            if (k >= 1)
                                validaplanejamentoResultados = 1;
                        } else
                            if ($scope.Respostas.toString().indexOf("," + 4 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 30 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                if (k == 0)
                                    $scope.planejamentoResultados.push($scope.Avaliacao[12] + " prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão.");
                                else
                                    $scope.planejamentoResultados.push("Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão.");

                                //$scope.planejamentoResultados.push("Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão.");
                                //planejamentoResultados += "";
                                //planejamentoResultados += "Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão.\n";
                                //planejamentoResultados += "\n";
                                //planejamentoResultados += "\n";
                                $scope.planejamentoResultados.push("Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. ");
                                //planejamentoResultados += "Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados.\n";
                                //planejamentoResultados += "\n";
                                //planejamentoResultados += "\n";
                                $scope.Respostas = $scope.Respostas.toString().replace(4 + ",", "");
                                $scope.Respostas = $scope.Respostas.toString().replace(30 + ",", "");
                                if (k >= 1)
                                    validaplanejamentoResultados = 1;
                            }
                            else
                                if ($scope.Respostas.toString().indexOf("" + 1 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {

                                    if (k == 0)
                                        $scope.planejamentoResultados.push($scope.Avaliacao[12] + " com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros. ");
                                    else
                                        $scope.planejamentoResultados.push(" Com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros. ");

                                    //planejamentoResultados += "Bastante envolvido com a concretização do que é planejado , frequentemente sobrecarrega-se, puxando de volta para si, tarefas já delegadas, temendo que não aconteçam. Poderá também, nem delegar aquelas que julga mais difíceis, deixando para a equipe apenas a realização das rotinas a que já está acostumada e poderá realizar com o menor risco de erro.\n";
                                    //planejamentoResultados += "\n";
                                    //planejamentoResultados += "\n";
                                    $scope.Respostas = $scope.Respostas.toString().replace(1 + ",", "");
                                    if (k >= 1)
                                        validaplanejamentoResultados = 1;
                                } else
                                    if ($scope.Respostas.toString().indexOf("," + 24 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {

                                        if (k == 0)
                                            $scope.planejamentoResultados.push($scope.Avaliacao[12] + " quando os resultados são atingidos, demonstra-se orgulhoso da sua interferÊncia para garantir o êxito; porém, no \"não atingimento \", poderá tornar-se agressivo, atribuindo à equipe, ao processo, ao fornecedor... enfim, a qualquer fator externo a causa dos maus resultados.  ");
                                        else
                                            $scope.planejamentoResultados.push(" Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferÊncia para garantir o êxito; porém, no \"não atingimento \", poderá tornar-se agressivo, atribuindo à equipe, ao processo, ao fornecedor... enfim, a qualquer fator externo a causa dos maus resultados.  ");

                                        //$scope.planejamentoResultados.push(" Não costuma dar muitas explicações e investir tempo e energia no treinamento e desenvolvimento da equipe, por acreditar que a maioria das pessoas tem preguiça mental , o que as impede de raciocinar e aplicar os conteúdos às situações práticas da vida. ");
                                        //planejamentoResultados += " Não costuma dar muitas explicações e investir tempo e energia no treinamento e desenvolvimento da equipe, por acreditar que a maioria das pessoas tem preguiça mental , o que as impede de raciocinar e aplicar os conteúdos às situações práticas da vida. \n";
                                        //planejamentoResultados += "\n";
                                        //planejamentoResultados += "\n";
                                        $scope.Respostas = $scope.Respostas.toString().replace(24 + ",", "");
                                        if (k >= 1)
                                            validaplanejamentoResultados = 1;
                                    } else
                                        if ($scope.Respostas.toString().indexOf("," + 2 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {
                                            if (k == 0)
                                                $scope.planejamentoResultados.push($scope.Avaliacao[12] + " não prioriza o planejamento, ao contrário, prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                                            else
                                                $scope.planejamentoResultados.push(" Não prioriza o planejamento, ao contrário, prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");

                                            //$scope.planejamentoResultados.push("Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                                            //planejamentoResultados += "Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. \n";
                                            //planejamentoResultados += "\n";
                                            //planejamentoResultados += "\n";
                                            $scope.Respostas = $scope.Respostas.toString().replace(2 + ",", "");
                                            if (k >= 1)
                                                validaplanejamentoResultados = 1;
                                        } else
                                            if ($scope.Respostas.toString().indexOf("," + 40 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                                                if (k == 0)
                                                    $scope.planejamentoResultados.push($scope.Avaliacao[12] + " não da opiniões ou sugestões, temendo que o comprometam e submete-se sem discutir ao plano que lhe é apresentado, aceitando passivamente a idéia dos outros. Quando cobrado, colocará a responsabilidade dos resultados na equipe e tornar-se-à hostil, distante e autoritário em relação a ela.  ");
                                                else
                                                    $scope.planejamentoResultados.push(" Não da opiniões ou sugestões, temendo que o comprometam e submete-se sem discutir ao plano que lhe é apresentado, aceitando passivamente a idéia dos outros. Quando cobrado, colocará a responsabilidade dos resultados na equipe e tornar-se-à hostil, distante e autoritário em relação a ela.  ");

                                                //$scope.planejamentoResultados.push("Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                                                //planejamentoResultados += "Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. \n";
                                                //planejamentoResultados += "\n";
                                                //planejamentoResultados += "\n";
                                                $scope.Respostas = $scope.Respostas.toString().replace(40 + ",", "");
                                                if (k >= 1)
                                                    validaplanejamentoResultados = 1;
                                            } else
                                                if ($scope.Respostas.toString().indexOf("," + 4 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                                    if (k == 0)
                                                        $scope.planejamentoResultados.push($scope.Avaliacao[12] + " prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. ");
                                                    else
                                                        $scope.planejamentoResultados.push(" Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. ");

                                                    //$scope.planejamentoResultados.push("Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. ");
                                                    //planejamentoResultados += "Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. \n";
                                                    //planejamentoResultados += "\n";
                                                    //planejamentoResultados += "\n";
                                                    $scope.Respostas = $scope.Respostas.toString().replace(4 + ",", "");
                                                    if (k >= 1)
                                                        validaplanejamentoResultados = 1;
                                                } else
                                                    if ($scope.Respostas.toString().indexOf("," + 30 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {
                                                        if (k == 0)
                                                            $scope.planejamentoResultados.push($scope.Avaliacao[12] + " não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. ");
                                                        else
                                                            $scope.planejamentoResultados.push("Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. ");

                                                        //$scope.planejamentoResultados.push("Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. ");
                                                        //planejamentoResultados += "Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. \n";
                                                        //planejamentoResultados += "\n";
                                                        //planejamentoResultados += "\n";
                                                        $scope.Respostas = $scope.Respostas.toString().replace(30 + ",", "");
                                                        if (k >= 1)
                                                            validaplanejamentoResultados = 1;
                                                    }
                    if (k >= 1 && validaplanejamentoResultados < 1) {
                        //q4
                        //if ($scope.Respostas.toString().indexOf("," + 11 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 39 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //    planejamentoResultados += "";
                        //    planejamentoResultados += "Sente-se perdido se não trabalhar com objetivos e parte da realidade de hoje para o estabelecimento de metas, envolvendo  a equipe na discussão de estratégias necessárias ao atingimento. \n";
                        //    planejamentoResultados += "\n";
                        //    planejamentoResultados += "\n";
                        //    planejamentoResultados += "Delega tarefas e compartilha responsabilidades, acompanhando, avaliando e redirecionando  o trabalho , sempre que se fizer necessário, dividindo com a equipe o mérito dos resultados e assumindo, com o grupo, a responsabilidade também pelos erros e insucessos. \n";
                        //    planejamentoResultados += "\n";
                        //    planejamentoResultados += "\n";
                        //    validaplanejamentoResultados = 1;
                        //} else
                        //    if ($scope.Respostas.toString().indexOf("," + 11 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //        planejamentoResultados += "Sente-se perdido se não trabalhar com objetivos e parte da realidade de hoje para o estabelecimento de metas, envolvendo  a equipe na discussão de estratégias necessárias ao atingimento. \n";
                        //        planejamentoResultados += "\n";
                        //        planejamentoResultados += "\n";
                        //        validaplanejamentoResultados = 1;
                        //    } else {
                        //        if ($scope.Respostas.toString().indexOf("," + 39 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4')
                        //            planejamentoResultados += "Delega tarefas e compartilha responsabilidades, acompanhando, avaliando e redirecionando  o trabalho , sempre que se fizer necessário, dividindo com a equipe o mérito dos resultados e assumindo, com o grupo, a responsabilidade também pelos erros e insucessos. \n";
                        //        planejamentoResultados += "\n";
                        //        planejamentoResultados += "\n";
                        //        validaplanejamentoResultados = 1;
                        //    }
                        //q3
                        if ($scope.Respostas.toString().indexOf("," + 4 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 30 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {

                            $scope.planejamentoResultados.push(" Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. ");
                            $scope.planejamentoResultados.push(" Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. ");
                            //planejamentoResultados += "";
                            //planejamentoResultados += "Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. \n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. \n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            validaplanejamentoResultados = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(4 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(30 + ",", "");
                        } else
                            if ($scope.Respostas.toString().indexOf("," + 4 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                $scope.planejamentoResultados.push("Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. ");
                                //planejamentoResultados += "Prefere estabelecer metas que, garantidamente , serão atingidas pois, metas ousadas tendem mais a desmotivar a equipe, dificultando o atingimento dos resultados e evita um acompanhamento muito formal do andamento das tarefas, preferindo deixar o grupo trabalhar com maior liberdade, sem pressão. \n";
                                //planejamentoResultados += "\n";
                                //planejamentoResultados += "\n";
                                validaplanejamentoResultados = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(4 + ",", "");
                            } else
                                if ($scope.Respostas.toString().indexOf("," + 30 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                    $scope.planejamentoResultados.push("Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. ");
                                    //planejamentoResultados += "Não realiza reuniões formais para análise e acompanhamento de resultados e esquiva-se a toda a situação em que seja necessário assumir uma atitude menos popular na cobrança de resultados. \n";
                                    //planejamentoResultados += "\n";
                                    //planejamentoResultados += "\n";
                                    validaplanejamentoResultados = 1;
                                    $scope.Respostas = $scope.Respostas.toString().replace(30 + ",", "");
                                }
                        //q1

                        if ($scope.Respostas.toString().indexOf("" + 1 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 24 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {

                            $scope.planejamentoResultados.push(" Com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros.");

                            $scope.planejamentoResultados.push(" Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferÊncia para garantir o êxito; porém, no \"não atingimento \", poderá tornar-se agressivo, atribuindo à equipe, ao processo, ao fornecedor... enfim, a qualquer fator externo a causa dos maus resultados. ");

                            //planejamentoResultados += " ";
                            //planejamentoResultados += "Bastante envolvido com a concretização do que é planejado , frequentemente sobrecarrega-se, puxando de volta para si, tarefas já delegadas, temendo que não aconteçam. Poderá também, nem delegar aquelas que julga mais difíceis, deixando para a equipe apenas a realização das rotinas a que já está acostumada e poderá realizar com o menor risco de erro. Não costuma dar muitas explicações e investir tempo e energia no treinamento e desenvolvimento da equipe, por acreditar que a maioria das pessoas tem preguiça mental , o que as impede de raciocinar e aplicar os conteúdos às situações práticas da vida. \n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "Em razão disso, com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros. Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferência para garantir o êxito;  porém, no “não atingimento” , fica emocionalmente abalado , atribuindo à equipe, ao processo, ao fornecedor...enfim, a qualquer fator externo a responsabilidade pela qualidade dos resultados.  ";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            validaplanejamentoResultados = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(1 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(24 + ",", "");
                        } else
                            if ($scope.Respostas.toString().indexOf("" + 1 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                $scope.planejamentoResultados.push(" Com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros.");
                                //planejamentoResultados += "Bastante envolvido com a concretização do que é planejado , frequentemente sobrecarrega-se, puxando de volta para si, tarefas já delegadas, temendo que não aconteçam. Poderá também, nem delegar aquelas que julga mais difíceis, deixando para a equipe apenas a realização das rotinas a que já está acostumada e poderá realizar com o menor risco de erro. Não costuma dar muitas explicações e investir tempo e energia no treinamento e desenvolvimento da equipe, por acreditar que a maioria das pessoas tem preguiça mental , o que as impede de raciocinar e aplicar os conteúdos às situações práticas da vida. \n";
                                //planejamentoResultados += "\n";
                                //planejamentoResultados += "\n";
                                validaplanejamentoResultados = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(1 + ",", "");
                            } else
                                if ($scope.Respostas.toString().indexOf("," + 24 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {

                                    $scope.planejamentoResultados.push(" Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferÊncia para garantir o êxito; porém, no \"não atingimento \", poderá tornar-se agressivo, atribuindo à equipe, ao processo, ao fornecedor... enfim, a qualquer fator externo a causa dos maus resultados. ");//$scope.planejamentoResultados.push("Em razão disso, com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros. Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferência para garantir o êxito;  porém, no “não atingimento” , fica emocionalmente abalado , atribuindo à equipe, ao processo, ao fornecedor...enfim, a qualquer fator externo a responsabilidade pela qualidade dos resultados.  ");
                                    //planejamentoResultados += "Em razão disso, com mais frequência, trabalha isoladamente no estabelecimento de metas e objetivos, só depois informando ao grupo o que deve ser feito, cobrando resultados e buscando os responsáveis pelos erros. Quando os resultados são atingidos, demonstra-se orgulhoso da sua interferência para garantir o êxito;  porém, no “não atingimento” , fica emocionalmente abalado , atribuindo à equipe, ao processo, ao fornecedor...enfim, a qualquer fator externo a responsabilidade pela qualidade dos resultados.  ";
                                    //planejamentoResultados += "\n";
                                    //planejamentoResultados += "\n";
                                    validaplanejamentoResultados = 1;
                                    $scope.Respostas = $scope.Respostas.toString().replace(24 + ",", "");
                                }

                        //q2
                        if ($scope.Respostas.toString().indexOf("," + 2 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 40 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {

                            $scope.planejamentoResultados.push(" Não prioriza o planejamento, ao contrário, prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                            $scope.planejamentoResultados.push(" Não da opiniões ou sugestões, temendo que o comprometam e submete-se sem discutir ao plano que lhe é apresentado, aceitando passivamente a idéia dos outros. Quando cobrado, colocará a responsabilidade dos resultados na equipe e tornar-se-à hostil, distante e autoritário em relação a ela. ");
                            //planejamentoResultados += "";
                            //planejamentoResultados += "Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. \n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "Tende a delegar a tarefa ao subordinado sem, no entanto, opinar sobre a forma de fazer, deixando que isso aconteça de acordo com o estilo de cada um. No entanto, se for cobrado, colocará a responsabilidade sobre os resultados na equipe e tornar-se -à hostil, distante  e autoritário em relação a ela. \n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            validaplanejamentoResultados = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(2 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(40 + ",", "");
                        } else
                            if ($scope.Respostas.toString().indexOf("," + 2 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                //$scope.planejamentoResultados.push("Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                                $scope.planejamentoResultados.push(" Não prioriza o planejamento, ao contrário, prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. ");
                                //planejamentoResultados += "Prefere ater-se a rotina de cada dia do que investir tempo e energia em planos de longo prazo que ninguém garante que serão atingidos. \n";
                                //planejamentoResultados += "\n";
                                //planejamentoResultados += "\n";
                                validaplanejamentoResultados = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(2 + ",", "");
                            }
                        if ($scope.Respostas.toString().indexOf("," + 40 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                            //$scope.planejamentoResultados.push("Tende a delegar a tarefa ao subordinado sem, no entanto, opinar sobre a forma de fazer, deixando que isso aconteça de acordo com o estilo de cada um. No entanto, se for cobrado, colocará a responsabilidade sobre os resultados na equipe e tornar-se -à hostil, distante  e autoritário em relação a ela. ");
                            $scope.planejamentoResultados.push(" Não da opiniões ou sugestões, temendo que o comprometam e submete-se sem discutir ao plano que lhe é apresentado, aceitando passivamente a idéia dos outros. Quando cobrado, colocará a responsabilidade dos resultados na equipe e tornar-se-à hostil, distante e autoritário em relação a ela. ");
                            //planejamentoResultados += "Tende a delegar a tarefa ao subordinado sem, no entanto, opinar sobre a forma de fazer, deixando que isso aconteça de acordo com o estilo de cada um. No entanto, se for cobrado, colocará a responsabilidade sobre os resultados na equipe e tornar-se -à hostil, distante  e autoritário em relação a ela. \n";
                            //planejamentoResultados += "\n";
                            //planejamentoResultados += "\n";
                            validaplanejamentoResultados = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(40 + ",", "");
                        }
                    }
                }
            }

            /**********************************************/
            //monta dados liderança
            if (((k == 0 && lideranca.length < 1) || k > 0)) {
                if (validalideranca < 1) {
                    //if ($scope.maioresResultados[k] == 'Q1') {
                    if ($scope.Respostas.toString().indexOf("," + 8 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 23 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {

                        if (k == 0)
                            $scope.lideranca.push($scope.Avaliacao[12] + " delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.");
                        else
                            $scope.lideranca.push(" Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.");

                        $scope.lideranca.push(" Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder. ");
                        //lideranca += " ";
                        //lideranca += "	Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem. \n";
                        //lideranca += "\n";
                        //lideranca += "\n";
                        //lideranca += " Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder. ";
                        //lideranca += "\n";
                        //lideranca += "\n";
                        $scope.Respostas = $scope.Respostas.toString().replace(8 + ",", "");
                        $scope.Respostas = $scope.Respostas.toString().replace(23 + ",", "");
                        if (k >= 1)
                            validalideranca = 1;
                    } else
                        if ($scope.Respostas.toString().indexOf("," + 3 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 37 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                            if (k == 0)
                                $scope.lideranca.push($scope.Avaliacao[12] + " delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.");
                            else
                                $scope.lideranca.push(" Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.");

                            //lideranca += "	Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas. \n";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            $scope.lideranca.push("  Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva. ");
                            //lideranca += "  Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva. ";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(3 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(37 + ",", "");
                            if (k >= 1)
                                validalideranca = 1;
                        } else
                            if ($scope.Respostas.toString().indexOf("," + 5 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 26 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                if (k == 0)
                                    $scope.lideranca.push($scope.Avaliacao[12] + " alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.");
                                else
                                    $scope.lideranca.push(" Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.");

                                //lideranca += " Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação. \n";
                                //lideranca += "\n";
                                //lideranca += "\n";
                                $scope.lideranca.push(" Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos. ");
                                //lideranca += " Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos. ";
                                //lideranca += "\n";
                                //lideranca += "\n";
                                $scope.Respostas = $scope.Respostas.toString().replace(5 + ",", "");
                                $scope.Respostas = $scope.Respostas.toString().replace(26 + ",", "");
                                if (k >= 1)
                                    validalideranca = 1;
                            } else
                                if ($scope.Respostas.toString().indexOf("," + 8 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                                    if (k == 0)
                                        $scope.lideranca.push($scope.Avaliacao[12] + " delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.");
                                    else
                                        $scope.lideranca.push(" Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.");

                                    //lideranca += "	Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem. \n";
                                    //lideranca += "\n";
                                    //lideranca += "\n";
                                    $scope.Respostas = $scope.Respostas.toString().replace(8 + ",", "");
                                    if (k >= 1)
                                        validalideranca = 1;
                                }
                                else if ($scope.Respostas.toString().indexOf("," + 23 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                                    if (k == 0)
                                        $scope.lideranca.push($scope.Avaliacao[12] + " usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder.");
                                    else
                                        $scope.lideranca.push(" Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder.");

                                    //lideranca += " Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder.\n";
                                    //lideranca += "\n";
                                    //lideranca += "\n";
                                    $scope.Respostas = $scope.Respostas.toString().replace(23 + ",", "");
                                    if (k >= 1)
                                        validalideranca = 1;
                                } else
                                    if ($scope.Respostas.toString().indexOf("," + 3 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                                        if (k == 0)
                                            $scope.lideranca.push($scope.Avaliacao[12] + " delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.");
                                        else
                                            $scope.lideranca.push(" Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.");

                                        //lideranca += "	Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.\n";
                                        //lideranca += "\n";
                                        //lideranca += "\n";
                                        $scope.Respostas = $scope.Respostas.toString().replace(3 + ",", "");
                                        if (k >= 1)
                                            validalideranca = 1;
                                    }
                                    else if ($scope.Respostas.toString().indexOf("," + 37 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                                        if (k == 0)
                                            $scope.lideranca.push($scope.Avaliacao[12] + " não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva.");
                                        else
                                            $scope.lideranca.push(" Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva.");

                                        //lideranca += "  Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva.\n";
                                        //lideranca += "\n";
                                        //lideranca += "\n";
                                        $scope.Respostas = $scope.Respostas.toString().replace(37 + ",", "");
                                        if (k >= 1)
                                            validalideranca = 1;
                                    } else
                                        if ($scope.Respostas.toString().indexOf("," + 5 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                            if (k == 0)
                                                $scope.lideranca.push($scope.Avaliacao[12] + " alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.");
                                            else
                                                $scope.lideranca.push(" Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.");

                                            //lideranca += " Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.\n";
                                            //lideranca += "\n";
                                            //lideranca += "\n";
                                            $scope.Respostas = $scope.Respostas.toString().replace(5 + ",", "");
                                            if (k >= 1)
                                                validalideranca = 1;
                                        }
                                        else if ($scope.Respostas.toString().indexOf("," + 26 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q4') {

                                            if (k == 0)
                                                $scope.lideranca.push($scope.Avaliacao[12] + " avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos.");
                                            else
                                                $scope.lideranca.push(" Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos.");

                                            //lideranca += " Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos.\n ";
                                            //lideranca += "\n";
                                            //lideranca += "\n";
                                            $scope.Respostas = $scope.Respostas.toString().replace(26 + ",", "");
                                            if (k >= 1)
                                                validalideranca = 1;
                                        }
                    if (k >= 1 && validalideranca < 1) {
                        //q4
                        //if ($scope.Respostas.toString().indexOf("," + 19 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 38 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {

                        //    lideranca += " Tem um Plano de Ação claro e consistente, com vista a realização das tarefas e ao desenvolvimento das pessoas. Envolve a equipe nesse plano, de modo a motivar cada colaborador à realização do que lhe é delegado, buscando dar o melhor possível.\n";
                        //    lideranca += "\n";
                        //    lideranca += "\n";
                        //    lideranca += " Incentiva o esforço coletivo e estimula a cooperação reduzindo, com isso, a competição. Usa a crítica e o elogio de forma certa no exercício do feedback, sendo que não critica em público, mas de forma privada e com foco no potencial do indivíduo.\n ";
                        //    lideranca += "\n";
                        //    lideranca += "\n";
                        //    validalideranca = 1;
                        //}
                        //else {
                        //    if ($scope.Respostas.toString().indexOf("," + 19 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //        lideranca += " Tem um Plano de Ação claro e consistente, com vista a realização das tarefas e ao desenvolvimento das pessoas. Envolve a equipe nesse plano, de modo a motivar cada colaborador à realização do que lhe é delegado, buscando dar o melhor possível.\n";
                        //        lideranca += "\n";
                        //        lideranca += "\n";
                        //        validalideranca = 1;
                        //    } else
                        //    if ($scope.Respostas.toString().indexOf("," + 38 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4')
                        //        lideranca += " Incentiva o esforço coletivo e estimula a cooperação reduzindo, com isso, a competição. Usa a crítica e o elogio de forma certa no exercício do feedback, sendo que não critica em público, mas de forma privada e com foco no potencial do indivíduo. \n";
                        //        lideranca += "\n";
                        //        lideranca += "\n";
                        //    validalideranca = 1;
                        //}
                        //q3
                        if ($scope.Respostas.toString().indexOf("," + 5 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 26 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                            //lideranca += "";
                            $scope.lideranca.push(" Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.");
                            //lideranca += " Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.\n";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            $scope.lideranca.push(" Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos. ");
                            //lideranca += " Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos. \n";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            validalideranca = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(5 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(26 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 5 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                //lideranca += " Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.\n";
                                $scope.lideranca.push(" Alegre e informal, trata a equipe de igual para igual, evitando fazer uso da autoridade que lhe é conferida pela hierarquia. Valoriza muito as relações sociais junto ao grupo, bem como as reuniões de comemoração. Detesta as reuniões de avaliação.");
                                //lideranca += "\n";
                                //lideranca += "\n";
                                validalideranca = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(5 + ",", "");
                            } else
                            if ($scope.Respostas.toString().indexOf("," + 26 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                //lideranca += " Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos. \n";
                                //lideranca += "\n";
                                $scope.lideranca.push(" Avesso à crítica, tanto como emissor quanto como receptor, ao dar feedback costuma minimizar os aspectos negativos e hipervalorizar os positivos, numa linguagem não assertiva, sempre evitando conflitos e descontentamentos. ");
                                //lideranca += "\n";
                                validalideranca = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(26 + ",", "");
                            }
                        }
                        //q1

                        if ($scope.Respostas.toString().indexOf("," + 8 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 23 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                            //lideranca += " ";
                            //lideranca += "	Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.\n";
                            $scope.lideranca.push("	Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.");
                            //lideranca += "\n";
                            //lideranca += "\n";
                            $scope.lideranca.push(" Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder. ");
                            //lideranca += " Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder.\n ";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            validalideranca = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(8 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(23 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 8 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                //lideranca += "	Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.\n";
                                $scope.lideranca.push("	Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem.");
                                //lideranca += "\n";
                                //lideranca += "\n";
                                validalideranca = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(8 + ",", "");
                            } else
                            if ($scope.Respostas.toString().indexOf("," + 23 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                //lideranca += " Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder.\n ";
                                //lideranca += "\n";
                                //lideranca += "\n";
                                $scope.lideranca.push(" Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder. ");
                                validalideranca = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(23 + ",", "");
                            }
                        }
                        //q2
                        if ($scope.Respostas.toString().indexOf("," + 3 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 37 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                            $scope.lideranca.push("	Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.");
                            //lideranca += "";
                            //lideranca += "	Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.\n";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            $scope.lideranca.push("  Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva.");
                            //lideranca += "  Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva.\n ";
                            //lideranca += "\n";
                            //lideranca += "\n";
                            validalideranca = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(3 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(37 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 3 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                $scope.lideranca.push("	Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.");
                                //lideranca += "	Delega sem acompanhamento e mantém-se distante dos problemas. Sempre que pode, mostra-se inacessível e prefere envolver-se com tarefas do que com pessoas.\n";
                                //lideranca += "\n";
                                //lideranca += "\n";
                                validalideranca = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(3 + ",", "");
                            } else
                            if ($scope.Respostas.toString().indexOf("," + 37 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                //lideranca += "  Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva.\n ";
                                //lideranca += "\n";
                                //lideranca += "\n";
                                $scope.lideranca.push("  Não realiza reuniões e, quando delas participa, evita opinar. Não treina a equipe, não avalia e não gosta de dar feedback. Quando essa tarefa lhe é exigida, arranja todas as desculpas para adiá-la ao máximo e, ao realiza-la, tenta falar o mínimo, usando uma linguagem não assertiva.");
                                validalideranca = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(37 + ",", "");
                            }
                        }
                    }
                }
            }


            /**********************************************/
            //monta dados comunicação Relacionamento Interpessoal

            if (((k == 0 && comunicacaoRelacionamento.length < 1) || k > 0)) {
                //if ($scope.maioresResultados[k] == 'Q1') {
                if (validacomunicacaoRelacionamento < 1) {
                    if ($scope.Respostas.toString().indexOf("," + 12 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 15 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                        if (k == 0)
                            $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " possui uma comunicação impositiva, geralmente em uma só via: EU falo e VOCÊ escuta, colocando a sua opnião como expressão de verdade. Diz o que pensa, doa a quem doer, não foge à discussão, manifestando suas emoções ainda quando negativas e não entrega os pontos antes de queimar todos os trunfos. ");
                            //$scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " comunica-se de forma agressiva, colocando a sua opinião como expressão de verdade absoluta. Diz o que pensa, doa a quem doer e numa discussão não entrega os pontos antes de queimar todos os trunfos, sentindo prazer em dar a cartada final, sem abrir mão de suas convicções. ");
                        else
                            $scope.comunicacaoRelacionamento.push(" Possui uma comunicação impositiva, geralmente em uma só via: EU falo e VOCÊ escuta, colocando a sua opnião como expressão de verdade. Diz o que pensa, doa a quem doer, não foge à discussão, manifestando suas emoções ainda quando negativas e não entrega os pontos antes de queimar todos os trunfos. ");
                        //comunicacaoRelacionamento += "";
                        //comunicacaoRelacionamento += " Comunica-se de forma agressiva, colocando a sua opinião como expressão de verdade absoluta. Diz o que pensa, doa a quem doer e numa discussão não entrega os pontos antes de queimar todos os trunfos, sentindo prazer em dar a cartada final, sem abrir mão de suas convicções. \n";
                        //comunicacaoRelacionamento += "\n";
                        //comunicacaoRelacionamento += "\n";
                        //$scope.comunicacaoRelacionamento.push(" Faz apologia das suas vitórias, tendendo a hiperdimensioná-las e subestimando os demais. Apresenta uma escuta reduzida, dando muito mais atenção ao seu diálogo interior. Tende a só considerar novas idéias quando estas servem para reforçar a sua posição.");
                        $scope.comunicacaoRelacionamento.push(" Sente prazer em dar a cartada final, sem abrir mão de suas convicções. Este perfil tende a fazer apologia das suas vitórias, citando a si mesmo como exemplo do que deve ser feito. ");
                        //comunicacaoRelacionamento += " Faz apologia das suas vitórias, tendendo a hiperdimensioná-las e subestimando os demais. Apresenta uma escuta reduzida, dando muito mais atenção ao seu diálogo interior. Tende a só considerar novas idéias quando estas servem para reforçar a sua posição. \n";
                        //comunicacaoRelacionamento += "\n";
                        //comunicacaoRelacionamento += "\n";
                        $scope.Respostas = $scope.Respostas.toString().replace(12 + ",", "");
                        $scope.Respostas = $scope.Respostas.toString().replace(15 + ",", "");
                        if (k >= 1)
                            validacomunicacaoRelacionamento = 1;
                    } else
                        if ($scope.Respostas.toString().indexOf("," + 17 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 35 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {
                            //comunicacaoRelacionamento += "";
                            if (k == 0)
                                $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. ");
                            else
                                $scope.comunicacaoRelacionamento.push(" Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. ");
                            //comunicacaoRelacionamento += " Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            $scope.comunicacaoRelacionamento.push(" É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. ");
                            //comunicacaoRelacionamento += " É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(17 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(35 + ",", "");
                            if (k >= 1)
                                validacomunicacaoRelacionamento = 1;
                        } else
                            if ($scope.Respostas.toString().indexOf("," + 6 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 10 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {
                                //comunicacaoRelacionamento += "";

                                if (k == 0)
                                    $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. ");
                                else
                                    $scope.comunicacaoRelacionamento.push(" Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. ");

                                //comunicacaoRelacionamento += " Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                $scope.comunicacaoRelacionamento.push("Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. ");
                                //comunicacaoRelacionamento += " Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                $scope.Respostas = $scope.Respostas.toString().replace(6 + ",", "");
                                $scope.Respostas = $scope.Respostas.toString().replace(10 + ",", "");
                                if (k >= 1)
                                    validacomunicacaoRelacionamento = 1;
                            } else
                                if ($scope.Respostas.toString().indexOf("," + 12 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                                    if (k == 0)
                                        $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " possui uma comunicação impositiva, geralmente em uma só via: EU falo e VOCÊ escuta, colocando a sua opnião como expressão de verdade. Diz o que pensa, doa a quem doer, não foge à discussão, manifestando suas emoções ainda quando negativas e não entrega os pontos antes de queimar todos os trunfos. ");
                                    else
                                        $scope.comunicacaoRelacionamento.push(" Possui uma comunicação impositiva, geralmente em uma só via: EU falo e VOCÊ escuta, colocando a sua opnião como expressão de verdade. Diz o que pensa, doa a quem doer, não foge à discussão, manifestando suas emoções ainda quando negativas e não entrega os pontos antes de queimar todos os trunfos. ");
                                    //comunicacaoRelacionamento += "	Delega mal por não acreditar na capacidade alheia, o que o torna centralizador, puxando para si responsabilidades alheias, temendo que não aconteçam. Não treina e nem desenvolve pessoas, preferindo ser o único que sabe no meio dos que não sabem. \n";
                                    //comunicacaoRelacionamento += "\n";
                                    //comunicacaoRelacionamento += "\n";
                                    $scope.Respostas = $scope.Respostas.toString().replace(12 + ",", "");
                                    if (k >= 1)
                                        validacomunicacaoRelacionamento = 1;
                                }
                                else if ($scope.Respostas.toString().indexOf("," + 15 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                                    if (k == 0)
                                        $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " sente prazer em dar a cartada final, sem abrir mão de suas convicções. Este perfil tende a fazer apologia das suas vitórias, citando a si mesmo como exemplo do que deve ser feito. ");
                                    else
                                        $scope.comunicacaoRelacionamento.push(" Sente prazer em dar a cartada final, sem abrir mão de suas convicções. Este perfil tende a fazer apologia das suas vitórias, citando a si mesmo como exemplo do que deve ser feito. ");

                                    //$scope.comunicacaoRelacionamento.push(" Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder. ");
                                    //comunicacaoRelacionamento += " Usa a crítica qual chicote de feitor e seus feedbacks são ferinos e desmotivadores . Não gosta de elogiar e prefere ter subordinados submissos e não questionadores. Não costuma dividir o poder. ";
                                    //comunicacaoRelacionamento += "\n";
                                    //comunicacaoRelacionamento += "\n";
                                    $scope.Respostas = $scope.Respostas.toString().replace(15 + ",", "");
                                    if (k >= 1)
                                        validacomunicacaoRelacionamento = 1;
                                } else
                                    if ($scope.Respostas.toString().indexOf("," + 17 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                                        if (k == 0)
                                            $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. ");
                                        else
                                            $scope.comunicacaoRelacionamento.push(" Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. ");

                                        //$scope.comunicacaoRelacionamento.push(" Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. ");
                                        //comunicacaoRelacionamento += " Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. \n";
                                        //comunicacaoRelacionamento += "\n";
                                        //comunicacaoRelacionamento += "\n";
                                        $scope.Respostas = $scope.Respostas.toString().replace(17 + ",", "");
                                        if (k >= 1)
                                            validacomunicacaoRelacionamento = 1;
                                    }
                                    else if ($scope.Respostas.toString().indexOf("," + 35 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {
                                        if (k == 0)
                                            $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " é defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. ");
                                        else
                                            $scope.comunicacaoRelacionamento.push(" É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. ");


                                        //$scope.comunicacaoRelacionamento.push(" É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. ");
                                        //comunicacaoRelacionamento += " É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. \n";
                                        //comunicacaoRelacionamento += "\n";
                                        //comunicacaoRelacionamento += "\n";
                                        $scope.Respostas = $scope.Respostas.toString().replace(35 + ",", "");
                                        if (k >= 1)
                                            validacomunicacaoRelacionamento = 1;
                                    } else
                                        if ($scope.Respostas.toString().indexOf("," + 6 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                            if (k == 0)
                                                $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. ");
                                            else
                                                $scope.comunicacaoRelacionamento.push(" Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. ");

                                            //$scope.comunicacaoRelacionamento.push(" Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. ");
                                            //comunicacaoRelacionamento += " Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. \n";
                                            //comunicacaoRelacionamento += "\n";
                                            //comunicacaoRelacionamento += "\n";
                                            $scope.Respostas = $scope.Respostas.toString().replace(6 + ",", "");
                                            if (k >= 1)
                                                validacomunicacaoRelacionamento = 1;
                                        }
                                        else if ($scope.Respostas.toString().indexOf("," + 10 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {
                                            if (k == 0)
                                                $scope.comunicacaoRelacionamento.push($scope.Avaliacao[12] + " gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. ");
                                            else
                                                $scope.comunicacaoRelacionamento.push(" Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. ");
                                            //$scope.comunicacaoRelacionamento.push(" Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. ");
                                            //comunicacaoRelacionamento += " Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. \n";
                                            //comunicacaoRelacionamento += "\n";
                                            //comunicacaoRelacionamento += "\n";
                                            $scope.Respostas = $scope.Respostas.toString().replace(10 + ",", "");
                                            if (k >= 1)
                                                validacomunicacaoRelacionamento = 1;
                                        }

                    if (k >= 1 && validacomunicacaoRelacionamento < 1) {
                        //q4
                        //if ($scope.Respostas.toString().indexOf("," + 20 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 36 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //    comunicacaoRelacionamento += "";
                        //    comunicacaoRelacionamento += " Pratica uma comunicação assertiva, falando de seus erros e acertos e estimula as pessoas a exporem suas idéias, ouvindo-as com atenção e respeito. É humilde, sendo capaz de aprender com os outros , independente de posição social ou hierárquica. \n";
                        //    comunicacaoRelacionamento += "\n";
                        //    comunicacaoRelacionamento += "\n";
                        //    comunicacaoRelacionamento += " Dá e recebe feedback, dentro de uma comunicação de verdade e respeito e, quando faz perguntas espera receber respostas diretas. Relaciona-se bem nas diferentas camadas sociais e hierárquicas. \n";
                        //    comunicacaoRelacionamento += "\n";
                        //    comunicacaoRelacionamento += "\n";
                        //    validacomunicacaoRelacionamento = 1;
                        //}
                        //else {
                        //    if ($scope.Respostas.toString().indexOf("," + 20 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //        comunicacaoRelacionamento += " Pratica uma comunicação assertiva, falando de seus erros e acertos e estimula as pessoas a exporem suas idéias, ouvindo-as com atenção e respeito. É humilde, sendo capaz de aprender com os outros , independente de posição social ou hierárquica. \n";
                        //        comunicacaoRelacionamento += "\n";
                        //        comunicacaoRelacionamento += "\n";
                        //        validacomunicacaoRelacionamento = 1;
                        //    }
                        //    if ($scope.Respostas.toString().indexOf("," + 36 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //        comunicacaoRelacionamento += " Dá e recebe feedback, dentro de uma comunicação de verdade e respeito e, quando faz perguntas espera receber respostas diretas. Relaciona-se bem nas diferentas camadas sociais e hierárquicas. \n";
                        //        comunicacaoRelacionamento += "\n";
                        //        comunicacaoRelacionamento += "\n";
                        //        validacomunicacaoRelacionamento = 1;
                        //    }
                        //}
                        //q3
                        if ($scope.Respostas.toString().indexOf("," + 6 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 10 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {

                            $scope.comunicacaoRelacionamento.push(" Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. ");
                            $scope.comunicacaoRelacionamento.push(" Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. ");
                            //comunicacaoRelacionamento += "";
                            //comunicacaoRelacionamento += " Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += " Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            validacomunicacaoRelacionamento = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(6 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(10 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 6 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                $scope.comunicacaoRelacionamento.push(" Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. ");
                                //comunicacaoRelacionamento += " Extrovertido, relaciona-se facilmente, tratando a todos de maneira amistosa e informal. A maioria das pessoas o (a) considera alegre e bem humorado(a). Evita assuntos desagradáveis e notícias tristes, busca agradar a todos, age de forma conciliadora, pois é avesso(a) a conflitos e discórdias. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                validacomunicacaoRelacionamento = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(6 + ",", "");
                            }else
                            if ($scope.Respostas.toString().indexOf("," + 10 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                $scope.comunicacaoRelacionamento.push(" Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. ");
                                //comunicacaoRelacionamento += " Gosta de fazer piadas e brincadeiras para alegrar o ambiente; conta tudo de si, adora ouvir os outros, tende a demonstrar aprovação até mesmo a posições antagônicas, por achar que todo o ponto de vista deva ser respeitado. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                validacomunicacaoRelacionamento = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(10 + ",", "");
                            }
                        }
                        //q1

                        if ($scope.Respostas.toString().indexOf("," + 12 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 15 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                            $scope.comunicacaoRelacionamento.push(" Possui uma comunicação impositiva, geralmente em uma só via: EU falo e VOCÊ escuta, colocando a sua opnião como expressão de verdade. Diz o que pensa, doa a quem doer, não foge à discussão, manifestando suas emoções ainda quando negativas e não entrega os pontos antes de queimar todos os trunfos. ");
                            $scope.comunicacaoRelacionamento.push(" Sente prazer em dar a cartada final, sem abrir mão de suas convicções. Este perfil tende a fazer apologia das suas vitórias, citando a si mesmo como exemplo do que deve ser feito. ");
                            //comunicacaoRelacionamento += "";
                            //comunicacaoRelacionamento += " Comunica-se de forma agressiva, colocando a sua opinião como expressão de verdade absoluta. Diz o que pensa, doa a quem doer e numa discussão não entrega os pontos antes de queimar todos os trunfos, sentindo prazer em dar a cartada final, sem abrir mão de suas convicções. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += " Faz apologia das suas vitórias, tendendo a hiperdimensioná-las e subestimando os demais. Apresenta uma escuta reduzida, dando muito mais atenção ao seu diálogo interior. Tende a só considerar novas idéias quando estas servem para reforçar a sua posição. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            validacomunicacaoRelacionamento = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(12 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(15 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 12 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                $scope.comunicacaoRelacionamento.push(" Possui uma comunicação impositiva, geralmente em uma só via: EU falo e VOCÊ escuta, colocando a sua opnião como expressão de verdade. Diz o que pensa, doa a quem doer, não foge à discussão, manifestando suas emoções ainda quando negativas e não entrega os pontos antes de queimar todos os trunfos. ");
                                //comunicacaoRelacionamento += " Comunica-se de forma agressiva, colocando a sua opinião como expressão de verdade absoluta. Diz o que pensa, doa a quem doer e numa discussão não entrega os pontos antes de queimar todos os trunfos, sentindo prazer em dar a cartada final, sem abrir mão de suas convicções. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                validacomunicacaoRelacionamento = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(12 + ",", "");
                            }else
                            if ($scope.Respostas.toString().indexOf("," + 15 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                $scope.comunicacaoRelacionamento.push(" Sente prazer em dar a cartada final, sem abrir mão de suas convicções. Este perfil tende a fazer apologia das suas vitórias, citando a si mesmo como exemplo do que deve ser feito. ");
                                //comunicacaoRelacionamento += " Faz apologia das suas vitórias, tendendo a hiperdimensioná-las e subestimando os demais. Apresenta uma escuta reduzida, dando muito mais atenção ao seu diálogo interior. Tende a só considerar novas idéias quando estas servem para reforçar a sua posição. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                validacomunicacaoRelacionamento = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(15 + ",", "");
                            }else{
                        //q2
                        if ($scope.Respostas.toString().indexOf("," + 17 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 35 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                            $scope.comunicacaoRelacionamento.push(" Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. ");
                            $scope.comunicacaoRelacionamento.push(" É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. ");
                            //comunicacaoRelacionamento += "";
                            //comunicacaoRelacionamento += " Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += " É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. \n";
                            //comunicacaoRelacionamento += "\n";
                            //comunicacaoRelacionamento += "\n";
                            validacomunicacaoRelacionamento = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(17 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(35 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 17 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                //comunicacaoRelacionamento += " Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                $scope.comunicacaoRelacionamento.push(" Apresenta a Comunicação comprometida pela omissão, preferindo calar ao invés de falar pois, assim, não poderão lhe cobrar pelo que não disse. Tem por hábito não dar opinião sem que isso lhe seja exigido e, ao fazê-lo, não toma partido pois considera a neutralidade a forma mais fácil de se posicionar. ");
                                validacomunicacaoRelacionamento = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(17 + ",", "");
                            }else
                            if ($scope.Respostas.toString().indexOf("," + 37 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                //comunicacaoRelacionamento += " É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. \n";
                                //comunicacaoRelacionamento += "\n";
                                //comunicacaoRelacionamento += "\n";
                                $scope.comunicacaoRelacionamento.push(" É defensivo, resguardando-se , evitando exposição e recorrendo a estratégias de fuga como a desculpa e a queixa. Desconfiado, sempre questiona se por trás de um  bom trato não existem outros interesses envolvidos e não acredita em promessas antes que se cumpram. Não costuma confiar para não correr o risco de se decepcionar. Reservado e econômico nas amizades. ");
                                validacomunicacaoRelacionamento = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(37 + ",", "");
                            }
                        }
                }
                        }
                    }
                }

            }
            /**********************************************/
            //monta dados processo decisório


            if (((k == 0 && processoDecisorio.length < 1) || k > 0)) {
                //if ($scope.maioresResultados[k] == 'Q1') {
                if (validaprocessoDecisorio < 1) {
                    if ($scope.Respostas.toString().indexOf("," + 13 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 16 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {

                        if (k == 0)
                            $scope.processoDecisorio.push($scope.Avaliacao[12] + " toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo.");
                        else
                            $scope.processoDecisorio.push(" Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo.");

                        //processoDecisorio += "";
                        //processoDecisorio += " Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo.\n";
                        //processoDecisorio += "\n";
                        //processoDecisorio += "\n";
                        $scope.processoDecisorio.push(" Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario.");
                        //processoDecisorio += " Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario.\n";
                        //processoDecisorio += "\n";
                        //processoDecisorio += "\n";
                        $scope.Respostas = $scope.Respostas.toString().replace(13 + ",", "");
                        $scope.Respostas = $scope.Respostas.toString().replace(23 + ",", "");
                        if (k >= 1)
                            validaprocessoDecisorio = 1;
                    } else
                        if ($scope.Respostas.toString().indexOf("," + 18 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 34 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                            if (k == 0)
                                $scope.processoDecisorio.push($scope.Avaliacao[12] + " decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos.");
                            else
                                $scope.processoDecisorio.push(" Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos.");

                            //processoDecisorio += "";
                            //processoDecisorio += " Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos.\n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            $scope.processoDecisorio.push(" Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador. ");
                            //processoDecisorio += " Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador. \n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            $scope.Respostas = $scope.Respostas.toString().replace(18 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(34 + ",", "");
                            if (k >= 1)
                                validaprocessoDecisorio = 1;
                        } else
                            if ($scope.Respostas.toString().indexOf("," + 7 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 22 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                if (k == 0)
                                    $scope.processoDecisorio.push($scope.Avaliacao[12] + " decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição.");
                                else
                                    $scope.processoDecisorio.push(" Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição.");

                                //processoDecisorio += "";
                                //processoDecisorio += " Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição.\n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                $scope.processoDecisorio.push(" Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo.");
                                //processoDecisorio += " Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo.\n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                $scope.Respostas = $scope.Respostas.toString().replace(7 + ",", "");
                                $scope.Respostas = $scope.Respostas.toString().replace(22 + ",", "");
                                if (k >= 1)
                                    validaprocessoDecisorio = 1;
                            } else
                                if ($scope.Respostas.toString().indexOf("," + 13 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {

                                    if (k == 0)
                                        $scope.processoDecisorio.push($scope.Avaliacao[12] + " toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo.");
                                    else
                                        $scope.processoDecisorio.push(" Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo.");

                                    //processoDecisorio += " Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo.\n";
                                    //processoDecisorio += "\n";
                                    //processoDecisorio += "\n";
                                    $scope.Respostas = $scope.Respostas.toString().replace(13 + ",", "");
                                    if (k >= 1)
                                        validaprocessoDecisorio = 1;
                                }
                                else if ($scope.Respostas.toString().indexOf("," + 16 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                                    if (k == 0)
                                        $scope.processoDecisorio.push($scope.Avaliacao[12] + " frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario.");
                                    else
                                        $scope.processoDecisorio.push(" Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario.");

                                    //processoDecisorio += " Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario.\n";
                                    //processoDecisorio += "\n";
                                    //processoDecisorio += "\n";
                                    $scope.Respostas = $scope.Respostas.toString().replace(16 + ",", "");
                                    if (k >= 1)
                                        validaprocessoDecisorio = 1;
                                } else
                                    if ($scope.Respostas.toString().indexOf("," + 18 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {
                                        if (k == 0)
                                            $scope.processoDecisorio.push($scope.Avaliacao[12] + " decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos.");
                                        else
                                            $scope.processoDecisorio.push(" Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos.");

                                        //processoDecisorio += " Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos.\n";
                                        //processoDecisorio += "\n";
                                        //processoDecisorio += "\n";
                                        $scope.Respostas = $scope.Respostas.toString().replace(18 + ",", "");
                                        if (k >= 1)
                                            validaprocessoDecisorio = 1;
                                    }
                                    else if ($scope.Respostas.toString().indexOf("," + 34 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                                        if (k == 0)
                                            $scope.processoDecisorio.push($scope.Avaliacao[12] + " detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador.");
                                        else
                                            $scope.processoDecisorio.push(" Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador.");

                                        //processoDecisorio += " Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador.\n";
                                        //processoDecisorio += "\n";
                                        //processoDecisorio += "\n";
                                        $scope.Respostas = $scope.Respostas.toString().replace(34 + ",", "");
                                        if (k >= 1)
                                            validaprocessoDecisorio = 1;
                                    }
                                    else
                                        if ($scope.Respostas.toString().indexOf("," + 7 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                            if (k == 0)
                                                $scope.processoDecisorio.push($scope.Avaliacao[12] + " decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição.");
                                            else
                                                $scope.processoDecisorio.push(" Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição.");
                                            //processoDecisorio += " Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição.\n";
                                            //processoDecisorio += "\n";
                                            //processoDecisorio += "\n";
                                            $scope.Respostas = $scope.Respostas.toString().replace(7 + ",", "");

                                            if (k >= 1)
                                                validaprocessoDecisorio = 1;
                                        }
                                        else if ($scope.Respostas.toString().indexOf("," + 22 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                            if (k == 0)
                                                $scope.processoDecisorio.push($scope.Avaliacao[12] + " outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo.");
                                            else
                                                $scope.processoDecisorio.push(" Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo.");

                                            //processoDecisorio += " Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo.\n";
                                            //processoDecisorio += "\n";
                                            //processoDecisorio += "\n";
                                            if (k >= 1)
                                                validaprocessoDecisorio = 1;
                                        }


                    if (k >= 1 && validaprocessoDecisorio < 1) {
                        //q4
                        //if ($scope.Respostas.toString().indexOf("," + 28 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 33 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //    processoDecisorio += "";
                        //    processoDecisorio += " Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição.\n";
                        //    processoDecisorio += "\n";
                        //    processoDecisorio += "\n";
                        //    processoDecisorio += " Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo.\n";
                        //    processoDecisorio += "\n";
                        //    processoDecisorio += "\n";
                        //    validaprocessoDecisorio = 1;
                        //}
                        //else {
                        //    if ($scope.Respostas.toString().indexOf("," + 28 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //        processoDecisorio += " Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição. \n";
                        //        processoDecisorio += "\n";
                        //        processoDecisorio += "\n";
                        //        validaprocessoDecisorio = 1;
                        //    }
                        //    if ($scope.Respostas.toString().indexOf("," + 33 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q4') {
                        //        processoDecisorio += " Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo. \n";
                        //        processoDecisorio += "\n";
                        //        processoDecisorio += "\n";
                        //        validaprocessoDecisorio = 1;
                        //    }
                        //}
                        //q3
                        if ($scope.Respostas.toString().indexOf("," + 7 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 22 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                            $scope.processoDecisorio.push(" Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição. ");
                            $scope.processoDecisorio.push(" Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo. ");

                            //processoDecisorio += "";
                            //processoDecisorio += " Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição. \n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += " Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo. \n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            validaprocessoDecisorio = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(7 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(22 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 7 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                $scope.processoDecisorio.push(" Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição. ");
                                //processoDecisorio += " Decidir é um processo difícil para esse perfil, considerando que seu desejo é sempre concordar com todos para não contrariar ninguém. O adiamento para uma tomada de posição, as respostas evasivas, o discurso vazio caracterizam e evidenciam a dificuldade na tomada de posição. \n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                validaprocessoDecisorio = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(7 + ",", "");
                            }
                            if ($scope.Respostas.toString().indexOf("," + 22 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                $scope.processoDecisorio.push(" Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo. ");
                                //processoDecisorio += " Outro fator complicador é que esse perfil , caracterizado por uma necessidade social intensa, fica muito voltado para as relações, o que o impede de análises apuradas e criteriosas para uma decisão embasada. Tenderá a fazer observações óbvias, generalistas e de boa aceitação pela maioria, valorizando a harmonia com o grupo. \n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                validaprocessoDecisorio = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(22 + ",", "");
                            }
                        }
                        //q1

                        if ($scope.Respostas.toString().indexOf("," + 13 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 16 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {

                            $scope.processoDecisorio.push(" Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo. ");
                            $scope.processoDecisorio.push(" Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario. ");
                            //processoDecisorio += "";
                            //processoDecisorio += " Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo. \n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += " Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario. \n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            validaprocessoDecisorio = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(13 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(16 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 13 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                $scope.processoDecisorio.push(" Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo. ");
                                //processoDecisorio += " Toma as decisões por si e as comunica ao grupo, apresentando bastante resistência às argumentações que apresentem divergência aos seus pontos de vista. Poderá, às vezes até colher opinião do grupo, mostrando-se aberto a sugestões; depois fará aquilo que na sua opinião achar conveniente, levando em conta mais os interesses pessoais que o coletivo. \n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                validaprocessoDecisorio = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(13 + ",", "");
                            }
                            if ($scope.Respostas.toString().indexOf("," + 16 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                $scope.processoDecisorio.push(" Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario. ");
                                //processoDecisorio += " Frequentemente toma decisões de forma impetuosa e emocional, como demonstração de força e poder. Ainda que o caminho tomado não seja o melhor, resiste e contra argumenta, desconsiderando as razões em contrario. \n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                validaprocessoDecisorio = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(16 + ",", "");
                            }
                        }
                        //q2
                        if ($scope.Respostas.toString().indexOf("," + 18 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 34 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {

                            $scope.processoDecisorio.push(" Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos. ");
                            $scope.processoDecisorio.push(" Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador. ");

                            //processoDecisorio += "";
                            //processoDecisorio += " Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos. \n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += " Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador. \n";
                            //processoDecisorio += "\n";
                            //processoDecisorio += "\n";
                            validaprocessoDecisorio = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(18 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(34 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 18 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                $scope.processoDecisorio.push(" Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos. ");
                                //processoDecisorio += " Decidir é um processo lento e muito sofrido. A procrastinação é um hábito. Custa a decidir e poderá voltar atrás inúmeras vezes. Não dá e nem pede opiniões. Não gosta de palpites. É conservador pelo simples fato de que o território conhecido reserva menor número de imprevistos. \n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                validaprocessoDecisorio = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(18 + ",", "");
                            }
                            if ($scope.Respostas.toString().indexOf("," + 34 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                $scope.processoDecisorio.push(" Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador. ");
                                //processoDecisorio += " Detesta lidar com mudanças e resiste a elas por necessidade de segurança. Não toma decisões arrojadas ou audaciosas. Quanto menor o movimento, menos lhe parecerá assustador. \n";
                                //processoDecisorio += "\n";
                                //processoDecisorio += "\n";
                                validaprocessoDecisorio = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(34 + ",", "");
                            }
                        }
                    }

                }
            }

            /**********************************************/

            //monta dados administração conflitos
            if (((k == 0 && admConflitos.length < 1) || k > 0)) {
                if (validaadmConflitos < 1) {
                    //if ($scope.maioresResultados[k] == 'Q1') {
                    if ($scope.Respostas.toString().indexOf("," + 14 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 21 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {

                        if (k == 0)
                            $scope.admConflitos.push($scope.Avaliacao[12] + " não tem paciência na administração de conflitos de terceiros, frequentemente tomando partido por vínculos ou simpatias. Tende a acolher como tendo razão, a parte cuja argumentação mais o favoreça em interesses pessoais. Quando isso não acontece, força um pedido de desculpas e dá o problema como resolvido, “proibindo” que ele reapareça.");
                        else
                            $scope.admConflitos.push(" Não tem paciência na administração de conflitos de terceiros, frequentemente tomando partido por vínculos ou simpatias. Tende a acolher como tendo razão, a parte cuja argumentação mais o favoreça em interesses pessoais. Quando isso não acontece, força um pedido de desculpas e dá o problema como resolvido, “proibindo” que ele reapareça.");

                        $scope.admConflitos.push(" Quando ele mesmo for uma das partes do conflito irá, até o último instante, defendendo seu ponto de vista e ignorando os argumentos da outra parte. A comunicação será agressiva, seja em conteúdo ou forma e tentará dificultar o espaço de comunicação para seu adversário. ");
                        
                        $scope.Respostas = $scope.Respostas.toString().replace(14 + ",", "");
                        $scope.Respostas = $scope.Respostas.toString().replace(21 + ",", "");
                        if (k >= 1)
                            validaadmConflitos = 1;
                    } else
                        if ($scope.Respostas.toString().indexOf("," + 27 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 31 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {

                            if (k == 0)
                                $scope.admConflitos.push($scope.Avaliacao[12] + " foge a responsabilidade de administrar conflitos, fingindo não ver , ignorando  o que possa estar acontecendo. Quando não conseguir fugir, mostrar-se-à neutro e indiferente na mediação, deixando que aconteça o que tem que acontecer de forma apática e fatalista. Ao ter que se posicionar, é mais provável dizer que ambos estão errados a posicionar-se por um ou outro. ");
                            else
                                $scope.admConflitos.push(" Foge a responsabilidade de administrar conflitos, fingindo não ver , ignorando  o que possa estar acontecendo. Quando não conseguir fugir, mostrar-se-à neutro e indiferente na mediação, deixando que aconteça o que tem que acontecer de forma apática e fatalista. Ao ter que se posicionar, é mais provável dizer que ambos estão errados a posicionar-se por um ou outro. ");

                            $scope.admConflitos.push(" Quando for uma das partes do conflito, tenderá a “travar”, calando ou fugindo ao confronto. ");
                        
                            $scope.Respostas = $scope.Respostas.toString().replace(27 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(31 + ",", "");
                            if (k >= 1)
                                validaadmConflitos = 1;
                        } else
                            if ($scope.Respostas.toString().indexOf("," + 9 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 25 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {

                                if (k == 0)
                                    $scope.admConflitos.push($scope.Avaliacao[12] + " naturalmente conciliador, ante um conflito tentará colocar panos quentes e acalmar a situação com discurso de reconciliação e harmonização. Apela para o célebre “deixa prá lá”, considerando resolvido o que apenas ficou adiado ou varrido para baixo do tapete. ");
                                else
                                    $scope.admConflitos.push(" Naturalmente conciliador, ante um conflito tentará colocar panos quentes e acalmar a situação com discurso de reconciliação e harmonização. Apela para o célebre “deixa prá lá”, considerando resolvido o que apenas ficou adiado ou varrido para baixo do tapete. ");

                                $scope.admConflitos.push(" Quando o conflito o envolver diretamente, poderá até pedir desculpas, mesmo considerando-se com a razão, não posicionando de forma agressiva no contexto do conflito, mas chorando mágoas depois para terceiros, mesmo que nem sequer estejam a par ou envolvidos com o acontecido. ");

                              
                                $scope.Respostas = $scope.Respostas.toString().replace(9 + ",", "");
                                $scope.Respostas = $scope.Respostas.toString().replace(25 + ",", "");
                                if (k >= 1)
                                    validaadmConflitos = 1;
                            } else
                                if ($scope.Respostas.toString().indexOf("," + 14 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                                    if (k == 0)
                                        $scope.admConflitos.push($scope.Avaliacao[12] + " não tem paciência na administração de conflitos de terceiros, frequentemente tomando partido por vínculos ou simpatias. Tende a acolher como tendo razão, a parte cuja argumentação mais o favoreça em interesses pessoais. Quando isso não acontece, força um pedido de desculpas e dá o problema como resolvido, “proibindo” que ele reapareça. ");
                                    else
                                        $scope.admConflitos.push(" Não tem paciência na administração de conflitos de terceiros, frequentemente tomando partido por vínculos ou simpatias. Tende a acolher como tendo razão, a parte cuja argumentação mais o favoreça em interesses pessoais. Quando isso não acontece, força um pedido de desculpas e dá o problema como resolvido, “proibindo” que ele reapareça. ");
                           
                                    $scope.Respostas = $scope.Respostas.toString().replace(14 + ",", "");
                                    if (k >= 1)
                                        validaadmConflitos = 1;
                                }
                                else if ($scope.Respostas.toString().indexOf("," + 21 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q1') {
                                    if (k == 0)
                                        $scope.admConflitos.push($scope.Avaliacao[12] + " quando ele mesmo for uma das partes do conflito irá, até o último instante, defendendo seu ponto de vista e ignorando os argumentos da outra parte. A comunicação será agressiva, seja em conteúdo ou forma e tentará dificultar o espaço de comunicação para seu adversário. ");
                                    else
                                        $scope.admConflitos.push(" Quando ele mesmo for uma das partes do conflito irá, até o último instante, defendendo seu ponto de vista e ignorando os argumentos da outra parte. A comunicação será agressiva, seja em conteúdo ou forma e tentará dificultar o espaço de comunicação para seu adversário. ");

                                
                                    $scope.Respostas = $scope.Respostas.toString().replace(21 + ",", "");
                                    if (k >= 1)
                                        validaadmConflitos = 1;
                                } else
                                    if ($scope.Respostas.toString().indexOf("," + 27 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {
                                        if (k == 0)
                                            $scope.admConflitos.push($scope.Avaliacao[12] + " foge a responsabilidade de administrar conflitos, fingindo não ver , ignorando  o que possa estar acontecendo. Quando não conseguir fugir, mostrar-se-à neutro e indiferente na mediação, deixando que aconteça o que tem que acontecer de forma apática e fatalista. Ao ter que se posicionar, é mais provável dizer que ambos estão errados a posicionar-se por um ou outro. ");
                                        else
                                            $scope.admConflitos.push(" Foge a responsabilidade de administrar conflitos, fingindo não ver , ignorando  o que possa estar acontecendo. Quando não conseguir fugir, mostrar-se-à neutro e indiferente na mediação, deixando que aconteça o que tem que acontecer de forma apática e fatalista. Ao ter que se posicionar, é mais provável dizer que ambos estão errados a posicionar-se por um ou outro. ");
                                 
                                        $scope.Respostas = $scope.Respostas.toString().replace(27 + ",", "");
                                        if (k >= 1)
                                            validaadmConflitos = 1;
                                    }
                                    else if ($scope.Respostas.toString().indexOf("," + 31 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q2') {
                                        if (k == 0)
                                            $scope.admConflitos.push($scope.Avaliacao[12] + " quando for uma das partes do conflito, tenderá a “travar”, calando ou fugindo ao confronto. ");
                                        else
                                            $scope.admConflitos.push(" Quando for uma das partes do conflito, tenderá a “travar”, calando ou fugindo ao confronto. ");

                                   
                                        $scope.Respostas = $scope.Respostas.toString().replace(31 + ",", "");
                                        if (k >= 1)
                                            validaadmConflitos = 1;
                                    } else
                                        if ($scope.Respostas.toString().indexOf("," + 9 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {
                                            if (k == 0)
                                                $scope.admConflitos.push($scope.Avaliacao[12] + " naturalmente conciliador, ante um conflito tentará colocar panos quentes e acalmar a situação com discurso de reconciliação e harmonização. Apela para o célebre “deixa prá lá”, considerando resolvido o que apenas ficou adiado ou varrido para baixo do tapete. ");
                                            else
                                                $scope.admConflitos.push(" Naturalmente conciliador, ante um conflito tentará colocar panos quentes e acalmar a situação com discurso de reconciliação e harmonização. Apela para o célebre “deixa prá lá”, considerando resolvido o que apenas ficou adiado ou varrido para baixo do tapete. ");

                                            $scope.Respostas = $scope.Respostas.toString().replace(9 + ",", "");
                                            if (k >= 1)
                                                validaadmConflitos = 1;
                                        }
                                        else if ($scope.Respostas.toString().indexOf("," + 25 + ",") >= 0 && $scope.maioresResultadosNome[k] == 'Q3') {
                                            if (k == 0)
                                                $scope.admConflitos.push($scope.Avaliacao[12] + " quando o conflito o envolver diretamente, poderá até pedir desculpas, mesmo considerando-se com a razão, não posicionando de forma agressiva no contexto do conflito, mas chorando mágoas depois para terceiros, mesmo que nem sequer estejam a par ou envolvidos com o acontecido. ");
                                            else
                                                $scope.admConflitos.push(" Quando o conflito o envolver diretamente, poderá até pedir desculpas, mesmo considerando-se com a razão, não posicionando de forma agressiva no contexto do conflito, mas chorando mágoas depois para terceiros, mesmo que nem sequer estejam a par ou envolvidos com o acontecido. ");

                                            $scope.Respostas = $scope.Respostas.toString().replace(25 + ",", "");
                                            if (k >= 1)
                                                validaadmConflitos = 1;
                                        }

                    if (k >= 1 && validaadmConflitos < 1) {
                        //q3
                        if ($scope.Respostas.toString().indexOf("," + 9 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 25 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {

                            $scope.admConflitos.push(" Naturalmente conciliador, ante um conflito tentará colocar panos quentes e acalmar a situação com discurso de reconciliação e harmonização. Apela para o célebre “deixa prá lá”, considerando resolvido o que apenas ficou adiado ou varrido para baixo do tapete. ");
                            $scope.admConflitos.push(" Quando o conflito o envolver diretamente, poderá até pedir desculpas, mesmo considerando-se com a razão, não posicionando de forma agressiva no contexto do conflito, mas chorando mágoas depois para terceiros, mesmo que nem sequer estejam a par ou envolvidos com o acontecido. ");

                            validaadmConflitos = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(9 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(25 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 9 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                $scope.admConflitos.push(" Naturalmente conciliador, ante um conflito tentará colocar panos quentes e acalmar a situação com discurso de reconciliação e harmonização. Apela para o célebre “deixa prá lá”, considerando resolvido o que apenas ficou adiado ou varrido para baixo do tapete. ");
                                validaadmConflitos = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(9 + ",", "");
                            }
                            if ($scope.Respostas.toString().indexOf("," + 25 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q3') {
                                $scope.admConflitos.push(" Quando o conflito o envolver diretamente, poderá até pedir desculpas, mesmo considerando-se com a razão, não posicionando de forma agressiva no contexto do conflito, mas chorando mágoas depois para terceiros, mesmo que nem sequer estejam a par ou envolvidos com o acontecido. ");
                                validaadmConflitos = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(25 + ",", "");
                            }
                        }
                        //q1

                        if ($scope.Respostas.toString().indexOf("," + 14 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 21 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {

                            $scope.admConflitos.push(" Não tem paciência na administração de conflitos de terceiros, frequentemente tomando partido por vínculos ou simpatias. Tende a acolher como tendo razão, a parte cuja argumentação mais o favoreça em interesses pessoais. Quando isso não acontece, força um pedido de desculpas e dá o problema como resolvido, “proibindo” que ele reapareça. ");
                            $scope.admConflitos.push(" Quando ele mesmo for uma das partes do conflito irá, até o último instante, defendendo seu ponto de vista e ignorando os argumentos da outra parte. A comunicação será agressiva, seja em conteúdo ou forma e tentará dificultar o espaço de comunicação para seu adversário. ");
                            validaadmConflitos = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(14 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(21 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 14 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                $scope.admConflitos.push(" Não tem paciência na administração de conflitos de terceiros, frequentemente tomando partido por vínculos ou simpatias. Tende a acolher como tendo razão, a parte cuja argumentação mais o favoreça em interesses pessoais. Quando isso não acontece, força um pedido de desculpas e dá o problema como resolvido, “proibindo” que ele reapareça. ");
                                validaadmConflitos = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(14 + ",", "");
                            }
                            if ($scope.Respostas.toString().indexOf("," + 21 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q1') {
                                $scope.admConflitos.push(" Quando ele mesmo for uma das partes do conflito irá, até o último instante, defendendo seu ponto de vista e ignorando os argumentos da outra parte. A comunicação será agressiva, seja em conteúdo ou forma e tentará dificultar o espaço de comunicação para seu adversário. ");
                                validaadmConflitos = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(21 + ",", "");
                            }
                        }
                        //q2
                        if ($scope.Respostas.toString().indexOf("," + 27 + ",") >= 0 && $scope.Respostas.toString().indexOf("," + 31 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {

                            $scope.admConflitos.push(" Foge a responsabilidade de administrar conflitos, fingindo não ver , ignorando  o que possa estar acontecendo. Quando não conseguir fugir, mostrar-se-à neutro e indiferente na mediação, deixando que aconteça o que tem que acontecer de forma apática e fatalista. Ao ter que se posicionar, é mais provável dizer que ambos estão errados a posicionar-se por um ou outro. ");
                            $scope.admConflitos.push(" Quando for uma das partes do conflito, tenderá a “travar”, calando ou fugindo ao confronto. ");
                            validaadmConflitos = 1;
                            $scope.Respostas = $scope.Respostas.toString().replace(27 + ",", "");
                            $scope.Respostas = $scope.Respostas.toString().replace(31 + ",", "");
                        } else {
                            if ($scope.Respostas.toString().indexOf("," + 27 + ",") >= 0 && $scope.maioresResultadosNome[0] != 'Q2') {
                                $scope.admConflitos.push(" Foge a responsabilidade de administrar conflitos, fingindo não ver , ignorando  o que possa estar acontecendo. Quando não conseguir fugir, mostrar-se-à neutro e indiferente na mediação, deixando que aconteça o que tem que acontecer de forma apática e fatalista. Ao ter que se posicionar, é mais provável dizer que ambos estão errados a posicionar-se por um ou outro. ");
                                validaadmConflitos = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(27 + ",", "");
                            }
                            if ($scope.Respostas.toString().indexOf("," + 31 + ",") >= 0) {
                                $scope.admConflitos.push(" Quando for uma das partes do conflito, tenderá a “travar”, calando ou fugindo ao confronto. ");
                                validaadmConflitos = 1;
                                $scope.Respostas = $scope.Respostas.toString().replace(27 + ",", "");
                            }
                        }
                    }
                }
            }
        }

        $scope.consideracaoFinal.push("Recorrendo a  análise SWOT, podemos considerar , no perfil de " + $scope.Avaliacao[12] + " (Talentos) e  FRAQUEZAS (Oportunidades de Otimização).\n ");
        $scope.consideracaoFinal.push($scope.Avaliacao[12]);
        if ($scope.maioresResultados[0] == 'Q1') {
            $scope.consideracaoFinal.push(" apresenta um perfil proativo e destemido, capaz de correr riscos e enfrentar problemas. De raciocínio rápido, esse perfil atua melhor com equipes imaturas e/ou em situações de emergência em que haja urgência na tomada de decisão. Como prefere ser seguido a ser um servidor, em situações inusitadas, tende a buscar por si mesmo a solução, ainda que para isso tenha que quebrar alguma regra. ");
            $scope.consideracaoFinal.push(" A expansão desse perfil e sua evolução para um Q4 (Dominante Cordial) reside em trabalhar no sentido de ampliar as suas Habilidades de Comunicação e Relacionamento Interpessoal, na direção da CORDIALIDADE sem, no entanto, reduzir os seus índices percentuais  de DOMINÂNCIA.  ");

            $scope.consideracaoFinal.push(" O perfil apresentado por " + $scope.Avaliacao[12] + " tende a agir com autonomia , o que é positivo . No entanto, em situações de estresse, dependendo dos percentuais atingidos, poderá  tender a impulsividade e insubordinação. ");
            $scope.consideracaoFinal.push(" É necessário que esteja atento a administração de prioridades, a sua trajetória de carreira , sem empolgar-se com os elogios  (que poderão ser inúmeros) e a um Planejamento eficaz, evitando que manipulações e vaidades lhe obstruam o caminho. ");
        } else
            if ($scope.maioresResultados[0] == 'Q2') {
                $scope.consideracaoFinal.push(" apresenta um perfil reativo, o que lhe confere atributos de disciplina, constância e capacidade de realizar tarefas repetitivas sem entediar-se É também uma pessoa discreta, não afeita a fofocas e rodinhas de conversa. ");
                $scope.consideracaoFinal.push(" É também uma pessoa discreta, não afeita a fofocas e rodinhas de conversa. A maioria das pessoas nesse perfil  apresentam , como Canal  Sensorial  Predominante o CINESTÉSICO, reagindo mais ao que sente do que ao que vê e ouve. ");

                $scope.consideracaoFinal.push(" O perfil apresentado por " + $scope.Avaliacao[12] + ", movido pela Necessidade de Segurança,  é procrastinador por medo de errar , o que o leva a adiar as decisões , não fazer muitas perguntas e resistir a dar respostas.");
                $scope.consideracaoFinal.push("  Para promover a  expansão desse perfil  será necessário um trabalho prévio de elevação da Autoestima, acompanhado de uma varredura de crenças que lhe vença as resistências à exposição e exercite as habilidades de comunicação. ");
            } else
                if ($scope.maioresResultados[0] == 'Q3') {
                    $scope.consideracaoFinal.push(" apresenta um perfil calmo e prestativo, o que lhe confere facilidade de ser aceito e estabelecer vínculos.  Muito expansivo, adapta-se facilmente às situações, sem maiores questionamentos. ");
                    $scope.consideracaoFinal.push(" A maioria das pessoas nesse perfil  apresentam , como Canal  Sensorial  Predominante o CINESTÉSICO, reagindo mais aos sentimentos e demonstrações de afeto do que às regras e regulamentos. ");

                    $scope.consideracaoFinal.push(" O perfil apresentado por " + $scope.Avaliacao[12] + ", , movido pela Necessidade de Aceitação  é, às vezes, prolixo , falando sem pensar , apenas no intuito de agradar. Essa característica lhe dificulta lidar com assuntos sigilosos. A pronta disposição para colaborar e a dificuldade em dizer “Não”, poderão levar FULANO DE TAL a assumir compromissos além do que possa atender, o que o fará a ficar sobrecarregado, começando muitas coisas e não conseguindo levá-las à conclusão.");
                    $scope.consideracaoFinal.push("   Para promover a  expansão desse perfil  será necessário um trabalho prévio de varredura de crenças, promovendo a elevação da Autoestima.  ");
                } else {
                    $scope.consideracaoFinal.push(" apresenta um perfil orientado para o crescimento coletivo  e habilidoso no trato com pessoas e situações, promove uma liderança harmoniosa e cria um ambiente adequado ao trabalho e a cooperação. ");
                    $scope.consideracaoFinal.push("Geralmente este perfil  apresenta ,os Canais  Sensoriais bastante desenvolvidos, o que lhes faculta uma percepção apurada das situações e das pessoas, antecipando soluções e resolvendo problemas já na sua origem. ");

                    $scope.consideracaoFinal.push(" O perfil apresentado por " + $scope.Avaliacao[12] + " , capaz de agir com autonomia e adequação nas diferentes situações, tende a atrair para si todo o trabalho alheio que exija tais aptidões. ");
                    $scope.consideracaoFinal.push(" É necessário que esteja atento a administração de prioridades, a sua trajetória de carreira , sem empolgar-se com os elogios  (que poderão ser inúmeros) e a um Planejamento eficaz, evitando que manipulações e vaidades lhe obstruam o caminho. ");

                }
        if (parseInt(QtdCategoria9) == 0 || parseInt(QtdCategoria8) == 0 || parseInt(QtdCategoria10) == 0 || parseInt(QtdCategoria11) == 0) {
            if (parseInt(QtdCategoria8) == 0)
                $scope.consideracaoFinal.push(" Conseguirá maiores resultados se desenvolver a habilidade de posicionar-se e expor suas vontades , opiniões e contrariedades.");
            if (parseInt(QtdCategoria9) == 0)
                $scope.consideracaoFinal.push(" Conseguirá maiores resultados se optar por não falar impulsivamente, principalmente em momentos de estresse e pressão emocional.");
            if (parseInt(QtdCategoria10) == 0)
                $scope.consideracaoFinal.push(" Conseguirá maiores resultados se desenvolver uma comunicação menos impositiva e mais persuasiva, ouvindo mais e atentando mais para a opinião e sentimentos alheios.");
            if (parseInt(QtdCategoria11) == 0)
                $scope.consideracaoFinal.push(" Necessita  buscar orientação, suporte  e oportunidades de desenvolvimento para melhor uso da comunicação e otimização do relacionamento interpessoal.");
        } else
            $scope.consideracaoFinal.push(" ");

        $scope.visaoGeral.push(planejamentoResultados);
        $scope.visaoGeral.push(lideranca);
        $scope.visaoGeral.push(comunicacaoRelacionamento);
        $scope.visaoGeral.push(processoDecisorio);
        $scope.visaoGeral.push(admConflitos);

        $scope.dadosGraficos.push(QtdCategoria8);
        $scope.dadosGraficos.push(QtdCategoria9);
        $scope.dadosGraficos.push(QtdCategoria10);
        $scope.dadosGraficos.push(QtdCategoria11);

        $scope.dataDominancia.push(Math.round(((parseInt(QtdCategoria8) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) + (parseInt(QtdCategoria11) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11)))) * 100, 2));
        $scope.dataDominancia.push(Math.round(((parseInt(QtdCategoria9) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) + (parseInt(QtdCategoria10) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11)))) * 100, 2));

        $scope.dataHostilidade.push((Math.round(((parseInt(QtdCategoria8) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100) + ((QtdCategoria9 / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100), 2)));
        $scope.dataHostilidade.push((Math.round(((parseInt(QtdCategoria10) / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100) + ((QtdCategoria11 / (parseInt(QtdCategoria8) + parseInt(QtdCategoria9) + parseInt(QtdCategoria10) + parseInt(QtdCategoria11))) * 100), 2)));

        $scope.labels.push(descCategoria8);
        $scope.labels.push(descCategoria9);
        $scope.labels.push(descCategoria10);
        $scope.labels.push(descCategoria11);

        $scope.series.push("Q1");
        $scope.series.push("Q2");
        $scope.series.push("Q3");
        $scope.series.push("Q4"); 

        $scope.data.push(parseFloat((((parseFloat(QtdCategoria8) / (parseFloat(QtdCategoria8) + parseFloat(QtdCategoria9) + parseFloat(QtdCategoria10) + parseFloat(QtdCategoria11)))) * 100).toFixed(2)));
        $scope.data.push(parseFloat(((parseFloat(QtdCategoria9) / (parseFloat(QtdCategoria8) + parseFloat(QtdCategoria9) + parseFloat(QtdCategoria10) + parseFloat(QtdCategoria11)))) * 100).toFixed(2) );
        $scope.data.push(parseFloat(((parseFloat(QtdCategoria10) / (parseFloat(QtdCategoria8) + parseFloat(QtdCategoria9) + parseFloat(QtdCategoria10) + parseFloat(QtdCategoria11)))) * 100).toFixed(2));
        $scope.data.push(parseFloat(((parseFloat(QtdCategoria11) / (parseFloat(QtdCategoria8) + parseFloat(QtdCategoria9) + parseFloat(QtdCategoria10) + parseFloat(QtdCategoria11)))) * 100).toFixed(2) );

        $scope.options = {
            scales: { yAxes: [{ ticks: { max: 100, beginAtZero: true, scaleShowVerticalLines: true, scaleOverride: true } }] },
            tooltips: {
                enabled: true
            },
            hover: {
                animationDuration: 1
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);

                        });
                    });
                }
            }
        }

        $scope.optionsPieEu = {
            title: {
                display: true,
                position: "top",
                text: "Visão do EU",
                fontSize: 18,
                fontColor: "#111"
            },
            legend: {
                display: true,
                position: "bottom",
                labels: {
                    fontColor: "#333",
                    fontSize: 16
                }
            },

            tooltips: {
                enabled: true
            },
            hover: {
                animationDuration: 1
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (pie, index) {
                            var data = dataset.data[index];
                           
                            ctx.fillText(data + "%" + (index < 1 ? "Submissão" : "Dominância"), (index < 1 ? pie._model.x - 100 : pie._model.x + 100), pie._model.y - 5);

                        });
                    });
                }
            }
        }

        $scope.optionsPieMundo = {
            responsive: true,
            title: {
                display: true,
                position: "top",
                text: "Visão do Mundo",
                fontSize: 18,
                fontColor: "#111"
            },
            legend: {
                display: true,
                position: "bottom",
                labels: {
                    fontColor: "#333",
                    fontSize: 16
                }
            },
            hover: {
                animationDuration: 1
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (pie, index) {
                            var data = dataset.data[index];
                            //if (index < 1)
                            //    ctx.textAlign = 'left';
                            //else
                            //    ctx.textAlign = 'rigth';
                            ctx.fillText(data + "%" + (index < 1 ? "Cordialidade" : "Hostilidade"), (index < 1 ? pie._model.x - 100 : pie._model.x + 100), pie._model.y - 5);

                        });
                    });
                }
            }
        }

    }

    $scope.consultar = function () {
        addLoader();
        relatorioexecucaoService.consultar().then(function (response) {
            $scope.registros = response.data;
            removeLoader()
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "RELATORIOEXECUCAO - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalRELATORIOEXECUCAO',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.registro = {};
        });
    }
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
                relatorioexecucaoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.registros, response.data);
                    }
                });

            }
        });
    }

    $scope.editar = function (data) {
        addLoader();
        relatorioexecucaoService.editar(data).then(function (response) {
            removeLoader();
            $scope.registro = response.data;
            $scope.tituloModal = "RELATORIOEXECUCAO - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalRELATORIOEXECUCAO',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.registro = {};
            });
        });
    }

    $scope.clickme = function () {
        swal({
            title: "Avaliação Finalizada",
            text: "Obrigado por realizar o teste conosco.",
            type: "success",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Finalizar!",
            closeOnConfirm: false
        }, function (isConfirm) {
            swal.close();
            
            $location.url('/app/testeavaliacao');
            $scope.$apply();
        });
    }

    $scope.exportAction = function () {

        var form = $('.form'),
        cache_width = form.width(),
        a4 = [595.28, 841.89];
        $('body').scrollTop(0);
            $scope.createPDF();
    
        }

    $scope.createPDF = function () {


        var options = {
            margin: 15,
            filename: 'teste.pdf',
            image: {
                type: 'jpeg',
                quality: 0.98
            },
            html2canvas: {
                dpi: 192,
                letterRendering: true
            },
            jsPDF: {
                orientation: 'portrait'
            }
        }

        var element = document.getElementById('relatorioExecucao');
        element.style.display = 'block';
        html2pdf(element, options);     
    }
    
    $scope.montarDadosGrafico();
}]);