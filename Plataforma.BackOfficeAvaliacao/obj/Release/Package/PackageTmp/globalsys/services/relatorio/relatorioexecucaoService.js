'use strict';
Globalsys.factory('relatorioexecucaoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var relatorioexecucaoServiceFactory = {};

relatorioexecucaoServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/RelatorioExecucao/', data).success(function (response) {
        return response;
    });
};


relatorioexecucaoServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/RelatorioExecucao').success(function (response) {
        return response;
    });
};


relatorioexecucaoServiceFactory.consultarAvaliacao = function (data) {
    return $http.get(serviceBase + 'api/RelatorioExecucao/consultarAvaliacao?Codigo=' + data).success(function (response) {
        return response;
    });
};

relatorioexecucaoServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/RelatorioExecucao/' + data.Codigo).success(function (response) {
        return response;
    });
};




relatorioexecucaoServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/RelatorioExecucao?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


relatorioexecucaoServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/RelatorioExecucao/' + data.Codigo, data)
 };


    return relatorioexecucaoServiceFactory;
}]);
