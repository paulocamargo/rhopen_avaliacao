'use strict';
Globalsys.factory('avaliacaoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var avaliacaoServiceFactory = {};

avaliacaoServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/Avaliacao/', data).success(function (response) {
        return response;
    });
};


avaliacaoServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/Avaliacao').success(function (response) {
        return response;
    });
};


avaliacaoServiceFactory.consultarAvaliacaoRelatorio = function (data) {
    //return $http.get(serviceBase + 'api/Avaliacao/consultarAvaliacaoRelatorio?codigo=' + data);
      //  return response;
        return $http.get(serviceBase + 'api/Avaliacao/consultarAvaliacaoRelatorio?codigo=' + data).success(function (response) {
            return response;
    });
};

avaliacaoServiceFactory.consultarEnunciados = function (data) {
    return $http.get(serviceBase + 'api/Avaliacao/consultarEnunciados?Codigo=' + data).success(function (response) {
        return response;
    });
};

avaliacaoServiceFactory.consultarEnunciadosRelatorio = function (data) {
    return $http.get(serviceBase + 'api/Avaliacao/consultarEnunciadosRelatorio?Codigo=' + data).success(function (response) {
        return response;
    });
};


avaliacaoServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/Avaliacao/' + data.Codigo).success(function (response) {
        return response;
    });
};


avaliacaoServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/Avaliacao?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


avaliacaoServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/Avaliacao/' + data.Codigo, data)
 };


avaliacaoServiceFactory.atualizarAvaliacaoRealizada = function (data) {
    return $http.put(serviceBase + 'api/Avaliacao/' + data.Codigo, data)
};

    return avaliacaoServiceFactory;
}]);
