﻿'use strict';
Globalsys.factory('membroService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var membroServiceFactory = {};

    membroServiceFactory.cadastrar = function (novosMembrosDoGrupo, grupo) {
        return $http.post(serviceBase + 'api/membro?grupo=' + grupo, novosMembrosDoGrupo).success(function (response) {
            return response;
        });
    };

    membroServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/membro?idGrupo=' + data).success(function (response) {
            return response;
        });
    };

    membroServiceFactory.deletar = function (removerMembrosDoGrupo, grupo) {

        return $http.delete(serviceBase + 'api/membro?grupo=' + grupo, {
            data: removerMembrosDoGrupo,
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).success(function (response) {
            return response;
        });
    };

    membroServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/membro?id=' + data.Codigo);
    };

    membroServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/membro/' + data.Codigo, data)
    };

    return membroServiceFactory;

}]);