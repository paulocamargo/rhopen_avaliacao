﻿'use strict';
Globalsys.factory('colaboradorService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var colaboradorServiceFactory = {};

    colaboradorServiceFactory.consultarTipoColaborador = function (data) {
        return $http.get(serviceBase + 'api/Colaborador/ConsultarTipoColaborador/').success(function (response) {
            return response;
        });
    };
    return colaboradorServiceFactory;

}]);