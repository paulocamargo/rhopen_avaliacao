﻿'use strict';
Globalsys.factory('usuarioService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var usuarioServiceFactory = {};
    usuarioServiceFactory.cadastrar = function (data) {
        return $http.post(serviceBase + 'api/Usuario/', data).success(function (response) {
            return response;
        });
    };

    usuarioServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/Usuario').success(function (response) {
            return response;
        });
    };

    usuarioServiceFactory.consultarComFiltroDeTipoDeColaborador = function (data) {
        return $http.get(serviceBase + 'api/Usuario/ConsultarUsuarioPorTipoColaborador/' + data).success(function (response) {
            return response;
        });
    };

    usuarioServiceFactory.deletar = function (data) {
        return $http.delete(serviceBase + 'api/Usuario/' + data.Codigo).success(function (response) {
            return response;
        });
    };

    usuarioServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/Usuario?id=' + data.Codigo).success(function (response) {
            return response;
        });;
    };

    usuarioServiceFactory.obterNomeDoUsuarioLogado = function (data) {
        return $http.get(serviceBase + 'api/Usuario/ObterNomeDoUsuarioLogado').success(function (response) {
            return response;
        });;
    };

    usuarioServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/Usuario/' + data.Codigo, data)
    };

    usuarioServiceFactory.detalhar = function (data) {
        return $http.get(serviceBase + 'api/Usuario/Detalhe/' + data).success(function (response) {
            return response;
        });
    };

    usuarioServiceFactory.trocarSenha = function (data) {
        return $http.post(serviceBase + 'api/Usuario/TrocarSenha/', data).success(function (response) {
            return response;
        });
    };

    usuarioServiceFactory.obterUsuarioLogado = function (data) {
        return $http.get(serviceBase + 'api/Usuario/ObterUsuarioLogado/').success(function (response) {
            return response;
        });;
    };

    usuarioServiceFactory.meusDados = function (data) {
        return $http.get(serviceBase + 'api/Usuario/MeusDados/').success(function (response) {
            return response;
        });
    };

    return usuarioServiceFactory;

}]);