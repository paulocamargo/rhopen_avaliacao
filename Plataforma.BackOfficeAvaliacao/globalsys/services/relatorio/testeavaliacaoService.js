'use strict';
Globalsys.factory('testeavaliacaoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var testeavaliacaoServiceFactory = {};

testeavaliacaoServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/testeavaliacao/', data).success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/testeavaliacao').success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/testeavaliacao/' + data.Codigo).success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/testeavaliacao?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/testeavaliacao/' + data.Codigo, data)
 };


    return testeavaliacaoServiceFactory;
}]);
