'use strict';
Globalsys.factory('categoriaService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var categoriaServiceFactory = {};

categoriaServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/Categoria/', data).success(function (response) {
        return response;
    });
};


categoriaServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/Categoria').success(function (response) {
        return response;
    });
};


categoriaServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/Categoria/' + data.Codigo).success(function (response) {
        return response;
    });
};


categoriaServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/Categoria?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


categoriaServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/Categoria/' + data.Codigo, data)
 };


    return categoriaServiceFactory;
}]);
