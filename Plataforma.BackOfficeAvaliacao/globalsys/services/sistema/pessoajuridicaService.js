'use strict';
Globalsys.factory('pessoajuridicaService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var pessoajuridicaServiceFactory = {};

pessoajuridicaServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/PessoaJuridica/', data).success(function (response) {
        return response;
    });
};


pessoajuridicaServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/PessoaJuridica').success(function (response) {
        return response;
    });
};


pessoajuridicaServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/PessoaJuridica/' + data.Codigo).success(function (response) {
        return response;
    });
};


pessoajuridicaServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/PessoaJuridica?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


pessoajuridicaServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/PessoaJuridica/' + data.Codigo, data)
 };


    return pessoajuridicaServiceFactory;
}]);
