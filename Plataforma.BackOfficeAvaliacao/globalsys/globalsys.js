﻿
var Globalsys = angular.module('globalsys', ['angle', 'ngMask', 'base64', 'LocalStorageModule', 'tmh.dynamicLocale', 'ui.utils.masks', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'angular-chartist', 'chart.js']);
Globalsys.directive('datepickerPopup', function (dateFilter, uibDatepickerPopupConfig) {
    return {
        restrict: 'A',
        priority: 1,
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {

            var dateFormat = attr.datepickerPopup || uibDatepickerPopupConfig.datepickerPopup;
            ngModel.$formatters.push(function (value) {
                return dateFilter(value, dateFormat);
            });
        }
    };
});

function add(arr, data) {
    var item = {};
    if (!exist(arr, data)) {
        arr.push(data);
    }
    return item;
}

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

function update(arr, data) {
    var item = {};
    if (arr.length > 0) {
        for (var index = 0; index < arr.length; index++) {
            if (arr[index].Codigo == data.Codigo) {
                arr[index] = data;
            }
        }
    }
    return item;
}
function exist(arr, data) {
    var exist = false;
    if (arr.length > 0) {
        for (var index = 0; index < arr.length; index++) {
            if (arr[index].Codigo == data.Codigo) {
                exist = true;
            }
        }
    }
    return exist;
}
function remover(arr, item) {
    for (var i = arr.length; i--;) {
        if (arr[i].Codigo === item.Codigo) {
            arr.splice(i, 1);
        }
    }
}
function parsleyFieldDirective($timeout) {
    return {
        restrict: 'E',
        require: '^?form',
        link: function (scope, elm, attrs, formController) {
            if (formController != null) {

                if (formController.parsley) {
                    $timeout(function () {
                        // formController.parsley.validate()
                    }, 150); // Need to validate after the data is in the dom.
                }
            }
        }
    };
}

var parsleyOptions = {
    priorityEnabled: false
    // ,
    // errorsWrapper: '<ul class="parsley-error-list"></ul>'

};
Globalsys.directive('parsleyValidate', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        require: '?form',
        link: function (scope, elm, attrs, formController) {
            elm.bind('$destroy', function () {

                formController.parsley.destroy();
            });
            if (!formController.parsley) {


                formController.parsley = new Parsley(elm[0], parsleyOptions);
                // $timeout(function () { formController.parsley.validate() }, 100);
            }

            scope.$on('feedReceived', function () {
                if (!formController.parsley) {
                    formController.parsley = new Parsley(elm[0], parsleyOptions);
                }
                // formController.parsley.validate();
            });
        }
    };
}]);
Globalsys.directive('input', parsleyFieldDirective);
Globalsys.directive('textarea', parsleyFieldDirective);
Globalsys.directive('select', parsleyFieldDirective);
Globalsys.directive('parsleyValidate', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        require: '?form',
        link: function (scope, elm, attrs, formController) {
            elm.bind('$destroy', function () {

                formController.parsley.destroy();
            });
            if (!formController.parsley) {
                formController.parsley = new Parsley(elm[0], parsleyOptions);
                //$timeout(function () { formController.parsley.validate() }, 100);
            }



            scope.$on('feedReceived', function () {
                if (!formController.parsley) {
                    formController.parsley = new Parsley(elm[0], parsleyOptions);
                }
                //formController.parsley.validate();
            });
            if (window.ParsleyValidator)
                window.ParsleyValidator.setLocale('pt-BR');
        }
    };
}]);
Globalsys.directive('filestyle', filestyle);

function filestyle() {
    var directive = {
        link: link,
        restrict: 'A'
    };
    return directive;

    function link(scope, element) {
        var options = element.data();

        // old usage support
        options.classInput = element.data('classinput') || options.classInput;

        element.filestyle(options);
    }
}

Globalsys.directive('hasPermission', function (permissionService, $timeout, $compile, $rootScope) {
    return {
        link: function (scope, element, attrs) {
            $rootScope.permissoes = [];
            var controllerName = scope.$resolve.$$controller;
            var objPermisao = {
                Controller: controllerName,
                Actions: []
            }

            if ($rootScope.permissoes.length > 0) {
                verificarPermissao();
            } else {
               
            }
            function verificarPermissao() {
                $rootScope.permissoes[0].Actions.forEach(function (item, index, array) {


                    var fn = $compile(angular.element(element).attr('ng-disabled', !item.HasPermission));

                    fn(scope)
                   
                }, 2000);
            }
        }
    };
});
Globalsys.directive('onFileChange', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.onFileChange);

            element.bind('change', function () {
                scope.$apply(function () {
                    var files = element[0].files;
                    if (files) {
                        onChangeHandler(files);
                    }
                });
            });
        }
    };
});



Globalsys.config(["$httpProvider", '$translateProvider', "$stateProvider", '$urlRouterProvider', "RouteHelpersProvider", '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', function ($httpProvider, $translateProvider, $stateProvider, $urlRouterProvider, RouteHelpersProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
    //$httpProvider.interceptors.push('authInterceptorService');
    $urlRouterProvider.otherwise('/app/testeavaliacao');

    $stateProvider
        // app routes
        .state('app.global', {
            url: '/global',
            abstract: true,
            template: '<div ui-view="" autoscroll="false" ng-class="app.viewAnimation" class="content-wrapper" style="padding: 0px;"></div>',
            data: {
                requireLogin: true
            } // this property will apply to all children of 'app'
        })

     .state('app.seguranca-funcao', {
         url: '/funcao',
         title: 'funcao',
         controller: 'funcaoController',
         resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
         templateUrl: 'globalsys/views/seguranca/funcao.html',

     })
        .state('app.seguranca-acao', {
            url: '/acao',
            title: 'acao',
            controller: 'acaoController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley', 'localytics.directives'),
            templateUrl: 'globalsys/views/seguranca/acao.html',
        })

      .state('app.relatorio-testeavaliacao', {
          url: '/testeavaliacao',
          title: 'testeavaliacao',
          controller: 'testeavaliacaoController',
          resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
          templateUrl: 'globalsys/views/relatorio/testeavaliacao.html',

      })
     .state('app.relatorio-relatorioexecucao', {
         url: '/relatorioexecucao',
         title: 'relatorioexecucao',
         controller: 'relatorioexecucaoController',
         resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
         templateUrl: 'globalsys/views/relatorio/relatorioexecucao.html',

     })
    ;
    // initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

}]);

//Globalsys.run(["$rootScope", "$translate", "$locale", "$state", "$log", "$injector", '$http', 'AUTHSETTINGS', '$location', 'authService', 'uibDatepickerPopupConfig', function ($rootScope, $translate, $locale, $state, $log, $injector, $http, AUTHSETTINGS, $location, authService, uibDatepickerPopupConfig) {
Globalsys.run(["$rootScope", "$translate", "$locale", "$state", "$log", "$injector", '$http',  'AUTHSETTINGS','$location', 'uibDatepickerPopupConfig', function ($rootScope, $translate, $locale, $state, $log, $injector, $http,AUTHSETTINGS,  $location,  uibDatepickerPopupConfig) {
    //authService.fillAuthData();
  
    uibDatepickerPopupConfig.currentText = 'Hoje';
    uibDatepickerPopupConfig.clearText = 'Limpar';
    uibDatepickerPopupConfig.closeText = 'Fechar';
    var isAutenticTeste = false;
    $rootScope.$on('$stateChangeStart', function (event, toState) {
        $location.path('/app/testeavaliacao');
    });
    //

}]);

