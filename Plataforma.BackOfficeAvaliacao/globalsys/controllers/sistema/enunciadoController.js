'use strict';
Globalsys.controller('enunciadoController', ['$scope', 'enunciadoService', '$uibModal','categoriaService', function ($scope, enunciadoService, $uibModal, categoriaService) {
    $scope.registros = [];
    $scope.registro = {};
    $scope.Categorias = {};
    $scope.QtdCategorias = [];
    $scope.tituloModal = "";
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };
    
    $scope.salvar = function () {
        if ($scope.registro.Codigo == undefined) {
            addLoader();
            enunciadoService.cadastrar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');

                }
            }, function (error) {

            });
        } else {
            addLoader();
            enunciadoService.atualizar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    $scope.loadCategorias = function () {
        categoriaService.consultar().then(function (response) {
            $scope.Categorias = response.data;
        });
    }

    $scope.loadCategorias();

    $scope.consultar = function () {
        addLoader();
        enunciadoService.consultar().then(function (response) {
            $scope.registros = response.data;
            removeLoader()
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "Enunciado - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalEnunciado',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.registro = {};
        });
    }
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
               enunciadoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.registros, response.data);
                    }
                });

            }
        });
    }
    $scope.editar = function (data) {
        addLoader();
       enunciadoService.editar(data).then(function (response) {
            removeLoader();
            $scope.registro = response.data;
            $scope.tituloModal = "Enunciado - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalEnunciado',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.registro = {};
            });
        });
    }
 
}]);
