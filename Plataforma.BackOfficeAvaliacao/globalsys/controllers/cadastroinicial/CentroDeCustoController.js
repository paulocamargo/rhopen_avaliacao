﻿'use strict';
Globalsys.controller('centroDeCustoController', ['$scope', 'centroDeCustoService', '$uibModal', '$timeout', 'gerenciaService', function ($scope, centroDeCustoService, $uibModal, $timeout, gerenciaService) {
    $scope.centrosDecustos = [];
    $scope.gerencias = [];
    $scope.centroDecusto = {};
    $scope.tituloModal = "";
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };
    $scope.consultarGerencia = function () {
        gerenciaService.consultar().then(function (response) {
            $scope.gerencias = response.data;

        });
    }
    function getItem(lista, item) {
        var selecionado = {};
        for (var i = 0; i < lista.length; i++) {
            if (lista[i].Codigo == item.Codigo)
                selecionado = lista[i];
        }
        return selecionado;
    }
    $scope.consultarGerencia();
    $scope.salvar = function () {
        if ($scope.centroDecusto.Codigo == undefined) {
            addLoader();
            centroDeCustoService.cadastrar($scope.centroDecusto).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.centrosDecustos, response.data);
                    $scope.centroDecusto = {};
                    $scope.$modalInstance.dismiss('cancel');

                }
            }, function (error) {

            });
        } else {
            addLoader();
            centroDeCustoService.atualizar($scope.centroDecusto).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.centrosDecustos, response.data);
                    $scope.centroDecusto = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    $scope.consultar = function () {
        addLoader();
        centroDeCustoService.consultar().then(function (response) {
            $scope.centrosDecustos = response.data;
            removeLoader()
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "Centro de custo - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalCentroDeCusto',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.centroDecusto = {};
        });
    }
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
                centroDeCustoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.centrosDecustos, response.data);
                    }
                });

            }
        });
    }
    $scope.editar = function (data) {
        addLoader();
        centroDeCustoService.editar(data).then(function (response) {
            removeLoader();
            $scope.centroDecusto = response.data;
            $scope.centroDecusto.Gerencia = getItem($scope.gerencias, response.data.Gerencia);
            $scope.tituloModal = "Centro de custo - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalCentroDeCusto',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.centroDecusto = {};
            });
        });
    }
    $scope.$watch('centroDecusto.Gerencia', function (data) {
        if (data != null && data != "") {
            $scope.centroDecusto.Cliente = data.NomeCliente;
        } else {
            $scope.centroDecusto.Cliente = "";
        }
    });
}]);