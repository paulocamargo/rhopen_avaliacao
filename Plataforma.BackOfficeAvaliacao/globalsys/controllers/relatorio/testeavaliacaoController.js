'use strict';
Globalsys.controller('testeavaliacaoController', ['$scope', '$location', 'testeavaliacaoService', 'relatorioexecucaoService', 'pessoafisicaService', 'avaliacaoService', 'enunciadoavaliacaoService', '$uibModal', function ($scope, $location, testeavaliacaoService, relatorioexecucaoService, pessoafisicaService, avaliacaoService, enunciadoavaliacaoService, $uibModal) {
    $scope.registros = [];
    $scope.Cliente = {};
    $scope.listaCliente = [];
    $scope.listaAvaliacao = [];
    $scope.listaAvaliacaoAux = [];
    $scope.listaQ1 = ['01','24','08','23','12','15','13','16','14','21'];
    $scope.listaQ2 = ['02','40','03','37','17','35','18','34','27','31'];
    $scope.listaQ3 = ['04','30','05','26','06','10','07','22','09','25'];
    $scope.listaQ4 = ['11','39','19','38','20','36','28','33','29','32'];
    $scope.listaResultado = [];
    $scope.registro = {};
    $scope.avaliacao = {};
    $scope.resultadoavaliacao = [];
    $scope.tituloModal = "";
    $scope.ultimaPaginacao = "";
   
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };

    $scope.clickme = function () {
        $location.path('/app/relatorioexecucao');
    };
    
    $scope.loadClientes = function (data) {
        if ($scope.usuario != "undefined"){
            pessoafisicaService.consultarCliente($scope.usuario.CPF.replace(".", "").replace("-", "").replace(".", "")).then(function (response) {
                $scope.listaCliente = response.data;
            });
        }
    }
        
    $scope.salvar = function () {
        var enunciadosSelecionados = "";


        for (var i = 0; i < $scope.listaAvaliacao.length; i++) {
            if ($scope.listaAvaliacao[i].Selected == true) {
                enunciadosSelecionados += $scope.listaAvaliacao[i].codigoEnum + ";";
            }
        }

        $scope.avaliacao.ClientePessoaFisica = $scope.Cliente;
        $scope.avaliacao.ItensSelecionadosEnunciados = enunciadosSelecionados;
        $scope.avaliacao.ModeloAvaliacao = $scope.listaAvaliacao[0].codigoModAval;
            addLoader();
            avaliacaoService.atualizarAvaliacaoRealizada($scope.avaliacao).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            });
            swal("Aviso", "Avaliação de perfil realizado com sucesso, obrigado.", "success");
            
            $scope.montarRelatorio();
    }

    $scope.consultar = function () {
        addLoader();
        testeavaliacaoService.consultar().then(function (response) {
            $scope.registros = response.data;
            removeLoader()
        });
    }


    $scope.testeRelatorio = function () {
        //q4 var codigo = "6;DH;Dominância e Hostilidade;3;SH;Submissão e Hostilidade;7;SC;Submissão e Cordialidade;9;DC;Dominância e Cordialidade;Paulo Henrique Camargo;5;12;36;32;25;13;19;1;18;30;2;35;6;26;33;20;11;8;7;9;39;14;28;21;38;;"
        //q4 var codigo = "7;DH;Dominância e Hostilidade;3;SH;Submissão e Hostilidade;7;SC;Submissão e Cordialidade;7;DC;Dominância e Cordialidade;Paulo Henrique Camargo;1;2;11;8;5;12;6;20;28;14;27;9;10;32;16;33;21;35;25;36;26;38;24;30;;"
        //q3 var codigo = "6;DH;Dominância e Hostilidade;7;SH;Submissão e Hostilidade;8;SC;Submissão e Cordialidade;6;DC;Dominância e Cordialidade;Paulo Henrique Camargo;1,2,11,8,3,5,19,17,6,20,13,7,14,27,9,16,34,22,33,35,25,36,26,24,40,30,39;;"
        //q2 
        var codigo = "8;DH;Dominância e Hostilidade;5;SH;Submissão e Hostilidade;9;SC;Submissão e Cordialidade;9;DC;Dominância e Cordialidade;Paulo Henrique Camargo;1;5;6;7;8;9;10;11;12;13;14;15;17;18;19;20;21;22;23;24;25;26;28;29;30;32;34;35;36;37;38;39;;"        //q1 var codigo = "9;DH;Dominância e Hostilidade;3;SH;Submissão e Hostilidade;7;SC;Submissão e Cordialidade;6;DC;Dominância e Cordialidade;Paulo Henrique Camargo;5;12;24;15;25;13;19;1;18;30;2;35;6;26;33;20;11;8;7;9;39;14;28;21;16;;"




        $location.path('/app/relatorioexecucao').search({ param: codigo });
    }

    $scope.consultarAvaliacao = function () {
        if ($scope.usuario.CPF.length > 1) {
            avaliacaoService.consultarEnunciados($scope.listaCliente[0].Codigo).then(function (response) {
                $scope.listaAvaliacaoAux = response.data;
                $scope.listaAvaliacaoAux.map(function (i) {
                    i.Selected = false;
                    return i;
                }
                )
            });
            var m = $scope.listaAvaliacaoAux.length, t, i;
            while (m) {
                i = Math.floor(Math.random() * m--);
                // And swap it with the current element.
                t = $scope.listaAvaliacaoAux[m];
                $scope.listaAvaliacaoAux[m] = $scope.listaAvaliacaoAux[i];
                $scope.listaAvaliacaoAux[i] = t;
            }
            $scope.listaAvaliacao = $scope.listaAvaliacaoAux;
        }
    }



    $scope.montarRelatorio = function () {
        var QtdCategoria8 = 0;
        var QtdCategoria9 = 0;
        var QtdCategoria10 = 0;
        var QtdCategoria11 = 0;
        var nomeCategoria8 = "";
        var nomeCategoria9 = "";
        var nomeCategoria10 = "";
        var nomeCategoria11 = "";
        var descCategoria8 = "";
        var descCategoria9 = "";
        var descCategoria10 = "";
        var respostasSelecionadas = "";
        var descCategoria11 = "";
        //$location.path('/app/relatorioexecucao').search({ param: 60 });

        var dadosGraficos = $scope.avaliacao.ItensSelecionadosEnunciados.split(";");

        for (var i = 0; i < dadosGraficos.length; i++) {
            for (var j = 0; j < $scope.listaAvaliacao.length; j++) {
               
                if (dadosGraficos[i] == $scope.listaAvaliacao[j].codigoEnum) {
                    if ($scope.listaAvaliacao[j].codigoCategoria == "1") {
                        QtdCategoria8 += 1;
                    } if ($scope.listaAvaliacao[j].codigoCategoria == "2") {
                        QtdCategoria9 += 1;
                    }
                    if ($scope.listaAvaliacao[j].codigoCategoria == "3") {
                        QtdCategoria10 += 1;
                    }
                    if ($scope.listaAvaliacao[j].codigoCategoria == "4") {
                        QtdCategoria11 += 1;
                    }
                    respostasSelecionadas += $scope.listaAvaliacao[j].codigoEnum + ";";
                }
                if ($scope.listaAvaliacao[j].codigoCategoria == "1") {
                    if (nomeCategoria8 == "") {
                        nomeCategoria8 = $scope.listaAvaliacao[j].nomeCategoria;
                        descCategoria8 = $scope.listaAvaliacao[j].descCategoria;
                    }
                } if ($scope.listaAvaliacao[j].codigoCategoria == "2") {
                    if (nomeCategoria9 == "") {
                        nomeCategoria9 = $scope.listaAvaliacao[j].nomeCategoria;
                        descCategoria9 = $scope.listaAvaliacao[j].descCategoria;
                    }
                }
                if ($scope.listaAvaliacao[j].codigoCategoria == "3") {
                    if (nomeCategoria10 == "") {
                        nomeCategoria10 = $scope.listaAvaliacao[j].nomeCategoria;
                        descCategoria10 = $scope.listaAvaliacao[j].descCategoria;
                    }
                }
                if ($scope.listaAvaliacao[j].codigoCategoria == "4") {
                    if (nomeCategoria11 == "") {
                        nomeCategoria11 = $scope.listaAvaliacao[j].nomeCategoria;
                        descCategoria11 = $scope.listaAvaliacao[j].descCategoria;
                    }
                }

            }
        }
        
        var quadranteResultado = QtdCategoria8;
        $scope.listaResultado = $scope.listaQ1;
        if (quadranteResultado < QtdCategoria9)
            $scope.listaResultado = $scope.listaQ2;
        if (quadranteResultado < QtdCategoria10)
            $scope.listaResultado = $scope.listaQ3;
        if (quadranteResultado < QtdCategoria11)
            $scope.listaResultado = $scope.listaQ4;

        //var QtdCategoria8 = 5;
        //var QtdCategoria9 = 3;
        //var QtdCategoria10 = 8;
        //var QtdCategoria11 = 4;
        //var nomeCategoria8 = "DH";
        //var nomeCategoria9 = "SH";
        //var nomeCategoria10 = "SC";
        //var nomeCategoria11 = "DC";
        //var descCategoria8 = "Dominancia e Hostilidade";
        //var descCategoria9 = "Submissão e Hostilidade";
        //var descCategoria10 = "Submissão e Cordialidade";
        //var descCategoria11 = "Dominância e Cordialidade";

        var codigo = QtdCategoria8 + ";" + nomeCategoria8 + ";" + descCategoria8+";";
        codigo += QtdCategoria9 + ";" + nomeCategoria9 + ";" + descCategoria9 + ";";
        codigo += QtdCategoria10 + ";" + nomeCategoria10 + ";" + descCategoria10 + ";";
        codigo += QtdCategoria11 + ";" + nomeCategoria11 + ";" + descCategoria11 + ";";
        codigo += $scope.Cliente.Nome +";";
        codigo += respostasSelecionadas + ";";
        //$location.path('/app/relatorioexecucao').search({ param: $scope.avaliacao.Codigo.concat(";").contat(QtdCategoria9) });
        $location.path('/app/relatorioexecucao').search({ param: codigo });
    };
    //implementation to show save button on last page of search
    $scope.updateProgressIcon = function () {
        if (document.getElementById("indexHeld").innerText == 31)
            $scope.ultimaPaginacao = "OK";
    };

    $scope.abrirModal = function () {
        var sucesso = 0;
        $scope.loadClientes();
        $scope.consultarAvaliacao();
        $scope.Cliente = $scope.listaCliente[0];
        if ($scope.listaCliente.length > 0){
            if ($scope.Cliente.EMAIL == $scope.usuario.Email)
            {
                try {
                    $scope.consultarAvaliacao();
                    if ($scope.listaAvaliacao.length > 1){
                        sucesso = 1;
                    }else 
                        sucesso = -1;
                } catch (e) {
                    sucesso = -1;
                }
            }
            if (sucesso == 1) {
             
            swal({
                title: "Atenção",
                text: "Iniciar o teste de avaliação?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim!",
                cancelButtonText: "Não!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var endDate = new Date();
                    $scope.avaliacao.HoraInicio = endDate;
                    $scope.avaliacao.Codigo = $scope.listaAvaliacao[0].avalCodigo;
                    $scope.tituloModal = "Teste Avaliação - Novo";
                    $scope.$modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'modalTesteAvaliacao',
                        scope: $scope
                    });
                    $scope.$modalInstance.result.then(function () {
                    }, function () {
                        $scope.registro = {};
                    });

                }
            });
        } else
            if (sucesso == -1) {
                swal({
                    title: "Atenção",
                    text: "Usuário não possui avaliação cadastrada.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK!",
                    cancelButtonText: "Não!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                });
            }else
            swal({
                title: "Atenção",
                text: "Email incorreto.",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim!",
                cancelButtonText: "Não!",
                closeOnConfirm: true,
                closeOnCancel: true
            });
        }
    }
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Deseja realmente remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
               testeavaliacaoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.registros, response.data);
                    }
                });

            }
        });
    }

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }

    $scope.editar = function (data) {
        addLoader();
       testeavaliacaoService.editar(data).then(function (response) {
            removeLoader();
            $scope.registro = response.data;
            $scope.tituloModal = "TESTEAVALIACAO - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalTesteAvaliacao',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.registro = {};
            });
        });
    }

}]);
