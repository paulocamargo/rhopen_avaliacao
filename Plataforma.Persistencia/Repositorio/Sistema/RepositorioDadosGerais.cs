﻿using Globalsys;
using Plataforma.Repositorios.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Globalsys;
using System.Threading.Tasks;
using Plataforma.Dominio.Sistema;

namespace Plataforma.Persistencia.Repositorio.Sistema
{
    public class RepositorioDadosGerais : IRepositorioDadosGerais
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RepositorioDadosGerais(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public IQueryable<EnunciadoAvaliacao> ListarEnunciadosAvaliacao(ModeloAvaliacao modAval)
        {
            return UnidadeTrabalho.ObterTodos<EnunciadoAvaliacao>().Where(x => x.ModeloAvaliacao.Codigo == modAval.Codigo);
        }

        public IQueryable<EnunciadoAvaliacao> ListarDadosRelatorioAvaliacao(Avaliacao Aval)
        {
            return UnidadeTrabalho.ObterTodos<EnunciadoAvaliacao>().Where(x => x.Codigo == Aval.Codigo);
        }
    }
}
