﻿using Globalsys;
using Plataforma.Dominio.Seguranca;
using Plataforma.Repositorios.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Repositorio.Seguranca
{
    public class RepositorioFuncao : IRepositorioFuncao
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        public RepositorioFuncao(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }
        public IList<Funcao> ObterPorTipo(TipoFuncao tipo)
        {
            IList<Funcao> funcao = UnidadeTrabalho.ObterTodos<Funcao>()
             .Where(f => (f.Tipo == tipo && f.DataDesativacao == null))
             .ToList();

            return funcao;
        }

        public IList<Funcao> ObterPaginasPorModulo(int idModulo)
        {
            IList<Funcao> funcao = UnidadeTrabalho.ObterTodos<Funcao>()
             .Where(f => (f.Pai.Codigo == idModulo && f.DataDesativacao == null))
             .ToList();

            return funcao;
        }

    }
}
