﻿using Globalsys;
using Plataforma.Dominio.Seguranca;
using Plataforma.Repositorios.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Repositorio.Seguranca
{
    public class RepositorioAcao : IRepositorioAcao
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        public RepositorioAcao(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }
        public IList<Acao> ObterAcoesPorPagina(int pagina)
        {
            IList<Acao> Acao = UnidadeTrabalho.ObterTodos<Acao>()
             .Where(a => a.Funcao.Codigo == pagina && a.DataDesativacao == null)
             .ToList();

            return Acao;
        }

    }
}
