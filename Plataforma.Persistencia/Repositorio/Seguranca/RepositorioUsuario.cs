﻿using Globalsys;
using Plataforma.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys.Extensoes;
using Plataforma.Repositorios.Seguranca;

namespace Plataforma.Persistencia.Repositorio.Seguranca
{
    public class RepositorioUsuario : IRepositorioUsuario
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RepositorioUsuario(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public Usuario ObterPorLogin(string login, string senha = null, bool somenteAtivos = false)
        {
            Usuario usuario = UnidadeTrabalho.ObterTodos<Usuario>()
            .Where(u => (u.Login == login))
            .FirstOrDefault();

            if (usuario == null || (somenteAtivos && (usuario.DataDesativacao != null)))
                return null;

            return usuario;
        }

        public object ChecarLoginAD(string login)
        {
            return Globalsys.Util.Tools.ValidarLogin(login);
        }

        //public IList<Usuario> ObterPorTipoDeColaborador(TipoColaborador tipoColaborador)
        //{
        //    IList<Usuario> usuario = UnidadeTrabalho.ObterTodos<Usuario>()
        //   .Where(u => (u.Colaborador.Tipo == tipoColaborador && u.DataDesativacao == null))
        //   .ToList();

        //    return usuario;
        //}

        public Usuario ObterUsuarioLogado()
        {
            string usuarioLogado = System.Web.HttpContext.Current.TryGetValue(c => c.User.Identity.Name);
            return UnidadeTrabalho.ObterTodos<Usuario>().Where(t => t.Login.Equals(usuarioLogado) && t.DataDesativacao == null).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Dictionary<Funcao, IEnumerable<Acao>> ObterFuncaoeAcoesPorUsuarioLogado()
        {

            Usuario usuario = new RepositorioUsuario(UnidadeTrabalho).ObterUsuarioLogado();
            int[] codigosGrupos = UnidadeTrabalho.ObterTodos<Membro>()
                .Where(mb => mb.Usuario.Codigo == usuario.Codigo && mb.DataDesativacao == null)
                    .Select(gp => gp.Grupo).Select(g => g.Codigo)
                        .ToArray();
            var acoesUsuario = UnidadeTrabalho.ObterTodos<PermisaoUsuarioAcao>().Where(c => c.Usuario.Codigo == usuario.Codigo && c.DataDesativacao == null).GroupBy(g => g.Acao.Funcao, g => g.Acao, (funcao, acoes) =>
                            new { Funcao = funcao, Acoes = acoes })
                                .ToDictionary(group => group.Funcao, group => group.Acoes);

            var acoesGrupo = UnidadeTrabalho.ObterTodos<PermissaoGrupoAcao>()
                 .Where(pga => codigosGrupos.Contains(pga.Grupo.Codigo) && pga.DataDesativacao == null)
                     .GroupBy(g => g.Acao.Funcao, g => g.Acao, (funcao, acoes) =>
                         new { Funcao = funcao, Acoes = acoes })
                             .ToDictionary(group => group.Funcao, group => group.Acoes);
            return acoesUsuario.Concat(acoesGrupo).ToDictionary(s => s.Key, s => s.Value);

            /* return UnidadeTrabalho.ObterTodos<PermissaoGrupoAcao>()
                 .Where(pga => codigosGrupos.Contains(pga.Grupo.Codigo))
                     .GroupBy(g => g.Acao.Funcao, g => g.Acao, (funcao, acoes) =>
                         new { Funcao = funcao, Acoes = acoes })
                             .ToDictionary(group => group.Funcao, group => group.Acoes);*/
        }

        public string ObterNomeDoUsuarioLogado()
        {
            return ObterUsuarioLogado().TryGetValue(v => v.Nome);
        }
    }
}
