﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class AvaliacaoMap : ClassMap<Avaliacao>
    {
        public AvaliacaoMap()
        {
            Table("AVALIACAO");

            Id(u => u.Codigo, "CD_AVALIACAO");
            Map(u => u.QuantidadePagina, "NU_AVALIACAO_QTDPAG").Nullable();
            Map(u => u.TempoTotalAvaliacao, "NU_AVALIACAO_TPTOTAL").Nullable();
            Map(u => u.HoraInicio, "NU_AVALIACAO_TIMEINI").Nullable();
            Map(u => u.HoraFim, "NU_AVALIACAO_TIMEFIM").Nullable();
            Map(u => u.TempoPagina, "NU_AVALIACAO_TIMEPAG").Nullable();
            Map(u => u.TempoCategoria, "NU_AVALIACAO_TIMECAT").Nullable();
            Map(u => u.DataCadastro, "DT_AVALIACAO_CADASTRO").Nullable();
            Map(u => u.DataDesativacao, "DT_AVALIACAO_DESATIVACAO").Nullable();
            Map(u => u.TipoPessoa, "CD_AVALIACAO_TPCLIENTE").Nullable();
            Map(u => u.ItensSelecionadosEnunciados, "TX_AVALIACAO_ITENSSELECIONADOS").Nullable();

            References(u => u.Categoria, "CD_AVALIACAO_CATEGORIA").Nullable();
            References(u => u.ClientePessoaFisica, "CD_AVALIACAO_CLIENTE").Nullable();
//            References(u => u.ClientePessoaJuridica, "CD_AVALIACAO_CLIENTE").Nullable();
            References(u => u.ModeloAvaliacao, "CD_AVALIACAO_MODAVALIACAO").Nullable();
           
        }
    }
}
