﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class EnunciadoMap : ClassMap<Enunciado>
    {
        public EnunciadoMap()
        {
            Table("ENUNCIADO");

            Id(u => u.Codigo, "CD_ENUNCIADO");
            Map(u => u.Nome, "TX_ENUNCIADO_NOME").Nullable();
            Map(u => u.Identificador, "NU_ENUNCIADO_ID").Nullable();
            Map(u => u.DataCadastro, "DT_ENUNCIADO_CADASTRO").Nullable();
            Map(u => u.DataDesativacao, "DT_ENUNCIADO_DESATIVADO").Nullable();
            //Map(u => u.TimeEnunciado, "NU_ENUNCIADO_TIME").Not.Nullable();

            References(u => u.Categoria, "CD_ENUNCIADO_CATEG").Nullable();
        }
    }
}
