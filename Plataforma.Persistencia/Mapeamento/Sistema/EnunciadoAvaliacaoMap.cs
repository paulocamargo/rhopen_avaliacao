﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class EnunciadoAvaliacaoMap : ClassMap<EnunciadoAvaliacao>
    {
        public EnunciadoAvaliacaoMap()
        {
            Table("ENUNCIADO_AVALIACAO");

            Id(u => u.Codigo, "CD_ENUNCIADOAVALIACAO");
            Map(u => u.DataCadastro, "DT_ENUNCIADO_CADASTRO").Nullable();
            Map(u => u.DataDesativacao, "DT_ENUNCIADO_DESATIVADO").Nullable();

            References(u => u.Enunciado, "CD_ENUNCIADOAVALIACAO_ENUN").Nullable();
            References(u => u.ModeloAvaliacao, "CD_ENUNCIADOAVALIACAO_MODAVAL").Nullable();

        }
    }
}
