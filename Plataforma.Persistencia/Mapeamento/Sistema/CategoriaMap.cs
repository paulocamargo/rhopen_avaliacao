﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class CategoriaMap : ClassMap<Categoria>
    {
        public CategoriaMap()
        {
            Table("CATEGORIA");

            Id(u => u.Codigo, "CD_CATEGORIA");
            Map(u => u.Nome, "TX_CATEGORIA_NOME").Nullable();
            Map(u => u.Descricao, "TX_CATEGORIA_DESC").Nullable();
            Map(u => u.DataCadastro, "DT_CATEGORIA_CADASTRO").Nullable();
            Map(u => u.DataDesativacao, "DT_CATEGORIA_DESATIVACAO").Nullable();

            References(u => u.CategoriaRelacionada, "CD_CATEGORIA_REL").Nullable();


        }
    }
}
