﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class ModeloAvaliacaoMap : ClassMap<ModeloAvaliacao>
    {
        public ModeloAvaliacaoMap()
        {
            Table("MODELO_AVALIACAO");

            Id(u => u.Codigo, "CD_MODAVALIACAO");
            Map(u => u.Nome, "TX_MODAVALIACAO_NOME").Nullable();
            Map(u => u.TempoAvaliacao, "NU_MODAVALIACAO_TIME").Nullable();
            Map(u => u.QtdEnunciadosModelo, "NU_MODAVALIACAO_QTDENUN").Nullable();
            Map(u => u.DataCadastro, "DT_MODAVALIACAO_CADASTRO").Nullable();
            Map(u => u.EnunciadosModelo, "TX_MODAVALIACAO_MODELOVAL").Nullable(); 
            Map(u => u.DataDesativacao, "DT_MODAVALIACAO_DESATIVACAO").Nullable();

        }
    }
}
