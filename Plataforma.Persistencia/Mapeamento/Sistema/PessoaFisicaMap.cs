﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class PessoaFisicaMap : ClassMap<PessoaFisica>
    {
        public PessoaFisicaMap()
        {
            Table("PESSOAFISICA");

            Id(u => u.Codigo, "CD_CODIGO");
            Map(u => u.Nome, "TX_CLIENTE_NOME").Nullable();
            Map(u => u.CPF, "NU_CLIENTE_CPF").Nullable();
            Map(u => u.EMAIL, "TX_CLIENTE_EMAIL").Nullable();
            Map(u => u.Sexo, "CD_CLIENTE_SEXO").Nullable();
            Map(u => u.NivelHierarquico, "TX_CLIENTE_NIVELH").Nullable();
            References(u => u.Hierarquia, "CD_PESSOAFISICA_HIERARQUIA").Nullable();
            Map(u => u.DataNascimento, "DT_CLIENTE_NASCIM").Nullable();

            Map(u => u.DataCadastro, "DT_CLIENTE_CADASTRO").Nullable();
            Map(u => u.DataDesativacao, "DT_CLIENTE_DESATIVACAO").Nullable();
        }
    }
}
