﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class PessoaJuridicaMap : ClassMap<PessoaJuridica>
    {
        public PessoaJuridicaMap()
        {
            Table("PESSOAJURIDICA");

            Id(u => u.Codigo, "CD_CODIGO");
            Map(u => u.RazaoSocial, "TX_CLIENTE_NOME").Nullable();
            Map(u => u.CNPJ, "NU_CLIENTE_CNPJ").Nullable();
            Map(u => u.EMAIL, "TX_CLIENTE_EMAIL").Nullable();
            Map(u => u.DataCadastro, "DT_CLIENTE_CADASTRO").Nullable();
            Map(u => u.DataDesativacao, "DT_CLIENTE_DESATIVACAO").Nullable();

        }
    }
}
