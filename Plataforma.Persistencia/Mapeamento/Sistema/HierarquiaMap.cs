﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Sistema
{
    public class HierarquiaMap : ClassMap<Hierarquia>
    {
        public HierarquiaMap()
        {
            Table("HIERARQUIA");

            Id(u => u.Codigo, "CD_HIERARQUIA");
            Map(u => u.Nome, "TX_HIERARQUIA_NOME").Not.Nullable();
            Map(u => u.DataCadastro, "DT_HIERARQUIA_CADASTRO").Not.Nullable();
            Map(u => u.DataDesativacao, "DT_HIERARQUIA_DESATIVACAO").Nullable();
        }
    }
}