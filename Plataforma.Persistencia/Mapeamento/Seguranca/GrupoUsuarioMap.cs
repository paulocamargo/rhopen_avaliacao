﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class GrupoUsuarioMap : ClassMap<GrupoUsuario>
    {
        public GrupoUsuarioMap()
        {
            Table("GROUP_USUARIO");

            Id(u => u.Codigo, "CD_GPUSUARIO");
            Map(u => u.Nome, "TX_GPUSUARIO_NOME").Nullable();
            Map(u => u.NivelAcesso, "NU_GPUSUARIO_ACS").Nullable();
            Map(u => u.DataCadastro, "DT_GPUSUARIO_CADASTRO").Nullable();
            Map(u => u.DataDesativacao, "DT_GPUSUARIO_DESATIVACAO").Nullable();

        }
    }
}
