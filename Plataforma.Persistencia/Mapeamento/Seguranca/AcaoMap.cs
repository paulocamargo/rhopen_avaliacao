﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class AcaoMap :ClassMap<Acao>
    {
        public AcaoMap()
        {
            Table("SEG_ACAO");

            Id(f => f.Codigo, "CD_ACAO");
            //Map(f => f., "BL_ATIVO").Not.Nullable();
            //Map(f => f., "BL_AUDITAVEL").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO").Not.Nullable();
            Map(f => f.DataDeCadastro, "DT_REGISTRO").Nullable();
            //Map(f => f., "TX_DESCRICAO").Nullable();
            Map(f => f.Nome, "TX_NOME").Not.Nullable();
            //Map(f => f., "CD_PAGINA").Nullable();
            //Map(f => f.Icone, "TX_URL").Nullable();

        }
    }
}
