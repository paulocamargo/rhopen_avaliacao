﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class UsuarioMap : ClassMap<Usuario>
    {
        public UsuarioMap()
        {
            Table("USUARIO");

            Id(u => u.Codigo, "CD_USUARIO");
            Map(u => u.Nome, "TX_USUARIO_NOME").Nullable();
            Map(u => u.Login, "TX_USUARIO_LOGIN").Nullable();
            Map(u => u.Email, "TX_USER_EMAIL").Nullable();
            Map(u => u.Senha, "TX_USER_PWD").Nullable();
            Map(u => u.CPF, "NU_USUARIO_CPF").Nullable();         
            Map(u => u.DataDesativacao, "DT_USER_DESATIVADO").Nullable();
            Map(u => u.DataCadastro, "DT_USUARIO_CADASTRO").Nullable();
            Map(u => u.PrimeiroAcesso, "BL_USUARIO_PACESSO").Nullable();
            References(u => u.GrupoUsuario, "CD_USUARIO_GROUP").Nullable();

        }
    }
}