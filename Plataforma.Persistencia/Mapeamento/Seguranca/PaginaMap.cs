﻿using FluentNHibernate.Mapping;
using Plataforma.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia.Mapeamento.Seguranca
{
    public class PaginaMap : ClassMap<Pagina>
    {
        public PaginaMap()
        {
            Table("SISTEMA_PAGINA");

            Id(p => p.Codigo, "CD_PAGINA");

            Map(p => p.Ativo, "BL_ATIVO");
            Map(p => p.DataDesativacao, "DT_DESATIVACAO").Nullable().Precision(4);
            Map(p => p.DataRegistro, "DT_REGISTRO").Nullable().Precision(4);
            Map(p => p.Nome, "TX_NOME").Not.Nullable();
            Map(p => p.Url, "TX_URL").Not.Nullable();
            Map(p => p.Ordem, "NU_SEQUENCIA").Not.Nullable();
            Map(p => p.TipoPagina, "TX_TP_PAGINA").Not.Nullable();
            References(p => p.PaginaPai, "CD_PAI");
            HasMany(p => p.Acoes).KeyColumn("CD_PAGINA");
            HasMany(p => p.PaginasFilhas).KeyColumn("CD_PAI");
        }
    }
}
