﻿using Globalsys;
using Globalsys.Repositories;
using Ninject.Modules;
using Plataforma.Persistencia.Repositorio.Seguranca;
using Plataforma.Persistencia.Repositorio.Sistema;
using Plataforma.Repositorios.Seguranca;
using Plataforma.Repositorios.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Persistencia
{
    
    public class ModuloPlataforma : NinjectModule
    {
        public override void Load()
        {
            base.Bind<IUnidadeTrabalho>().To<UnidadeTrabalho>();

            base.Bind<IRepositorioGrupo>().To<RepositorioGrupo>();
            base.Bind<IRepositorioUsuario>().To<RepositorioUsuario>();
            base.Bind<IRepositorioFuncao>().To<RepositorioFuncao>();
            base.Bind<IRepositorioAcao>().To<RepositorioAcao>();
            base.Bind<IRepositorioPermissao>().To<RepositorioPermissao>();
           base.Bind<IRepositorioDadosGerais>().To<RepositorioDadosGerais>();


        }
    }
}
