﻿using Plataforma.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys.Extensoes;

namespace Plataforma.Persistencia.Auditoria
{
    public class AuditModel
    {
        public static Usuario GetUserLogged()
        {
            UnidadeTrabalho contexto = new UnidadeTrabalho();
            string usuarioLogado = System.Web.HttpContext.Current.TryGetValue(c => c.User.Identity.Name);
            return contexto.ObterTodos<Dominio.Seguranca.Usuario>().Where(t => t.Login.ToLower().Equals(usuarioLogado.ToLower())).FirstOrDefault();
        }
    }
}
