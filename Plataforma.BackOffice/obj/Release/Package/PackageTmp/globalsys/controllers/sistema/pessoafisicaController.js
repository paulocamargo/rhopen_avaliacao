'use strict';
Globalsys.controller('pessoafisicaController', ['$scope', 'pessoafisicaService', 'hierarquiaService', '$uibModal', '$timeout', function ($scope, pessoafisicaService, hierarquiaService, $uibModal, $timeout) {
    $scope.registros = [];
    $scope.registro = {};
    $scope.Hierarquias = [];
    $scope.tituloModal = "";
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };

    
    //var objetoCombo = "nivelHierarquia";
    //$('.' + objetoCombo).prop('disable', true);
    //$.ajax({
    //    type: "POST",
    //    url: '/PessoaFisica/ConsultarCidade',
    //    data: { codigo: $(this).val() },
    //    success: function (result) {
    //        $('.' + objetoCombo).empty();

    //        for (var i = 0; i < result.Data.length; i++) {
    //            var texto = result.Data[i].hierarquiaNome != null ? result.Data[i].hierarquiaNome : "";
    //            $('.' + objetoCombo).append('<option value="' + result.Data[i].NivelHierarquico + '">' + texto + '</option>');
    //        }
    //        if (result.Data.length > 0) {
    //            $('.' + objetoCombo).prop('disabled', false);
    //        } else {
    //            $('.' + objetoCombo).prop('disabled', true);
    //        }
    //    }
    //});

    $scope.loadHierarquias = function () {
        hierarquiaService.consultar().then(function (response) {
            $scope.Hierarquias = response.data;
        });
    }

    var objetoCombo = "hierarquia";

    $scope.openInicio = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $scope.datePickerDateInicio.opened = !$scope.datePickerDateInicio.opened;
        });
    };

    $scope.openDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $scope.datePickerDateModal.opened = !$scope.datePickerDateModal.opened;
        });

    };
    
    $scope.salvar = function () {
        if ($scope.registro.Codigo == undefined) {
            addLoader();
            if ($scope.registro.DataNascimento.indexOf("/") > 0) {
                var data = $scope.registro.DataNascimento.split("/");
                
            } else
                if ($scope.registro.DataNascimento.indexOf("-") > 0)
                    var data = $scope.registro.DataNascimento.split("-");
            else 
                if ($scope.registro.DataNascimento.indexOf(".") > 0)
                        var data = $scope.registro.DataNascimento.split(".");

            if ($scope.registro.DataNascimento.length == 8)
            {
                data[2] = $scope.registro.DataNascimento.substring(5, 4);
                data[1] = $scope.registro.DataNascimento.substring(3, 2);
                data[0] = $scope.registro.DataNascimento.substring(1, 2);
            }


            $scope.registro.DataNascimento = new Date(data[2], data[1] - 1, data[0], 0, 0, 0, 0);
            pessoafisicaService.cadastrar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');

                }
            }, function (error) {

            });
        } else {
            addLoader();
            pessoafisicaService.atualizar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    $scope.datePickerDateInicio = {
        dateOptions: {
            formatYear: 'yy',
            startingDay: 1
        },
        format: 'dd/MM/yyyy',
        opened: false
    };

    $scope.datePickerDateModal = {
        dateOptions: {
            formatYear: 'yy',
            startingDay: 1
        },
        format: 'dd/MM/yyyy',
        opened: false
    };

    $scope.consultar = function () {
        addLoader();
        pessoafisicaService.consultar().then(function (response) {
            $scope.registros = response.data;
            removeLoader()
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "Pessoa Física - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalPessoaFisica',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.registro = {};
        });
    }
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
               pessoafisicaService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.registros, response.data);
                    }
                });

            }
        });
    }

    $scope.loadHierarquias();

    $scope.$watch('registro.hierarquia.Codigo', function (newValue, oldValue) {
        if (!newValue || newValue === oldValue) return;

        $('#hierarquia').val(response.data.NivelHierarquico);
    });


    $scope.editar = function (data) {
        addLoader();
       pessoafisicaService.editar(data).then(function (response) {
            removeLoader();
            $scope.registro = response.data;
            $scope.tituloModal = "Pessoa Física - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalPessoaFisica',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.registro = {};
            });
       });
    }
}]);
