﻿'use strict';
Globalsys.controller('usuarioController', ['$rootScope', '$scope', 'usuarioService', 'grupousuarioService', '$uibModal', '$timeout', 'colaboradorService', function ($rootScope, $scope, usuarioService, grupousuarioService, $uibModal, $timeout, colaboradorService) {
    $scope.usuarios = [];
    $scope.Grupos = [];
    $scope.tiposColaborador = [];
    $scope.usuario = { Cpf: ""};
    $scope.tituloModal = "";
    $scope.usuDetalhe = {};
    $scope.contratos = [];
    $scope.selecionado = [];
    $scope.contratosSelecionados = [];

    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };

    $scope.loadGrupos = function () {
        grupousuarioService.consultar().then(function (response) {
            $scope.Grupos = response.data;
        });
    }

    $scope.loginEmail = function () {
        $scope.usuario.Login = $scope.usuario.Email;
    }

    $scope.$watch('usuario.GrupoUsuario', function (newValue, oldValue) {
        if (!newValue || newValue === oldValue) return;

        $('#grupoUsuario')[0].selectedIndex =  newValue;
    });

    $scope.loadGrupos();

    //$scope.$watch('usuario.LoginAd', function (data) {
    //    if (data != undefined) {
    //        if (!data) {
    //            $scope.usuario.Login = $scope.usuario.Cpf;
    //        }
    //    }
    //});

    $scope.salvar = function () {
        if ($scope.usuario.Codigo == undefined) {
            addLoader();
            usuarioService.cadastrar($scope.usuario).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.usuarios, response.data);
                    $scope.usuario = {};
                    $scope.$modalInstance.dismiss('cancel');

                }
            }, function (error) {

            });
        } else {
            addLoader();
            usuarioService.atualizar($scope.usuario).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.usuarios, response.data);
                    $scope.usuario = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    $scope.obterNomeDoUsuarioLogado = function () {
        usuarioService.obterNomeDoUsuarioLogado().then(function (response) {
            $rootScope.nomeusuario = response.data;
        });
    }

    $scope.consultar = function () {
        addLoader();
        usuarioService.consultar().then(function (response) {
            $scope.usuarios = response.data;
            removeLoader()
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "Usuário - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalUsuario',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.usuario = {};
        });
    }

    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
                usuarioService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.usuarios, response.data);
                    }
                });

            }
        });
    }

    $scope.editar = function (data) {
        addLoader();
        usuarioService.editar(data).then(function (response) {
            removeLoader();
            $scope.usuario = response.data;
            $scope.tituloModal = "Usuário - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalUsuario',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.usuario = {};
            });
        });
    }

    $scope.detalhar = function (data) {
        addLoader();
        usuarioService.detalhar(data.Codigo).then(function (response) {
            removeLoader();
            $scope.usuDetalhe = response.data;

            $scope.abrirModalDetalhe();
        });
    }

    $scope.abrirModalDetalhe = function () {
        $scope.tituloModalDetalhe = "Detalhe Usuário";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '../globalsys/views/seguranca/meusDados.html',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.usuDetalhe = {};
        });
    }

    $scope.meusDados = function () {
        addLoader();
        usuarioService.meusDados().then(function (response) {
            removeLoader();
            $scope.usuDetalhe = response.data;
            $scope.abrirModalDetalhe();
        });
    }

    $scope.loadContratos = function () {
        contratoService.consultar().then(function (response) {
            $scope.contratos = response.data;
            if ($scope.usuario.Codigo != undefined) {
                //for (var i = 0; i < $scope.usuario.Contratos.length; i++) {
                //    $scope.selecionado.push($scope.usuario.Contratos[i].Codigo);
                //}
            }
        });
    }

    $scope.verificaChecados = function (codigo) {
        return $scope.selecionado.indexOf(codigo) > -1;
    }

}]);