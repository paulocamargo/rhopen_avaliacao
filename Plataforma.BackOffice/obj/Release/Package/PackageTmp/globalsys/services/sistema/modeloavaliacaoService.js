'use strict';
Globalsys.factory('modeloavaliacaoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var modeloavaliacaoServiceFactory = {};

modeloavaliacaoServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/ModeloAvaliacao/', data).success(function (response) {
        return response;
    });
};


modeloavaliacaoServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/ModeloAvaliacao').success(function (response) {
        return response;
    });
};


modeloavaliacaoServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/ModeloAvaliacao/' + data.Codigo).success(function (response) {
        return response;
    });
};


modeloavaliacaoServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/ModeloAvaliacao?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


modeloavaliacaoServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/ModeloAvaliacao/' + data.Codigo, data)
 };


    return modeloavaliacaoServiceFactory;
}]);
