'use strict';
Globalsys.factory('enunciadoavaliacaoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var enunciadoavaliacaoServiceFactory = {};

enunciadoavaliacaoServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/EnunciadoAvaliacao/', data).success(function (response) {
        return response;
    });
};


enunciadoavaliacaoServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/EnunciadoAvaliacao').success(function (response) {
        return response;
    });
};


enunciadoavaliacaoServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/EnunciadoAvaliacao/' + data.Codigo).success(function (response) {
        return response;
    });
};


enunciadoavaliacaoServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/EnunciadoAvaliacao?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


enunciadoavaliacaoServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/EnunciadoAvaliacao/' + data.Codigo, data)
 };


    return enunciadoavaliacaoServiceFactory;
}]);
