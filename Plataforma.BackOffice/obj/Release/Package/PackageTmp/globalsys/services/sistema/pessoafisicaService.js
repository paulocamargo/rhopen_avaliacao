'use strict';
Globalsys.factory('pessoafisicaService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var pessoafisicaServiceFactory = {};

pessoafisicaServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/PessoaFisica/', data).success(function (response) {
        return response;
    });
};


pessoafisicaServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/PessoaFisica').success(function (response) {
        return response;
    });
};


pessoafisicaServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/PessoaFisica/' + data.Codigo).success(function (response) {
        return response;
    });
};


pessoafisicaServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/PessoaFisica?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


pessoafisicaServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/PessoaFisica/' + data.Codigo, data)
 };


    return pessoafisicaServiceFactory;
}]);
