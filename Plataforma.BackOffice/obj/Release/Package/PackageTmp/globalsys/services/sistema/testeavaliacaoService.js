'use strict';
Globalsys.factory('testeavaliacaoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var testeavaliacaoServiceFactory = {};

testeavaliacaoServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/TesteAvaliacao/', data).success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/TesteAvaliacao').success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/TesteAvaliacao/' + data.Codigo).success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/TesteAvaliacao?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


testeavaliacaoServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/TesteAvaliacao/' + data.Codigo, data)
 };


    return testeavaliacaoServiceFactory;
}]);
