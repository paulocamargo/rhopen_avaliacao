﻿'use strict';
Globalsys.factory('permissoesService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var permissoesServiceFactory = {};

    permissoesServiceFactory.cadastrar = function (novosAcoesDoGrupo, grupo) {
        return $http.post(serviceBase + 'api/permissoes?grupo=' + grupo, novosAcoesDoGrupo).success(function (response) {
            return response;
        });
    };

    permissoesServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/permissoes?idGrupo=' + data).success(function (response) {
            return response;
        });
    };

    permissoesServiceFactory.deletar = function (removerPermissoesDoGrupo, usuario) {

        return $http.delete(serviceBase + 'api/permissoes?grupo=' + usuario, {
            data: removerPermissoesDoGrupo,
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).success(function (response) {
            return response;
        });
    };

    permissoesServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/permissoes/' + data.Codigo);
    };


    permissoesServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/permissoes/' + data.Codigo, data)
    };

    return permissoesServiceFactory;

}]);