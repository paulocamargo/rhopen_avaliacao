﻿'use strict';
Globalsys.controller('grupoController', ['$scope', 'toaster', 'grupoService', '$uibModal', '$rootScope', '$state', function ($scope, toaster, grupoService, $uibModal, $rootScope, $state) {
    $scope.grupos = [];
    $scope.grupo = {};
    $scope.tituloModal = "";

    $scope.cancel = function () {
        $scope.$modalGrupoInstance.dismiss('cancel');
    };         

    $scope.salvar = function() {
        if ($scope.grupo.Codigo == undefined) {
            addLoader();
            grupoService.cadastrar($scope.grupo).then(function (response) {
                removeLoader();
                if (response.data) {

                    add($scope.grupos, response.data);
                    $scope.grupo = {};
                    $scope.$modalGrupoInstance.dismiss('cancel');

                } else {
                    sweetAlert("Atenção", response.data.Message, "error");
                }

            }, function(error) {

            });
        } else {
            addLoader();
            grupoService.atualizar($scope.grupo).then(function (response) {
                removeLoader();
                if (response.data) {

                    update($scope.grupos, response.data);
                    $scope.grupo = {};
                    $scope.$modalGrupoInstance.dismiss('cancel');

                } else {
                    sweetAlert("Atenção", response.data.Message, "error");
                }

            }, function(error) {

            });
        }

        toaster.pop('success', 'Sucesso', 'Operação realizada com sucesso!', 2000);
    }

    consultar();

    function consultar() {
        addLoader();
        grupoService.consultar().then(function (response) {
            $scope.grupos = response.data;
            removeLoader()
        });
    }
   
    $scope.abrirModal = function () {
        $scope.tituloModal = "Grupo - Novo";
        $scope.$modalGrupoInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalGrupo',
            scope: $scope

        });
        $scope.$modalGrupoInstance.result.then(function () {
        }, function () {
            $scope.grupo = {};
        });
    }

    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
                grupoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        consultar();
                        //$scope.$modalGrupoInstance.dismiss('cancel');
                        //sweetAlert("Parabéns", response.data.Message, "success");
                        swal("Sucesso!", response.data.Message, "success");
                    } else {
                        sweetAlert("Atenção", response.data.Message, "error");
                    }
                });

            }
            else {
                swal("Atenção", "Ação cancelada!", "warning");
                //$scope.$modalGrupoInstance.dismiss('cancel');
            }
        });
    }

    $scope.editar = function (data) {
        addLoader();
        grupoService.editar(data).then(function (response) {
            removeLoader();
            $scope.grupo = response.data;
            $scope.tituloModal = "Grupo - Editar";
            $scope.$modalGrupoInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalGrupo',
                scope: $scope

            });
            $scope.$modalGrupoInstance.result.then(function () {
            }, function () {
                $scope.produto = {};
            });
        });
    }

    $scope.membros = function (data) {
        $rootScope.grupoSelecionadoNaGridGrupo = data;
        $state.go('app.seguranca-membro');
    }

    $scope.permissoes = function (data) {
        $rootScope.grupoSelecionadoNaGridGrupo = data;
        $state.go('app.seguranca-permissoes-grupos');
    }
}]);