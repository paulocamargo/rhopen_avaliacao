'use strict';
Globalsys.controller('modeloavaliacaoController', ['$scope', 'modeloavaliacaoService', 'categoriaService', '$uibModal', function ($scope, modeloavaliacaoService, categoriaService,$uibModal) {
    $scope.registros = [];
    $scope.Categorias = [];
    $scope.listaCategoriasSelecionado = [];
    $scope.registro = {};
    $scope.tituloModal = "";
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };
    
    $scope.salvar = function () {
        $scope.registro.EnunciadosModelo = "";
        for (var i = 0; i < $scope.listaCategoriasSelecionado.length; i++) {
            $scope.registro.EnunciadosModelo += $scope.listaCategoriasSelecionado[i] + ";";
        }
        if ($scope.registro.Codigo == undefined) {
            addLoader();
            modeloavaliacaoService.cadastrar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');

                }
            }, function (error) {

            });
        } else {
            addLoader();
            modeloavaliacaoService.atualizar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    $scope.consultar = function () {
        addLoader();
        modeloavaliacaoService.consultar().then(function (response) {
            $scope.registros = response.data;
            removeLoader()
        });
    }

    $scope.consultarCategorias = function () {
        categoriaService.consultar().then(function (response) {
            $scope.Categorias = response.data;
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "Modelo Avaliação - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalModeloAvaliacao',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.registro = {};
        });
    }

    $scope.itemChangeSave = function (codigo) {
        var checkFound = $scope.listaCategoriasSelecionado.indexOf(codigo);
        if (checkFound > -1)
            $scope.listaCategoriasSelecionado.splice(checkFound, 1);
        else
            $scope.listaCategoriasSelecionado.push(codigo);
    }

    $scope.consultarCategorias();
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
               modeloavaliacaoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.registros, response.data);
                    }
                });

            }
        });
    }
    $scope.editar = function (data) {
        addLoader();
       modeloavaliacaoService.editar(data).then(function (response) {
            removeLoader();
            $scope.registro = response.data;
            $scope.tituloModal = "Modelo Avaliação - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalModeloAvaliacao',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.registro = {};
            });
        });
    }
 
}]);
