﻿'use strict';
Globalsys.controller('moduloController', ['$rootScope', '$scope', 'moduloService', '$uibModal', '$timeout', '$location', 'menuService', function ($rootScope, $scope, moduloService, $uibModal, $timeout, $location, menuService) {
    $rootScope.modulos = [];
    $rootScope.moduloGlobal = {};
    setMenu();
    function setMenu() {
        var moduloID = window.localStorage.getItem('moduloID');
        if (moduloID != null)
            $rootScope.moduloGlobal = parseInt(moduloID);
    }
    $scope.update = function (data) {
        if (data != null) {
            window.localStorage.setItem('moduloID', data);
            var scope = angular.element($("#vixsidebar")).scope();
            $timeout(function () {
                menuService.carregarMenu(data).then(function (response) {
                    scope.$parent.menuItems = [];
                    //$rootScope.modulos = response.data;
                    $timeout(function () {
                        scope.$parent.menuItems = response.data;
                    }, 1000);
                });
               
            }, 0);
        }
    }
    $scope.loadModulos = function () {
        //window.localStorage.setItem('moduloID', 'moduloService');
        addLoader();
        moduloService.consultar().then(function (response) {
            $rootScope.modulos = response.data;
            //setMenu();
            removeLoader()
        });
    }

    $scope.selecionarModulo = function (data) {
        if (data != null) {
            window.localStorage.setItem('moduloID', data.Codigo);
            $location.path('/app/singleview');
        }
    }

}]);