'use strict';
Globalsys.controller('avaliacaoController', ['$scope', 'avaliacaoService', 'modeloavaliacaoService', 'pessoafisicaService','pessoajuridicaService','$uibModal', function ($scope, avaliacaoService, modeloavaliacaoService,pessoafisicaService,pessoajuridicaService, $uibModal) {
    $scope.registros = [];
    $scope.registro = {};
    $scope.PessoaFisica = {};
    $scope.PessoaJuridica = {};
    $scope.ModeloAvaliacao = {};
    $scope.tituloModal = "";
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };
    
    $scope.salvar = function () {
        if ($scope.registro.Codigo == undefined) {
            addLoader();
            avaliacaoService.cadastrar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');
                    swal("Sucesso!", "Nova avaliação cadastrada com sucesso.", "success");
                 

                }
            }, function (error) {

            });
        } else {
            addLoader();
            avaliacaoService.atualizar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    $scope.consultar = function () {
        addLoader();
        avaliacaoService.consultar().then(function (response) {
            $scope.registros = response.data;
            removeLoader()
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "Avaliação - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalAvaliacao',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.registro = {};
        });
    }
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
               avaliacaoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.registros, response.data);
                    }
                });
               swal("Sucesso!", "Avaliação removida com sucesso.", "success");
            }
        });
    }

    $scope.loadPessoaFisica = function () {
        pessoafisicaService.consultar().then(function (response) {
            $scope.PessoaFisica = response.data;
        });
    }

    $scope.loadPessoaJuridica = function () {
        pessoajuridicaService.consultar().then(function (response) {
            $scope.PessoaJuridica = response.data;
        });
    }

    $scope.loadModeloAvaliacao = function () {
        modeloavaliacaoService.consultar().then(function (response) {
            $scope.ModeloAvaliacao = response.data;
        });
    }

    $scope.loadPessoaFisica();
    $scope.loadPessoaJuridica();
    $scope.loadModeloAvaliacao();
    $scope.editar = function (data) {
        addLoader();
       avaliacaoService.editar(data).then(function (response) {
            removeLoader();
            $scope.registro = response.data;
            $scope.tituloModal = "Avaliação - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalAvaliacao',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.registro = {};
            });
        });
    }
 
}]);
