'use strict';
Globalsys.controller('testeavaliacaoController', ['$scope', 'testeavaliacaoService', 'enunciadoavaliacaoService', '$uibModal', '$window', function ($scope, testeavaliacaoService, enunciadoavaliacaoService, $uibModal, $window) {
    $scope.registros = [];
    $scope.registro = {};
    $scope.Clientes = [];
    $scope.Avaliacoes = [];
    $scope.tituloModal = "";
    $scope.EnunciadoAvaliacao = [];
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };

    $scope.salvar = function () {
        if ($scope.registro.Codigo == undefined) {
            addLoader();
            testeavaliacaoService.cadastrar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');

                }
            }, function (error) {

            });
        } else {
            addLoader();
            testeavaliacaoService.atualizar($scope.registro).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.registros, response.data);
                    $scope.registro = {};
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    $scope.loadCliente = function () {
        pessoafisicaService.consultar().then(function (response) {
            $scope.Clientes = response.data;
            pessoafisicaService.consultar().then(function (responses) {
                var Clientes = responses.data;
                for (var i = 0; i < Clientes.length; i++) {
                    $scope.Clientes.push(Clientes[i]);
                }
            });
        });
    }

    $scope.loadAvaliacao = function () {
        avaliacaoService.consultar().then(function (response) {
            $scope.Avaliacoes = response.data;
        });
    }

   $scope.loadEnunciadoAvaliacao = function () {
        enunciadoavaliacaoService.consultar().then(function (response) {
            $scope.EnunciadoAvaliacao = response.data;
        });
    }

   $scope.loadEnunciadoAvaliacao();
   $scope.loadAvaliacao();
   $scope.loadCliente();

    $scope.consultar = function () {
        addLoader();
        testeavaliacaoService.consultar().then(function (response) {
            $scope.registros = response.data;
            removeLoader()
        });
    }

    $scope.abrirModal = function () {
        $scope.tituloModal = "Teste Avaliação - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalTesteAvaliacao',
            scope: $scope
        });
        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.registro = {};
        });
    }
    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
               testeavaliacaoService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.registros, response.data);
                    }
                });

            }
        });
    }
    $scope.editar = function (data) {
        addLoader();
       testeavaliacaoService.editar(data).then(function (response) {
            removeLoader();
            $scope.registro = response.data;
            $scope.tituloModal = "Teste Avaliação - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalTesteAvaliacao',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.registro = {};
            });
        });
    }
 
}]);
