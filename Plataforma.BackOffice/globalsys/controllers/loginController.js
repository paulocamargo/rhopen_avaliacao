﻿'use strict';
Globalsys.controller('loginController', ['$scope', '$location', 'authService', 'AUTHSETTINGS', '$timeout', '$uibModal', function ($scope, $location, authService, AUTHSETTINGS, $timeout, $uibModal) {

    $scope.usuario = {};

    $scope.loginData = {
        userName: "",
        password: "",
        useRefreshTokens: false,
        activeDirectory: true
    };
  

    $scope.message = "";


    $scope.login = function () {
        addLoader();
        authService.login($scope.loginData).then(function (response) {
            removeLoader();
            $location.path('/app/singleview');
        },
            function (err) {
                removeLoader();
                if (err.error_description.indexOf("Primeiro Login") == 0) {
                    $scope.primeiroAcesso();
                } else {
                    sweetAlert("Atenção", err.error_description, "warning");
                }

            });
    };
    $scope.logOut = function () {
        addLoader();
        $timeout(function () {
            removeLoader();
            authService.logOut();
            if (!authService.authentication.isAuth) {
                $location.path('/page/login');
            } else {
                if ($location.$$path == "/page/login")
                    $location.path('/app/singleview');
            }
        },100);
    }
    $scope.authExternalProvider = function (provider) {

        var redirectUri = location.protocol + '//' + location.host + '/authcomplete.html';

        var externalProviderUrl = AUTHSETTINGS.APISERVICEBASEURI + "api/Account/ExternalLogin?provider=" + provider
            + "&response_type=token&client_id=" + AUTHSETTINGS.CLIENTID
                                                                    + "&redirect_uri=" + redirectUri;
        window.$windowScope = $scope;

        var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
    };

    $scope.authCompletedCB = function (fragment) {

        $scope.$apply(function () {

            if (fragment.haslocalaccount == 'False') {

                authService.logOut();

                authService.externalAuthData = {
                    provider: fragment.provider,
                    userName: fragment.external_user_name,
                    externalAccessToken: fragment.external_access_token
                };

                $location.path('/associate');

            }
            else {
                //Obtain access token and redirect to orders
                var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };
                authService.obtainAccessToken(externalData).then(function (response) {

                    $location.path('/orders');

                },
             function (err) {
                 $scope.message = err.error_description;
             });
            }

        });
    }

    $scope.primeiroAcesso = function () {
        $scope.usuario.Login = $scope.loginData.userName;
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalPrimeiroAcesso',
            scope: $scope
        });

        $scope.$modalInstance.result.then(function () {
        }, function () {
            resetArrays();
        });
    };
    $scope.registrarPrimeiroAcesso = function () {
        if (!validarSenha($scope.usuario.Senha, $scope.usuario.ConfirmacaoSenha)) {
            sweetAlert("Erro", "A confirmação de senha deve ser igual a senha!", "error");
        } else {
            //sweetAlert("Atenção", "Primeiro Acesso Registrado", "warning");
            addLoader();
            authService.registraPrimeiroAcessoUsuario($scope.usuario).then(function (response) {
                removeLoader();
                $scope.loginData.userName = response.data.Email;
                $scope.loginData.password = response.data.Senha;

                $scope.login();
            },
            function (err) {
            });
        }
    }

    function resetArrays() {
        $scope.usuario = {};
    }

    function validarSenha(senha, contrasenha) {
        if (senha != contrasenha)
            return false;
        else
            return true;
    }
}]);
