﻿'use strict';
Globalsys.controller('empresaController', ['$scope', 'empresaService', 'dadosGeraisService', '$uibModal', '$timeout', function ($scope, empresaService, dadosGeraisService, $uibModal, $timeout) {
    $scope.empresas = [];
    $scope.empresa = {};
    $scope.Cidades = [];
    $scope.Estados = [];
    $scope.tituloModal = "";
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.cancel = function () {
        $scope.$modalInstance.dismiss('cancel');
    };

    $scope.datePickerSetting = {
        dateOptions: {
            formatYear: 'yy',
            startingDay: 1
        },
        format: 'dd/MM/yyyy',
        opened: false
    };

    $scope.loadCidades = function (estadoSelecionado) {
        dadosGeraisService.carregarCidades(estadoSelecionado).then(function (response) {
            $scope.Cidades = response.data;
        });
    }

    $scope.loadEstados = function () {
        dadosGeraisService.listarEstados().then(function (response) {
            $scope.Estados = response.data;
        });
    }

    $scope.loadEstados();

    $scope.abrirModal = function () {
        $scope.empresa = {};
        $scope.Cidades = [];
        $scope.tituloModal = "Empresa - Novo";
        $scope.$modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalEmpresa',
            scope: $scope
        });

        $scope.$modalInstance.result.then(function () {
        }, function () {
            $scope.empresa = {};
            $scope.Cidades = [];
        });
    }

    $scope.salvar = function () {
        if ($scope.empresa.Codigo == undefined) {
            addLoader();
            empresaService.cadastrar($scope.empresa).then(function (response) {
                removeLoader();
                if (response.data) {
                    add($scope.empresas, response.data);
                    $scope.empresa = {};
                    $scope.Cidades = [];
                    $scope.$modalInstance.dismiss('cancel');

                }
            }, function (error) {

            });
        } else {
            addLoader();
            empresaService.atualizar($scope.empresa).then(function (response) {
                removeLoader();
                if (response.data) {
                    update($scope.empresas, response.data);
                    $scope.empresa = {};
                    $scope.Cidades = [];
                    $scope.$modalInstance.dismiss('cancel');
                }

            }, function (error) {

            });
        }
    }

    consultar();
    function consultar() {
        addLoader();
        empresaService.consultar().then(function (response) {
            $scope.empresas = response.data;
            removeLoader()
        });
    }

    function getItem(lista, item) {
        var selecionado = {};
        for (var i = 0; i < lista.length; i++) {
            if (lista[i].Codigo == item)
                selecionado = lista[i];
        }
        return selecionado;
    }

    $scope.editar = function (data) {
        addLoader();
        empresaService.editar(data).then(function (response) {
            removeLoader();
            $scope.empresa = response.data;
            $scope.Cidades = response.data.ListaCidade;
            $scope.empresa.Cidade = getItem(response.data.ListaCidade, $scope.empresa.CodigoCidade);

            $scope.tituloModal = "Empresa - Editar";
            $scope.$modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'modalEmpresa',
                scope: $scope

            });
            $scope.$modalInstance.result.then(function () {
            }, function () {
                $scope.empresa = {};
                $scope.Cidades = [];
            });

        });
    }

    $scope.deletar = function (data) {
        swal({
            title: "Atenção",
            text: "Você tem certeza que gostaria de remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                addLoader();
                empresaService.deletar(data).then(function (response) {
                    removeLoader();
                    if (response.data) {
                        remover($scope.empresas, response.data);
                        swal("Parabéns!", response.data.Message, "success");
                    }
                });

            } else {
                swal("Atenção", "Ação cancelada!", "success");
            }
        });
    }

}]);