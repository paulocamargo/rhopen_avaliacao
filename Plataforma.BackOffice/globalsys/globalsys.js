﻿
var Globalsys = angular.module('globalsys', ['angle', 'ngMask', 'base64', 'LocalStorageModule', 'tmh.dynamicLocale', 'ui.utils.masks', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'notification']);
Globalsys.directive('datepickerPopup', function (dateFilter, uibDatepickerPopupConfig) {
    return {
        restrict: 'A',
        priority: 1,
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {

            var dateFormat = attr.datepickerPopup || uibDatepickerPopupConfig.datepickerPopup;
            ngModel.$formatters.push(function (value) {
                return dateFilter(value, dateFormat);
            });
        }
    };
});

function add(arr, data) {
    var item = {};
    if (!exist(arr, data)) {
        arr.push(data);
    }
    return item;
}

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

function update(arr, data) {
    var item = {};
    if (arr.length > 0) {
        for (var index = 0; index < arr.length; index++) {
            if (arr[index].Codigo == data.Codigo) {
                arr[index] = data;
            }
        }
    }
    return item;
}
function exist(arr, data) {
    var exist = false;
    if (arr.length > 0) {
        for (var index = 0; index < arr.length; index++) {
            if (arr[index].Codigo == data.Codigo) {
                exist = true;
            }
        }
    }
    return exist;
}
function remover(arr, item) {
    for (var i = arr.length; i--;) {
        if (arr[i].Codigo === item.Codigo) {
            arr.splice(i, 1);
        }
    }
}
function parsleyFieldDirective($timeout) {
    return {
        restrict: 'E',
        require: '^?form',
        link: function (scope, elm, attrs, formController) {
            if (formController != null) {

                if (formController.parsley) {
                    $timeout(function () {
                        // formController.parsley.validate()
                    }, 150); // Need to validate after the data is in the dom.
                }
            }
        }
    };
}

var parsleyOptions = {
    priorityEnabled: false
    // ,
    // errorsWrapper: '<ul class="parsley-error-list"></ul>'

};
Globalsys.directive('parsleyValidate', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        require: '?form',
        link: function (scope, elm, attrs, formController) {
            elm.bind('$destroy', function () {

                formController.parsley.destroy();
            });
            if (!formController.parsley) {


                formController.parsley = new Parsley(elm[0], parsleyOptions);
                // $timeout(function () { formController.parsley.validate() }, 100);
            }

            scope.$on('feedReceived', function () {
                if (!formController.parsley) {
                    formController.parsley = new Parsley(elm[0], parsleyOptions);
                }
                // formController.parsley.validate();
            });
        }
    };
}]);
Globalsys.directive('input', parsleyFieldDirective);
Globalsys.directive('textarea', parsleyFieldDirective);
Globalsys.directive('select', parsleyFieldDirective);
Globalsys.directive('parsleyValidate', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        require: '?form',
        link: function (scope, elm, attrs, formController) {
            elm.bind('$destroy', function () {

                formController.parsley.destroy();
            });
            if (!formController.parsley) {
                formController.parsley = new Parsley(elm[0], parsleyOptions);
                //$timeout(function () { formController.parsley.validate() }, 100);
            }



            scope.$on('feedReceived', function () {
                if (!formController.parsley) {
                    formController.parsley = new Parsley(elm[0], parsleyOptions);
                }
                //formController.parsley.validate();
            });
            if (window.ParsleyValidator)
                window.ParsleyValidator.setLocale('pt-BR');
        }
    };
}]);
Globalsys.directive('filestyle', filestyle);

function filestyle() {
    var directive = {
        link: link,
        restrict: 'A'
    };
    return directive;

    function link(scope, element) {
        var options = element.data();

        // old usage support
        options.classInput = element.data('classinput') || options.classInput;

        element.filestyle(options);
    }
}

Globalsys.directive('hasPermission', function (permissionService, $timeout, $compile, $rootScope) {
    return {
        link: function (scope, element, attrs) {
            $rootScope.permissoes = [];
            var controllerName = scope.$resolve.$$controller;
            var objPermisao = {
                Controller: controllerName,
                Actions: []
            }

            if ($rootScope.permissoes.length > 0) {
                verificarPermissao();
            } else {
                /*permissionService.verify(JSON.stringify(objPermisao)).then(function (response) {
                    debugger;
                    $rootScope.permissoes.push(response.data);
                    verificarPermissao();
                }, function (error) {

                });*/
            }
            function verificarPermissao() {
                $rootScope.permissoes[0].Actions.forEach(function (item, index, array) {


                    var fn = $compile(angular.element(element).attr('ng-disabled', !item.HasPermission));

                    fn(scope)
                    // $(element.Id).prop('disabled', element.HasPermission);

                }, 2000);
            }
            /*$(':button').prop('disabled', true);
            var objPermisao = {
                Controller: "",
                Actions: []
            }
            var str = attrs.hasPermission.trim();
            var res = [];
            str.replace(/\{(.+?)\}/g, function (_, m) { res.push(m) });
            res.forEach(function (element, index, array) {
                if (index == 0)
                    objPermisao.Controller = element;
                else {
                    var item = { Ref: element.split("|")[0], Id: element.split("|")[1], HasPermission: false };
                    objPermisao.Actions.push(item);
                }
            });

            if (objPermisao.Controller != "") {
                permissionService.verify(JSON.stringify(objPermisao)).then(function (response) {
                    $timeout(function () {
                        response.data.Actions.forEach(function (element, index, array) {
                           
                            
                            var fn = $compile(angular.element($(element.Id)).attr('ng-disabled', !element.HasPermission));
                           // fn(scope)
                           // $(element.Id).prop('disabled', element.HasPermission);
                            debugger;
                        }, 2000);

                    });
                }, function (error) {

                });
            }
            */
            /* debugger;
             var matches = attrs.hasPermission.trim().match(/\[(.*?)\]/);
             var aa = attrs.hasPermission.trim().match("\\[.*?]");
             var bb = attrs.hasPermission.trim().match("\\[[^\\]]*]");
             var items = attrs.hasPermission.trim().split(",");
             var controller = angular.element(element).controller().constructor.name;
             debugger;*/
            /*if (!_.isString(attrs.hasPermission)) {
                throw 'hasPermission value must be a string'
            }
            var value = attrs.hasPermission.trim();
            var notPermissionFlag = value[0] === '!';
            if (notPermissionFlag) {
                value = value.slice(1).trim();
            }

            function toggleVisibilityBasedOnPermission() {
                var hasPermission = permissions.hasPermission(value);
                if (hasPermission && !notPermissionFlag || !hasPermission && notPermissionFlag) {
                    element[0].style.display = 'block';
                }
                else {
                    element[0].style.display = 'none';
                }
            }
            */
            // toggleVisibilityBasedOnPermission();
            // scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
        }
    };
});
Globalsys.directive('onFileChange', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.onFileChange);

            element.bind('change', function () {
                scope.$apply(function () {
                    var files = element[0].files;
                    if (files) {
                        onChangeHandler(files);
                    }
                });
            });

        }
    };
});



Globalsys.config(["$httpProvider", '$translateProvider', "$stateProvider", '$urlRouterProvider', "RouteHelpersProvider", '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', function ($httpProvider, $translateProvider, $stateProvider, $urlRouterProvider, RouteHelpersProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
    $httpProvider.interceptors.push('authInterceptorService');
    $urlRouterProvider.otherwise('/app/singleview');

    $stateProvider
        // app routes
        .state('app.global', {
            url: '/global',
            abstract: true,
            template: '<div ui-view="" autoscroll="false" ng-class="app.viewAnimation" class="content-wrapper" style="padding: 0px;"></div>',
            data: {
                requireLogin: true
            } // this property will apply to all children of 'app'
        })

        /// SEGURANÇA
        .state('app.seguranca-usuario', {
            url: '/usuario',
            title: 'usuario',
            controller: 'usuarioController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley', 'usuario'),
            templateUrl: 'globalsys/views/seguranca/usuario.html',

        })
        .state('app.seguranca-grupo', {
            url: '/grupousuario',
            title: 'grupousuario',
            controller: 'grupousuarioController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley', 'usuario'),
            templateUrl: 'globalsys/views/seguranca/grupousuario.html',

        })
        .state('app.seguranca-funcao', {
            url: '/funcao',
            title: 'funcao',
            controller: 'funcaoController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
            templateUrl: 'globalsys/views/seguranca/funcao.html',

        })
        .state('app.seguranca-acao', {
            url: '/acao',
            title: 'acao',
            controller: 'acaoController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley', 'localytics.directives'),
            templateUrl: 'globalsys/views/seguranca/acao.html',
        })
        /*.state('app.seguranca-permissoes-usuarios', {
            url: '/permissoes-usuarios',
            title: 'permissoes-usuarios',
            controller: 'permissoesUsuariosController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley', 'usuario'),
            templateUrl: 'globalsys/views/seguranca/permissoes-usuarios.html',

        })*/


        /// SISTEMA
        .state('app.sistema-categoria', {
            url: '/categoria',
            title: 'categoria',
            controller: 'categoriaController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
            templateUrl: 'globalsys/views/sistema/categoria.html',

        })
         .state('app.sistema-enunciado', {
             url: '/enunciado',
             title: 'enunciado',
             controller: 'enunciadoController',
             resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
             templateUrl: 'globalsys/views/sistema/enunciado.html',

         })
         .state('app.sistema-modeloavaliacao', {
             url: '/modeloavaliacao',
             title: 'modeloavaliacao',
             controller: 'modeloavaliacaoController',
             resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
             templateUrl: 'globalsys/views/sistema/modeloavaliacao.html',

         })
         .state('app.sistema-pessoafisica', {
             url: '/pessoafisica',
             title: 'pessoafisica',
             controller: 'pessoafisicaController',
             resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
             templateUrl: 'globalsys/views/sistema/pessoafisica.html',

         })
         .state('app.sistema-pessoajuridica', {
             url: '/pessoajuridica',
             title: 'pessoajuridica',
             controller: 'pessoajuridicaController',
             resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
             templateUrl: 'globalsys/views/sistema/pessoajuridica.html',

         })
          .state('app.sistema-avaliacao', {
              url: '/avaliacao',
              title: 'avaliacao',
              controller: 'avaliacaoController',
              resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
              templateUrl: 'globalsys/views/sistema/avaliacao.html',

          })

        //.state('app.sistema-logerros', {
        //    url: '/logerros',
        //    controller: 'logErroController',
        //    resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
        //    templateUrl: 'globalsys/views/sistema/logerro.html',

        .state('app.sistema-auditoria', {
            url: '/auditoria',
            controller: 'auditoriaController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
            templateUrl: 'globalsys/views/sistema/auditoria.html',
        })
        .state('app.relatorio-repassse', {
            url: '/testeavaliacao',
            title: 'testeavaliacao',
            controller: 'testeavaliacaoController',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley'),
            templateUrl: 'globalsys/views/sistema/testeavaliacao.html',

        })

        .state('page', {
            url: '/page',
            templateUrl: 'app/pages/page.html',
            resolve: RouteHelpersProvider.resolveFor('modernizr', 'icons'),
            controller: ['$rootScope', function ($rootScope) {
                $rootScope.app.layout.isBoxed = false;
            }]
        })

        .state('page.login', {
            url: '/login',
            title: 'Login',
            resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley', 'login'),
            templateUrl: 'app/pages/login.html'
        })

        

     .state('app.trocarSenha', {
         url: '/trocarsenha',
         title: 'trocarsenha',
         controller: 'trocarSenhaController',
         resolve: RouteHelpersProvider.resolveFor('toaster', 'filestyle', 'modernizr', 'icons', 'parsley', 'localytics.directives', 'moment', 'ui.calendar'),
         templateUrl: 'app/views/trocasenha.html'
     })

    ;
    // initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

}]);

Globalsys.run(["$rootScope", "$translate", "$locale", "$state", "$log", "$injector", '$http', 'AUTHSETTINGS', '$location', 'authService', 'uibDatepickerPopupConfig', function ($rootScope, $translate, $locale, $state, $log, $injector, $http, AUTHSETTINGS, $location, authService, uibDatepickerPopupConfig) {
    authService.fillAuthData();
  
    uibDatepickerPopupConfig.currentText = 'Hoje';
    uibDatepickerPopupConfig.clearText = 'Limpar';
    uibDatepickerPopupConfig.closeText = 'Fechar';
    var isAutenticTeste = false;
    $rootScope.$on('$stateChangeStart', function (event, toState) {
        if (!authService.authentication.isAuth) {
            $location.path('/page/login');
        } else {
            if ($location.$$path == "/page/login")
                $location.path('/app/singleview');
        }
    });
    //

}]);

