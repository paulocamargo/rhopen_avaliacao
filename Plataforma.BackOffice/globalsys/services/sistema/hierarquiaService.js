'use strict';
Globalsys.factory('hierarquiaService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var hierarquiaServiceFactory = {};

hierarquiaServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/Hierarquia/', data).success(function (response) {
        return response;
    });
};


hierarquiaServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/Hierarquia').success(function (response) {
        return response;
    });
};


hierarquiaServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/Hierarquia/' + data.Codigo).success(function (response) {
        return response;
    });
};


hierarquiaServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/Hierarquia?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


hierarquiaServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/Hierarquia/' + data.Codigo, data)
 };


    return hierarquiaServiceFactory;
}]);
