'use strict';
Globalsys.factory('avaliacaoclienteService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var avaliacaoclienteServiceFactory = {};

avaliacaoclienteServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/AvaliacaoCliente/', data).success(function (response) {
        return response;
    });
};


avaliacaoclienteServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/AvaliacaoCliente').success(function (response) {
        return response;
    });
};


avaliacaoclienteServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/AvaliacaoCliente/' + data.Codigo).success(function (response) {
        return response;
    });
};


avaliacaoclienteServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/AvaliacaoCliente?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


avaliacaoclienteServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/AvaliacaoCliente/' + data.Codigo, data)
 };


    return avaliacaoclienteServiceFactory;
}]);
