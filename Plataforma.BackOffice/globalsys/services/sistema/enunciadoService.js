'use strict';
Globalsys.factory('enunciadoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var enunciadoServiceFactory = {};

enunciadoServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/Enunciado/', data).success(function (response) {
        return response;
    });
};


enunciadoServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/Enunciado').success(function (response) {
        return response;
    });
};


enunciadoServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/Enunciado/' + data.Codigo).success(function (response) {
        return response;
    });
};


enunciadoServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/Enunciado?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


enunciadoServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/Enunciado/' + data.Codigo, data)
 };


    return enunciadoServiceFactory;
}]);
