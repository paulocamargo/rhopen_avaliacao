﻿'use strict';
Globalsys.factory('centroDeCustoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var centroDeCustoServiceFactory = {};
    centroDeCustoServiceFactory.cadastrar = function (data) {
        return $http.post(serviceBase + 'api/CentrosDeCusto/', data).success(function (response) {
            return response;
        });
    };

    centroDeCustoServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/CentrosDeCusto').success(function (response) {
            return response;
        });
    };


    centroDeCustoServiceFactory.deletar = function (data) {
        return $http.delete(serviceBase + 'api/CentrosDeCusto/' + data.Codigo).success(function (response) {
            return response;
        });
    };

    centroDeCustoServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/CentrosDeCusto?id=' + data.Codigo).success(function (response) {
            return response;
        });;
    };


    centroDeCustoServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/CentrosDeCusto/' + data.Codigo, data)
    };

    return centroDeCustoServiceFactory;

}]);