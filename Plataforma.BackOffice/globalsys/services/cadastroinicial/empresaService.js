﻿'use strict';
Globalsys.factory('empresaService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var empresaServiceFactory = {};

    empresaServiceFactory.cadastrar = function (data) {
        return $http.post(serviceBase + 'api/Empresa/', data).success(function (response) {
            return response;
        });
    };

    empresaServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/Empresa').success(function (response) {
            return response;
        });
    };

    empresaServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/Empresa?id=' + data.Codigo);
    };

    empresaServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/Empresa/' + data.Codigo, data)
    };

    empresaServiceFactory.deletar = function (data) {
        return $http.delete(serviceBase + 'api/Empresa/' + data.Codigo).success(function (response) {
            return response;
        });
    };

    return empresaServiceFactory;

}]);