﻿'use strict';
Globalsys.factory('grupoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var GrupoServiceFactory = {};

    GrupoServiceFactory.cadastrar = function (data) {
        debugger;

        return $http.post(serviceBase + 'api/Grupo/', data).success(function(response) {
            return response;
        });
    };

    GrupoServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/Grupo').success(function (response) {
            return response;
        });
    };

    GrupoServiceFactory.deletar = function (data) {
        return $http.delete(serviceBase + 'api/Grupo/' + data.Codigo).success(function (response) {
            return response;
        });
    };

    GrupoServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/Grupo?id=' + data.Codigo);
    };


    GrupoServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/Grupo/' + data.Codigo, data)
    };

    return GrupoServiceFactory;

}]);