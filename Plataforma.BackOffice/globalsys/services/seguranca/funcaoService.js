﻿'use strict';
Globalsys.factory('funcaoService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var funcaoServiceFactory = {};

    funcaoServiceFactory.cadastrar = function (data) {
        debugger;

        return $http.post(serviceBase + 'api/Funcao/', data).success(function(response) {
            return response;
        });
    };

    funcaoServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/Funcao').success(function (response) {
            return response;
        });
    };

    funcaoServiceFactory.ConsultarPorTipo = function (data) {
        return $http.get(serviceBase + 'api/Funcao/ConsultarPorTipo/' + data).success(function (response) {
            return response;
        });
    };

    funcaoServiceFactory.ConsultarCanal = function () {
        return $http.get(serviceBase + 'api/Funcao/ConsultarCanal/').success(function (response) {
            return response;
        });
    };

    funcaoServiceFactory.ConsultarTipo = function () {
        return $http.get(serviceBase + 'api/Funcao/ConsultarTipo/').success(function (response) {
            return response;
        });
    };

    funcaoServiceFactory.ConsultarPaginasPorModulo = function (data) {
        return $http.get(serviceBase + 'api/Funcao/ConsultarPaginasPorModulo/' + data ).success(function (response) {
            return response;
        });
    };
    

    funcaoServiceFactory.deletar = function (data) {
        return $http.delete(serviceBase + 'api/Funcao/' + data.Codigo).success(function (response) {
            return response;
        });
    };

    funcaoServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/Funcao?id=' + data.Codigo);
    };


    funcaoServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/Funcao/' + data.Codigo, data)
    };

    return funcaoServiceFactory;

}]);