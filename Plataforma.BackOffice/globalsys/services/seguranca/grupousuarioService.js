'use strict';
Globalsys.factory('grupousuarioService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;


var grupousuarioServiceFactory = {};

grupousuarioServiceFactory.cadastrar = function (data) {
    return $http.post(serviceBase + 'api/GrupoUsuario/', data).success(function (response) {
        return response;
    });
};


grupousuarioServiceFactory.consultar = function (data) {
    return $http.get(serviceBase + 'api/GrupoUsuario').success(function (response) {
        return response;
    });
};


grupousuarioServiceFactory.deletar = function (data) {
    return $http.delete(serviceBase + 'api/GrupoUsuario/' + data.Codigo).success(function (response) {
        return response;
    });
};


grupousuarioServiceFactory.editar = function (data) {
    return $http.get(serviceBase + 'api/GrupoUsuario?id=' + data.Codigo).success(function (response) {
        return response;
    });
};


grupousuarioServiceFactory.atualizar = function (data) {
    return $http.put(serviceBase + 'api/GrupoUsuario/' + data.Codigo, data)
 };


    return grupousuarioServiceFactory;
}]);
