﻿'use strict';
Globalsys.factory('permissoesUsuariosService', ['$http', 'AUTHSETTINGS', function ($http, AUTHSETTINGS) {

    var serviceBase = AUTHSETTINGS.APISERVICEBASEURI;

    var permissoesUsuariosServiceFactory = {};

    permissoesUsuariosServiceFactory.cadastrar = function (novosAcoesDoUsuario, usuario) {
        return $http.post(serviceBase + 'api/PermissoesUsuarios?usuario=' + usuario, novosAcoesDoUsuario).success(function (response) {
            return response;
        });
    };

    permissoesUsuariosServiceFactory.consultar = function (data) {
        return $http.get(serviceBase + 'api/PermissoesUsuarios?idUsuario=' + data).success(function (response) {
            return response;
        });
    };

    permissoesUsuariosServiceFactory.deletar = function (removerPermissoesUsuarioDoUsuario, usuario) {

        return $http.delete(serviceBase + 'api/PermissoesUsuarios?usuario=' + usuario, {
            data: removerPermissoesUsuarioDoUsuario,
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).success(function (response) {
            return response;
        });
    };

    permissoesUsuariosServiceFactory.editar = function (data) {
        return $http.get(serviceBase + 'api/PermissoesUsuarios/' + data.Codigo);
    };


    permissoesUsuariosServiceFactory.atualizar = function (data) {
        return $http.put(serviceBase + 'api/PermissoesUsuarios/' + data.Codigo, data)
    };

    return permissoesUsuariosServiceFactory;

}]);