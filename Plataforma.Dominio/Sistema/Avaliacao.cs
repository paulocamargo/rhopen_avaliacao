﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Sistema
{
     public class Avaliacao
    {
        public virtual Int32 Codigo { get; set; }

        public virtual Categoria Categoria { get; set; }

        public virtual ModeloAvaliacao ModeloAvaliacao { get; set; }

        public virtual Int32 QuantidadePagina { get; set; }

        public virtual String TempoTotalAvaliacao { get; set; }

        public virtual PessoaFisica ClientePessoaFisica { get; set; }

        //public virtual PessoaJuridica ClientePessoaJuridica { get; set; }

        public virtual String ItensSelecionadosEnunciados { get; set; }

        public virtual String TipoPessoa { get; set; }

        public virtual DateTime HoraInicio { get; set; }

        public virtual DateTime? HoraFim { get; set; }

        public virtual Int32 TempoCategoria { get; set; }

        public virtual Int32 TempoPagina { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }
    }
}
