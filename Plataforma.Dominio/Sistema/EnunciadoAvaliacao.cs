﻿using System;
using System.Collections.Generic;

namespace Plataforma.Dominio.Sistema
{
    public class EnunciadoAvaliacao
    {
       
            public virtual Int32 Codigo { get; set; }

            public virtual Enunciado Enunciado { get; set; }

            public virtual ModeloAvaliacao ModeloAvaliacao { get; set; }

            public virtual DateTime DataCadastro { get; set; }

            public virtual DateTime? DataDesativacao { get; set; }

       
    }
}