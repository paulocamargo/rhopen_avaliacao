﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Sistema
{
    public class Cliente
    {
        public virtual Int32 Codigo { get; set; }

        public virtual String Nome { get; set; }

        public virtual String CPF { get; set; }

        public virtual String CNPJ { get; set; }

        public virtual String Sexo { get; set; }

        public virtual String EMAIL { get; set; }

        public virtual String NivelHierarquico { get; set; }

        public virtual DateTime DataNascimento { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }
    }
}
