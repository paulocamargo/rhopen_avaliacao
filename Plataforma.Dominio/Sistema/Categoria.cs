﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Sistema
{
    public class Categoria
    {
        public virtual Int32 Codigo { get; set; }

        public virtual String Nome { get; set; }

        public virtual String Descricao { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual Categoria CategoriaRelacionada{ get; set; }

        public virtual DateTime? DataDesativacao { get; set; }
    }
}
