﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Sistema
{
    public class PessoaJuridica
    {
        public virtual Int32 Codigo { get; set; }

        public virtual String RazaoSocial { get; set; }

        public virtual String CNPJ { get; set; }

        public virtual String EMAIL { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }
    }
}
