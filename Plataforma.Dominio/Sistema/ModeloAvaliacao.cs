﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Sistema
{
    public class ModeloAvaliacao
    {
        public virtual Int32 Codigo { get; set; }

        public virtual string Nome  { get; set; }

        public virtual int TempoAvaliacao { get; set; }

        public virtual int QtdEnunciadosModelo { get; set; }

        public virtual string EnunciadosModelo { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

    }
}
