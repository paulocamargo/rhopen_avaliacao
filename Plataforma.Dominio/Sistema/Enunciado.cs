﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Sistema
{
    public class Enunciado
    {
        public virtual Int32 Codigo { get; set; }

        public virtual String Nome { get; set; }

        public virtual String Identificador { get; set; }

        //Tempo por enunciado
        public virtual Int32 TimeEnunciado { get; set; }

        public virtual Categoria Categoria { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }
    }
}
