﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Sistema
{
    public class AvaliacaoCliente
    {
        public Int32 Codigo { get; set; }

        public PessoaFisica PessoaFisica { get; set; }

        public PessoaJuridica PessoaJuridica { get; set; }

        public DateTime DataAvaliacao { get; set; }

        public DateTime DataCadatro { get; set; }

        public DateTime DataDesativado { get; set; }
    }
}
