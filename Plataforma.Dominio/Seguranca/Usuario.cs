﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Seguranca
{
    public class Usuario
    {
        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Login { get; set; }
        public virtual string Email { get; set; }
        public virtual string Senha { get; set; }
        public virtual string CPF { get; set; }
        public virtual DateTime? DataDesativacao { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual GrupoUsuario GrupoUsuario { get; set; }
        public virtual bool PrimeiroAcesso { get; set; }
    }
}
