﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Seguranca
{
    public class Pagina
    {

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual string Icone { get; set; }

        public virtual string CorDoModulo { get; set; }

        public virtual int Ordem { get; set; }

        public virtual Pagina PaginaPai { get; set; }

        public virtual Pagina Modulo { get; set; }

        public virtual IList<Pagina> PaginasFilhas { get; set; }

        public virtual string Url { get; set; }

        public virtual TipoPagina TipoPagina { get; set; }

        public virtual IList<Acao> Acoes { get; set; }

        public virtual bool Principal { get; set; }

        public virtual string Logo { get; set; }
    }
}
