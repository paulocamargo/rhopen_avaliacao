﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Seguranca
{
    public class Colaborador
    {
        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Email { get; set; }
        public virtual string Cpf { get; set; }
        public virtual DateTime? DataDesativacao { get; set; }
        public virtual DateTime DataDeCadastro { get; set; }
        //public virtual IList<UsuarioContrato> Contratos { get; set; }
    }
}
