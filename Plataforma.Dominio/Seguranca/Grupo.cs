﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Seguranca
{
    public class Grupo
    {
        public virtual Int32 Codigo { get; set; }

        public virtual String Nome { get; set; }

        public virtual String Descricao { get; set; }

        public virtual IList<Usuario> Usuarios { get; set; }

        public virtual IList<Acao> Acoes { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual DateTime DataDeCadastro { get; set; }
    }
}
