﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Seguranca
{
    public class GrupoUsuario
    {
        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual string NivelAcesso{ get; set; }
        public virtual DateTime? DataDesativacao { get; set; }
        public virtual DateTime? DataCadastro { get; set; }

    }
}
