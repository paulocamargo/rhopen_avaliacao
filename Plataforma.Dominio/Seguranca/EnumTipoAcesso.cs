﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Dominio.Seguranca
{
    public enum TipoAcesso
    {
        [Description("User")]
        Usuário,
        [Description("Admin")]
        Administrador
    }
}
